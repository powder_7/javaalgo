<?php 
use PHPUnit\Framework\TestCase;

// require "vendor/autoload.php";
require_once(__DIR__.'/vendor/autoload.php');

// use PHPUnit_Framework_TestCase;

class DependencyFailureTest extends TestCase
{
	public function testOne()
	{
		$this->assertTrue(false);
	}
	
	/**
	 * @depends testOne
	 */
	public function testTwo()
	{
	}
}

// $testObject = new DependencyFailureTest();
// $testObject->testOne();
// phpinfo();


/*
868
Given a positive integer N, find and return the longest distance between two consecutive 1's in the binary representation of N.

If there aren't two consecutive 1's, return 0.
 */
public class BinaryGap {

	public int binaryGap(int n) {
		int index = 0;
		int prev = 0;
		int ans = 0;
		while (n > 0) {
			index++;
			if ((n & 1) == 1) {
				if (prev != 0)
					ans = Math.max(ans, index - prev);
				prev = index;
			}
			n = n >>> 1;
		}
		return ans;

	}
	
    public int binaryGaps(int N) {
        String s = Integer.toBinaryString(N);
        int prev = 0;
        int max = 0;
        int index = 0;
        for(char c : s.toCharArray()) {
        	index++;
        	if(c == '1') {
	            if(prev != 0) {
	            	max = Math.max(max, index - prev);
	            }
                prev = index;
            }
            
        }
        return max;
    }
	
    
	
    //case : 0010 -> return 0;
    //		 0110 -> return 1
    //       1001 > return 3
    public int binaryGapss(int N) {
        String s = Integer.toBinaryString(N);
        char prev = '0';
        int max = 0;
        int count = 1;
        int total_one = 0;
        for(char c : s.toCharArray()) {
        	
        	if(c == '1' && prev == '0') {
        		max = Math.max(max, count);
	        }
        	if(c == '1') {
        		count = 1;
        		total_one++;
        	}
        	else 
        		count++;
        	
        	prev = c;
            
        }
        if(total_one == 1) return 0;
        return max;
    }
	
	public static void main(String[] agrs) {
		int n = 2;
		int res = new BinaryGap().binaryGapss(n);
		System.out.println(res);
	}
}

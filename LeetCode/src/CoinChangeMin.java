import java.util.HashMap;

/*
322
You are given coins of different denominations and a total amount of money amount. 
Write a function to compute the fewest number of coins that you need to make up that amount. 
If that amount of money cannot be made up by any combination of the coins, return -1.

Example 1:

Input: coins = [1, 2, 5], amount = 11
Output: 3 
Explanation: 11 = 5 + 5 + 1
Example 2:

Input: coins = [2], amount = 3
Output: -1
Note:
You may assume that you have an infinite number of each kind of coin.
 */
public class CoinChangeMin {
	
	// 
	// If V == 0, then 0 coins required.
	// If V > 0
	// minCoin(coins[0..m-1], V) = min {1 + minCoins(V-coin[i])}
	// where i varies from 0 to m-1 and coin[i] <= V
	// Note..DFS and loop thru coin[], return 0 or min coin if found else maxbacktrack to previous node.
	// O(2^n)
	// with memory 544ms run
	public int minCoins(int coins[], int V, HashMap<String, Integer> hm) {
		if (V == 0)
			return 0;
		int result = Integer.MAX_VALUE-1;
		String key = V +"m";
		if(hm.containsKey(key)) {
			return hm.get(key);
		}
		for (int coin : coins) {
			if (coin <= V) {
				int backtrack = minCoins(coins, V - coin, hm) + 1;
				result = Math.min(result, backtrack);
			}
		}
		hm.put(key, result);
		return result;
		
	}

	// 15ms runtimes
	public int coinChange(int[] coins, int amount) {
        int T[] = new int[amount+1];
		T[0]=0;
		for(int i=1; i< T.length; i++) {
			T[i] = Integer.MAX_VALUE-1;
		}
         // Go through all coins smaller than i
        for(int coin : coins) {
            // Compute minimum coins required for all
            // values from 1 to amount
            for(int i=1; i<=amount ; i++) {
				//coin <= total
				if(coin<=i) {
					T[i] = Math.min((1+T[i-coin]), T[i]);
				}
			}
		}
		
		return (T[amount]==Integer.MAX_VALUE-1)?-1: T[amount];
    
    }
	public static void main(String[] args) {
		int[] coins = {1,2,3};
		int V = 5;
		
		int res = new CoinChangeMin().minCoins(coins, V, new HashMap<String, Integer>());
//		int res = new CoinChangeMin().coinChange(coins, V);
		System.out.println(res);
	}
}

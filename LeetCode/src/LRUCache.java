import java.util.*;

/*
 * 146
 * Design and implement a data structure for Least Recently Used (LRU) cache. It should support the following operations: get and put.

get(key) - Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
put(key, value) - Set or insert the value if the key is not already present. When the cache reached its capacity, it should invalidate the least recently used item before inserting a new item.

Follow up:
Could you do both operations in O(1) time complexity?

Example:

LRUCache cache = new LRUCache( 2 /* capacity */
/*
cache.put(1, 1);
cache.put(2, 2);
cache.get(1);       // returns 1
cache.put(3, 3);    // evicts key 2
cache.get(2);       // returns -1 (not found)
cache.put(4, 4);    // evicts key 1
cache.get(1);       // returns -1 (not found)
cache.get(3);       // returns 3
cache.get(4);       // returns 4
 */

public class LRUCache {

	int max_cap = 0;
	int last_insert = Integer.MAX_VALUE;
	Map<Integer, Integer> hm;
	List<Integer> list;

	public LRUCache(int capacity) {
		max_cap = capacity;
		list = new ArrayList<>();
		hm = new HashMap<>();
		System.out.println(max_cap);
	}

	public int get(int key) {
		if (hm.containsKey(key)) {
			// update cache
			list.remove(0);
			list.add(key);

			return hm.get(key);
		} else
			return -1;
	}

	public void put(int key, int value) {
		if (hm.containsKey(key)) {
			//update cache
			list.remove(0);
			list.add(key);
		} else {

			if (hm.size() == max_cap) {
				int x = list.remove(0);
				hm.remove(x);
			}
			list.add(key);
			hm.put(key, value);
		}

	}

	/**
	 * Your LRUCache object will be instantiated and called as such: LRUCache obj =
	 * new LRUCache(capacity); int param_1 = obj.get(key); obj.put(key,value);
	 */
	public static void main(String args) {

	}
}

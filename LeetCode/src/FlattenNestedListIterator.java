import java.util.List;

/*
 * Given a nested list of integers, implement an iterator to flatten it.

Each element is either an integer, or a list -- whose elements may also be integers or other lists.

Example 1:

Input: [[1,1],2,[1,1]]
Output: [1,1,2,1,1]
Explanation: By calling next repeatedly until hasNext returns false, 
             the order of elements returned by next should be: [1,1,2,1,1].
Example 2:

Input: [1,[4,[6]]]
Output: [1,4,6]
Explanation: By calling next repeatedly until hasNext returns false, 
             the order of elements returned by next should be: [1,4,6].
 */
public class FlattenNestedListIterator {

	/**
	 * // This is the interface that allows for creating nested lists.
	 * // You should not implement it, or speculate about its implementation
	 * public interface NestedInteger {
	 *
	 *     // @return true if this NestedInteger holds a single integer, rather than a nested list.
	 *     public boolean isInteger();
	 *
	 *     // @return the single integer that this NestedInteger holds, if it holds a single integer
	 *     // Return null if this NestedInteger holds a nested list
	 *     public Integer getInteger();
	 *
	 *     // @return the nested list that this NestedInteger holds, if it holds a nested list
	 *     // Return null if this NestedInteger holds a single integer
	 *     public List<NestedInteger> getList();
	 * }
	 */
	public class NestedIterator implements Iterator<Integer> {

	    private List<NestedInteger> nList = null;
	    private List<int> index;
	    public NestedIterator(List<NestedInteger> nestedList) {
	        
	        nList = nestedList;
	        for(int i = 0; i < nList.size(); i++) {
	            System.out.println(i);
	        }
	    }

	    @Override
	    public Integer next() {
	        
	        return 0;
	    }

	    @Override
	    public boolean hasNext() {
	        return false;
	    }
	    
	}

	/**
	 * Your NestedIterator object will be instantiated and called as such:
	 * NestedIterator i = new NestedIterator(nestedList);
	 * while (i.hasNext()) v[f()] = i.next();
	 */
}

///**
// * // This is the interface that allows for creating nested lists.
// * // You should not implement it, or speculate about its implementation
// * public interface NestedInteger {
// *
// *     // @return true if this NestedInteger holds a single integer, rather than a nested list.
// *     public boolean isInteger();
// *
// *     // @return the single integer that this NestedInteger holds, if it holds a single integer
// *     // Return null if this NestedInteger holds a nested list
// *     public Integer getInteger();
// *
// *     // @return the nested list that this NestedInteger holds, if it holds a nested list
// *     // Return null if this NestedInteger holds a single integer
// *     public List<NestedInteger> getList();
// * }
// */
//public class NestedIterator implements Iterator<Integer> {
// 
//    private Stack<NestedInteger> stk = new Stack<NestedInteger>();
//    public NestedIterator(List<NestedInteger> nestedList) {
//        
//        //push list at the back into stack.
//        for(int i = nestedList.size()-1; i>=0; i--) {
//            stk.add(nestedList.get(i));
//        }
//        
//        while(!stk.empty()) {
//            System.out.println(stk.pop().getInteger());
//        }
// 
//    }
//
//    @Override
//    public Integer next() {
//        if(!stk.empty() {
//            return stk.pop().getInteger();
//        }
//        return null;
//    }
//
//    @Override
//    public boolean hasNext() {
// 
//            return false;
//    }
//    
//}
//
///**
// * Your NestedIterator object will be instantiated and called as such:
// * NestedIterator i = new NestedIterator(nestedList);
// * while (i.hasNext()) v[f()] = i.next();
// */

//NestedInteger nextInt;
//Stack<Iterator<NestedInteger>> stack;
//
//public NestedIterator(List<NestedInteger> nestedList) {
//    stack = new Stack<Iterator<NestedInteger>>();
//    stack.push(nestedList.iterator());
//}
//
//@Override
//public Integer next() {
//    return nextInt != null ? nextInt.getInteger() : null; //Just in case
//}
//
//@Override
//public boolean hasNext() {
//    while (!stack.isEmpty()) {
//        if (!stack.peek().hasNext()) stack.pop();
//        else if ((nextInt = stack.peek().next()).isInteger()) return true;
//        else stack.push(nextInt.getList().iterator());
//    }
//    return false;
//}
//
//
///**
//* // This is the interface that allows for creating nested lists.
//* // You should not implement it, or speculate about its implementation
//* public interface NestedInteger {
//*
//*     // @return true if this NestedInteger holds a single integer, rather than a nested list.
//*     public boolean isInteger();
//*
//*     // @return the single integer that this NestedInteger holds, if it holds a single integer
//*     // Return null if this NestedInteger holds a nested list
//*     public Integer getInteger();
//*
//*     // @return the nested list that this NestedInteger holds, if it holds a nested list
//*     // Return null if this NestedInteger holds a single integer
//*     public List<NestedInteger> getList();
//* }
//*/
//public class NestedIterator implements Iterator<Integer> {
//
//private Queue<Integer> q=new LinkedList<>();
//
//public NestedIterator(List<NestedInteger> nestedList) {
//    helper(q, nestedList);        
//}
//
//public void helper(Queue<Integer> q, List<NestedInteger> nestedList){
//    for(NestedInteger l: nestedList){
//        if(l.isInteger()){
//            q.offer(l.getInteger());
//        }else{
//            helper(q, l.getList());
//        }                
//    } 
//}
//
//@Override
//public Integer next() {
//    return q.poll();
//}
//
//@Override
//public boolean hasNext() {
//    return q.size()!=0;
//}
//}
//
///**
//* Your NestedIterator object will be instantiated and called as such:
//* NestedIterator i = new NestedIterator(nestedList);
//* while (i.hasNext()) v[f()] = i.next();
//*/

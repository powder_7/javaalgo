import java.util.ArrayList;
import java.util.List;

/*
 *  564
	Given an integer n, find the closest integer (not including itself), which is a palindrome.
	
	The 'closest' is defined as absolute difference minimized between two integers.
	
	Example 1:
	Input: "123"
	Output: "121"
	
	Solution: Find all 5 solutions for 5 cases. 
		case 1: 12345 -> 123 , +0 -> 12321
		case 2: 12345 -> 124 , +1 -> 12421
		case 3: 12345 -> 122 , -1 -> 12221
		case 4: 9999
		case 5: 100001
 */
public class FindTheClosestPalindrome {
	public String nearestPalindromic(String n) {
		// edge cases, no

		int len = n.length();
		int i = len % 2 == 0 ? len / 2 - 1 : len / 2;
		long left = Long.parseLong(n.substring(0, i + 1));

		// input: n 12345
		List<Long> candidate = new ArrayList<>();
		candidate.add(getPalindrome(left, len % 2 == 0));     // 12321
		candidate.add(getPalindrome(left + 1, len % 2 == 0)); // 12421
		candidate.add(getPalindrome(left - 1, len % 2 == 0)); // 12221
		candidate.add((long) Math.pow(10, len - 1) - 1);      // 9999
		candidate.add((long) Math.pow(10, len) + 1);          // 100001
 		
		long diff = Long.MAX_VALUE, res = 0, nl = Long.parseLong(n);
		for (long cand : candidate) {
			// not include itself, cloest to it*******
			if (cand == nl)
				continue;
			if (Math.abs(cand - nl) < diff) {
				diff = Math.abs(cand - nl);
				res = cand;
			} else if (Math.abs(cand - nl) == diff) {
				res = Math.min(res, cand);
			}
		}

		return String.valueOf(res);
	}

	 
	private long getPalindrome(long left, boolean even) {
		long res = left;
		if (!even)
			left = left / 10;
		while (left > 0) {
			res = res * 10 + left % 10; // res = 123 * 10 + 12 % 10 = 1230+2 = 1232
			left /= 10;
		}
		return res;
	}
 
	public static void main(String[] args) {
		
		String n = "123456";
		String s = new FindTheClosestPalindrome().nearestPalindromic(n);
		System.out.println(s);
 		
	}
}

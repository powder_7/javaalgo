import static org.junit.Assert.assertEquals;

import org.junit.Test;

/*
824. Goat Latin
A sentence S is given, composed of words separated by spaces. Each word consists of lowercase and uppercase letters only.

We would like to convert the sentence to "Goat Latin" (a made-up language similar to Pig Latin.)

The rules of Goat Latin are as follows:

If a word begins with a vowel (a, e, i, o, or u), append "ma" to the end of the word.
For example, the word 'apple' becomes 'applema'.
 
If a word begins with a consonant (i.e. not a vowel), remove the first letter and append it to the end, then add "ma".
For example, the word "goat" becomes "oatgma".
 
Add one letter 'a' to the end of each word per its word index in the sentence, starting with 1.
For example, the first word gets "a" added to the end, the second word gets "aa" added to the end and so on.
Return the final sentence representing the conversion from S to Goat Latin. 

Example 1:

Input: "I speak Goat Latin"
Output: "Imaa peaksmaaa oatGmaaaa atinLmaaaaa"
Example 2:

Input: "The quick brown fox jumped over the lazy dog"
Output: "heTmaa uickqmaaa rownbmaaaa oxfmaaaaa umpedjmaaaaaa overmaaaaaaa hetmaaaaaaaa azylmaaaaaaaaa ogdmaaaaaaaaaa"

 */
public class GoatLatin {
	public String toGoatLatin(String S) {
		String vowel = "aeiou";
		StringBuilder res = new StringBuilder();
		int count = 1;
		String[] token = S.split(" ");
		for (String t : token) {
			// first char is vowel
			if (vowel.contains(t.toLowerCase().substring(0, 1))) {
				res.append(t + "ma");
			}
			// remove first char
			// append t word and first char to the end
			else {
				res.append(t.substring(1) + t.substring(0, 1) + "ma");
			}
			for (int i = 0; i < count; i++) {
				res.append("a");
			}
			count++;
			res.append(" ");
		}
		// remove extra " " char
		res.deleteCharAt(res.length() - 1);
		return res.toString();
	}

	@Test
	public void test() {
		String S = "I speak Goat Latin";
		String exp1 = "Imaa peaksmaaa oatGmaaaa atinLmaaaaa";

		String res = toGoatLatin(S);
		assertEquals(exp1, res);
		System.out.println(res);

	}

	public static void main(String args[]) {
		new GoatLatin().test();
	}
}

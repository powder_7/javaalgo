


/*
880
An encoded string S is given.  To find and write the decoded string to a tape, the encoded string is read one character at a time and the following steps are taken:
If the character read is a letter, that letter is written onto the tape.
If the character read is a digit (say d), the entire current tape is repeatedly written d-1 more times in total.
Now for some encoded string S, and an index K, find and return the K-th letter (1 indexed) in the decoded string.

 */

/*
Example 1:
Input: S = "leet2code3", K = 10
Output: "o"
Explanation: 
The decoded string is "leetleetcodeleetleetcodeleetleetcode".
The 10th letter in the string is "o".

Example 2:
Input: S = "ha22", K = 5
Output: "h"
Explanation: 
The decoded string is "hahahaha".  The 5th letter is "h".

Example 3:
Input: S = "a2345678999999999999999", K = 1
Output: "a"
Explanation: 
The decoded string is "a" repeated 8301530446056247680 times.  The 1st letter is "a".
 */
public class DecodeStringAtIndex {
	
	/*
	 * Due to oversize length of decode/string, need to work backward.
	 * Calculate total size of string
	 * 	loop thru string, 
	 * 		if(digit(c)
	 * 			size = size * digit(c)	//size of decode string
	 * 		else size++;
	 * 
	 * Work backward.
	 * 	loop String S[n],...,S[0].
	 * 		K = K % size; // use mod bc repeats of string <-- trick
	 * 		if(K==0 && digit(c) return c;
	 * 	
	 * 		if(digit(c) // calculate backward
	 * 			size = size / c;
	 * 		else 
	 * 			size--;
	 * 
	 */
	public String decodeAtIndex(String S, int K) {
		
 		long size = 0;
		for(char c : S.toCharArray()) {
			if(Character.isDigit(c)) {
				size *= c-'0'; // char to int
			}
			else {
				size++;
			}
		}
		for(int i=S.length()-1; i >=0; i--) {
			K = (int) (K % size);	//trick
			char c = S.charAt(i);
			if(K == 0 && Character.isLetter(c)) {
				return ""+c;
			}
			else if(Character.isDigit(c)) {
				size = size / (c-'0'); //work backward
			}
			else {
				size--;
			}
		}
 
		return null;
	}

	
	public String decodeAtIndex1(String S, int K) {
        long size = 0;
        int N = S.length();

        // Find size = length of decoded string
        for (int i = 0; i < N; ++i) {
            char c = S.charAt(i);
            if (Character.isDigit(c))
                size *= c - '0';
            else
                size++;
        }

        for (int i = N-1; i >= 0; --i) {
            char c = S.charAt(i);
            K %= size;
            if (K == 0 && Character.isLetter(c))
                return Character.toString(c);

            if (Character.isDigit(c))
                size /= c - '0';
            else
                size--;
        }

        throw null;
    }
	public static void main(String[] args) {
		
//		String S = "leet2code3";
//		int K = 10;
		
//		String S = "ha22";
//		int K = 5;
		
//		String S = "a2345678999999999999999";
		String S = "a234567899999";
		int K =1;
		
		String res = new DecodeStringAtIndex().decodeAtIndex(S, K);
		System.out.println(res);

	}
}

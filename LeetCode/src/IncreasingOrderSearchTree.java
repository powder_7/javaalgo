import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

/*897. Increasing Order Search Tree
 * Given a tree, rearrange the tree in in-order so that the leftmost node in 
 * the tree is now the root of the tree, and 
 * every node has no left child and only 1 right child.
 * 
Example 1:
Input: [5,3,6,2,4,null,8,1,null,null,null,7,9]

       5
      / \
    3    6
   / \    \
  2   4    8
 /        / \ 
1        7   9

Output: [1,null,2,null,3,null,4,null,5,null,6,null,7,null,8,null,9]

 1
  \
   2
    \
     3
      \
       4
        \
         5
          \
           6
            \
             7
              \
               8
                \
                 9  

 */
/*
 * do dfs, then create a new tree
 * res is null
		curr = new TreeNode(x);
		res = curr;
   else 
   		curr.right = new TreeNode(x);
   		curr = curr.right
   	  
 */
public class IncreasingOrderSearchTree {

	// Definition for a binary tree node.
	public class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode(int x) {
			val = x;
		}
	}

	TreeNode root = null;
	TreeNode curr_node = null;
	TreeNode res = null;

	public void builBST() {
		int a[] = { 5, 3, 6, 2, 4, 8, 1, 7, 9 };
		// int a[] = {5,3,6,2,4};
		// int a[] = {379, 826};
		for (int x : a)
			root = makeBST(root, x);

		// printDFS(root);
		TreeNode res = increasingBST(root);
		// dfs(root);
		// printDFS(res);
		printRightTree(res);
	}

	public void printDFS(TreeNode root) {
		if (root == null)
			return;
		if (root.left != null)
			printDFS(root.left);
		System.out.println(root.val);
		if (root.right != null)
			printDFS(root.right);

	}

	public void printRightTree(TreeNode r) {
		while (r != null) {
			System.out.println(r.val);
			r = r.right;
		}
	}

	TreeNode makeBST(TreeNode root, int x) {
		if (root == null)
			return new TreeNode(x);
		if (x < root.val)
			root.left = makeBST(root.left, x);
		else
			root.right = makeBST(root.right, x);	
		return root;
	}

	public TreeNode increasingBST(TreeNode root) {
		dfs(root);
		return res;
	}

	public void dfs(TreeNode n) {
		if (n == null)
			return;
		dfs(n.left);
		if (res == null) {
			curr_node = new TreeNode(n.val);
			res = curr_node;

		} else {
			curr_node.right = new TreeNode(n.val);
			curr_node = curr_node.right;
		}

		dfs(n.right);
	}

	public static void main(String[] args) {

		IncreasingOrderSearchTree test = new IncreasingOrderSearchTree();
		test.builBST();

	}
}

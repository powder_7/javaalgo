import java.util.HashSet;
import java.util.Set;

/*
 * 961. N-Repeated Element in Size 2N Array
 * https://leetcode.com/problems/n-repeated-element-in-size-2n-array/
 In a array A of size 2N, there are N+1 unique elements, and exactly one of these elements is repeated N times.
Return the element repeated N times.
Example 1:

Input: [1,2,3,3]
Output: 3
Example 2:

Input: [2,1,2,5,3,2]
Output: 2
Example 3:

Input: [5,1,5,2,5,3,5,4]
Output: 5
Note:

4 <= A.length <= 10000
0 <= A[i] < 10000
A.length is even

 */
public class NRepeatedElementInSize2NArray {
	public int repeatedNTimes(int[] A) {
		Set<Integer> set = new HashSet<>();

		for (int x : A) {
			// contains x or add not successful
			if (!set.add(x)) {
				return x;
			}
		}
		return -1;
	}

	public static void main(String args[]) {
		int A[] = {5,1,5,2,5,3,5,4};
		System.out.println(new NRepeatedElementInSize2NArray().repeatedNTimes(A));
	}
}

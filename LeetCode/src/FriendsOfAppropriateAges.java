import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/*  825
 *  Friends Of Appropriate Ages
 *  Some people will make friend requests. The list of their ages is given and ages[i] is the age of the ith person. 

Person A will NOT friend request person B (B != A) if any of the following conditions are true:

age[B] <= 0.5 * age[A] + 7
age[B] > age[A]
age[B] > 100 && age[A] < 100
Otherwise, A will friend request B.

Note that if A requests B, B does not necessarily request A.  Also, people will not friend request themselves.

How many total friend requests are made?

Example 1:

Input: [16,16]
Output: 2
Explanation: 2 people friend request each other.
Example 2:

Input: [16,17,18]
Output: 2
Explanation: Friend requests are made 17 -> 16, 18 -> 17.
Example 3:

Input: [20,30,100,110,120]
Output: 3
Explanation: Friend requests are made 110 -> 100, 120 -> 110, 120 -> 100.
 */
/*
 * Friend if the following true
A = .5*ages+7 < B 
A > B
B <=100 || A >=100
A=B
 */
/*
 * answer: loop thru ages hashmap(age, repeat)
 * 
 * request fucntion() {
	     return  not(
		 age[B] <= 0.5 * age[A] + 7
		 age[B] > age[A]
		 age[B] > 100 && age[A] < 100)
		 )
  if meet requirement
  	res += khashmap.get(age) * (hashmap.get(age) - A==B?0:1)
 */
public class FriendsOfAppropriateAges {

	public int numFriendRequests(int[] ages) {
		
		Map<Integer, Integer> hm = new HashMap<>();
		int res = 0;
		for(int x : ages) {
			hm.put(x, hm.getOrDefault(x, 0) +1);
		}
		
		for(int a : hm.keySet()) {
			for(int b: hm.keySet()) {
				if(request(a,b)) {
					res += hm.get(a) * ( hm.get(b) - ((a==b)?1:0) );
				}
			}
		}
		
		return res;
	}
	
	public boolean request(int a, int b) {
		
		return !(b <= a*.5 +7 || b >a || (b > 100 && a < 100));
	}
	
    public int numFriendRequestss(int[] ages) {
        int[] count = new int[121];
        for (int age: ages) count[age]++;

        int ans = 0;
        for (int ageA = 0; ageA <= 120; ageA++) {
            int countA = count[ageA];
            for (int ageB = 0; ageB <= 120; ageB++) {
                int countB = count[ageB];
                if (ageA * 0.5 + 7 >= ageB) continue;
                if (ageA < ageB) continue;
                if (ageA < 100 && 100 < ageB) continue;
                ans += countA * countB;
                if (ageA == ageB) ans -= countA;
            }
        }

        return ans;
    }
	
	public static void main(String[] args) {
		int ages[] = {16,16}; //2
//		int ages[] = {16,17,18}; //2 should be 3?
//		int ages[] = {20,30,100,110,120};//3
		int res = new FriendsOfAppropriateAges().numFriendRequests(ages);
		System.out.println(res);
	}
	
}

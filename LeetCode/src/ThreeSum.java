import java.util.*;

/*
 15. 3Sum
 Given an array nums of n integers, are there elements a, b, c in nums such that a + b + c = 0? 
 Find all unique triplets in the array which gives the sum of zero.
Note:

The solution set must not contain duplicate triplets.

Given array nums = [-1, 0, 1, 2, -1, -4],
sort = [-1, -1, 0, 1, 1, 2, 2, 4]
A solution set is:
[
  [-1, 0, 1],
  [-1, -1, 2]
]
 */
/*
 * answer:
 * Arrays.sort(array)
 * loop array i = 0 to len-2
 * 	j = i+1
 * 	k = len-1
 * 		while(j < k)
 * 			0==sum= array[i]+ array[j] +array[k]
 * 			res.add(Arrays.asList(array[i],array[j++],array[k--])
 * 			else if sum > 0 k--
 * 			else sum < 0 j 
 */
public class ThreeSum {

	public List<List<Integer>> threeSum(int[] nums) {

		Arrays.sort(nums);
		Set<List<Integer>> set = new HashSet<>();
		
		for(int i = 0; i < nums.length-2; i++) {
			int j = i+1;
			int k = nums.length-1;
			while(j < k) {
				int sum = nums[i] + nums[j] + nums[k];
				if(sum == 0) set.add(Arrays.asList(nums[i], nums[j++], nums[k--]));
				else if(sum > 0) k--;
				else if(sum < 0) j++;
			}
		}
		
		return new ArrayList<>(set);
		
	}

	public static void main(String args[]) {
		int[] nums = {-1, 0, 1, 2, 1, 2, -1, -4};
		List<List<Integer>> res = new ThreeSum().threeSum(nums);
		System.out.println(res);
	}
}

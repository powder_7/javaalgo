import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

/*
767. Reorganize String
Given a string S, check if the letters can be rearranged so that two characters that are adjacent to each other are not the same.

If possible, output any possible result.  If not possible, return the empty string.

Example 1:

Input: S = "aab"
Output: "aba"
Example 2:

Input: S = "aaab"
Output: ""

Solution: get the most repeat 
		append to string
		update most repeat
		repeat
 */
public class ReorganizeString {

	public String reorganizeString(String S) {
		StringBuilder res = new StringBuilder();

		Map<Character, Integer> hm = new HashMap<>();
		for (char c : S.toCharArray()) {
			hm.put(c, hm.getOrDefault(c, 0) + 1);
		}

		PriorityQueue<Map.Entry<Character, Integer>> pq = new PriorityQueue<>((a, b) -> b.getValue() - a.getValue());
		pq.addAll(hm.entrySet());
		Map.Entry<Character, Integer> prev = null;

		while (!pq.isEmpty()) {
			Map.Entry<Character, Integer> entry = pq.poll();
			if (prev != null && prev.getValue() > 0) {
				pq.offer(prev);
			}
			res.append((char) entry.getKey());
			entry.setValue(entry.getValue() - 1);

			prev = entry;
		}

		return (res.length() == S.length()) ? res.toString() : "";
	}

	public String reorganizeString1(String S) {
		int[] map = new int[26];
		StringBuilder res = new StringBuilder();

		for (int i = 0; i < S.length(); i++) {
			map[S.charAt(i) - 'a']++;
		}
		PriorityQueue<int[]> pq = new PriorityQueue<>((a, b) -> b[1] - a[1]);

		for (int i = 0; i < 26; i++) {
			if (map[i] != 0)
				pq.add(new int[] { i, map[i] });

		}
		// [0] = key, [1] = value
		int[] prev = { 0, 0 };
		while (pq.size() > 0) {
			int[] curr = pq.poll();
			if (prev[1] > 0)
				pq.add(prev);
			res.append((char) (curr[0] + 'a'));
			curr[1]--;
			prev = curr;

		}

		return res.length() == S.length() ? res.toString() : "";
	}

	public void test() {
		String S = "vlvov";
		// String S = "aaaabbbbcccddd";
		// String S = "aaab";

		// String S = "zrhmhyevkojpsegvwolkpystdnkyhcjrdvqtyhucxdcwm";
		// String Output = "hjhmhmhoyoypypyrcrcscsdtdtdwkwkgklvnvqvuexezj";

		String res = reorganizeString(S);
		String res1 = reorganizeString1(S);

		System.out.println(res + "  " + res1);
		assertEquals(res, res1);

	}

	public static void main(String args[]) {
		new ReorganizeString().test();
	}
}

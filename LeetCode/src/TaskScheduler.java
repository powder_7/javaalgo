import java.util.Arrays;
/* 621. Task Scheduler
 * https://leetcode.com/problems/task-scheduler/solution/
 *  
Given a char array representing tasks CPU need to do. 
It contains capital letters A to Z where different letters represent different tasks. 
Tasks could be done without original order. Each task could be done in one interval. 
For each interval, CPU could finish one task or just be idle.

However, there is a non-negative cooling interval n that means between two same tasks, 
there must be at least n intervals that CPU are doing different tasks or just be idle.

You need to return the least number of intervals the CPU will take to finish all the given tasks.

 

Example:

Input: tasks = ["A","A","A","B","B","B"], n = 2
Output: 8
Explanation: A -> B -> idle -> A -> B -> idle -> A -> B. 

["A","A","A","A","B","B","B","B", "C","C","C", "D"]
4
result = 17

tasks ={"A", "A", "A", "B", "B", "B"} n = 2

step 1. initialize , x is idle
Axx
Axx
Axx
As you can see the total column is 3, so n+1 = 2+1. Thus 3 columns

step 2. fill in x
ABx
ABx
AB <-------(don't need to idle because task is done)
You see the max repeat is 3. You have AAA and BBB, thus p = 2. Finally, max - 1, cause you don't need to idle at the last row.
To find the area or total task and ideal. Total = (row)* (column) + (number of max) = (max-1)(n+1)+p = (3-1)(2+1)+2. Thus 2row*3column + 2 extra = 8 task and idle.

If not enough n idle or don't need to idle then return tasks length
 */
public class TaskScheduler {
	/*
	public int leastInterval(char[] tasks, int n) {
        Map<Character, Integer> map = new HashMap<>();//char array is better, I just want to make this answer easier to read.
        for (char c : tasks) {
            map.put(c, map.getOrDefault(c, 0) + 1);
        }
        
        int max = 0;//Most frequent task.
        for (int val : map.values()) {
            max = Math.max(val, max);
        }
        
        int p = 0;//how many tasks that has the same frequency as the top frequent task.(include itself)
        for (int val : map.values()) {
            if (val == max) {
                p++;
            }
        }
            
        int total = (max - 1) * (n + 1) + p;//Totally intervals to fill out all empty space.
        
        if (total < tasks.length) {
            return tasks.length; //After I fill out all empty space, there are still some tasks that I have not use them.
        } else {
            return total; //Task is not enough, I used some idles.
        }
    }
	*/

	public int leastInterval(char[] tasks, int n) {

		int[] c = new int[26];
		for (char t : tasks) {
			c[t - 'A']++;
		}
		Arrays.sort(c);
		int i = 25;
		while (i >= 0 && c[i] == c[25])
			i--;

		return Math.max(tasks.length, (c[25] - 1) * (n + 1) + 25 - i);
	}
	
	public static void test() {
		char [] tasks = {'A','A','A','B','B','B','C'};
		int n = 2; 
		int result = new TaskScheduler().leastInterval(tasks, n);
		System.out.print(result);
	}	
	
	public static void main(String[] args) {
		test();
	}
}

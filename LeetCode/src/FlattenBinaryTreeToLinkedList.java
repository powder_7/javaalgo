import java.util.Stack;


/*
 114. Flatten Binary Tree to Linked List
 Given a binary tree, flatten it to a linked list in-place.

For example, given the following tree:

    1
   / \
  2   5
 / \   \
3   4   6
The flattened tree should look like:

1
 \
  2
   \
    3
     \
      4
       \
        5
         \
          6
 */
public class FlattenBinaryTreeToLinkedList {

	// Definition for a binary tree node.
	public class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode(int x) {
			val = x;
		}
	}
	TreeNode res = null;
	/*
	 * DFS with stack
	 * set curr.left to null
	 * set curr.right to stack peek if not empty
	 */
	public void flatten(TreeNode root) {
		
		Stack<TreeNode> st = new Stack<>();
		st.push(root);
		while(!st.isEmpty()) {
			TreeNode curr = st.pop();
			
			if(curr.right != null) st.push(curr.right);
			if(curr.left != null) st.push(curr.left);
			
			curr.left = null;
			if(!st.isEmpty()) {
				curr.right = st.peek();
			}
		}

	}
	
	public void postOrder(TreeNode r) {
		if(r==null) return;
		
		postOrder(r.right);
		postOrder(r.left);
		res.left = null;
		res.right = r;
		res = r;
		
	}

	public static void main(String[] args) {

	}

}

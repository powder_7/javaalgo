import static org.junit.Assert.assertEquals;

import java.util.Stack;

import org.junit.Test;

/*844. Backspace String Compare
Given two strings S and T, return if they are equal when both are typed into empty text editors. # means a backspace character.

Example 1:

Input: S = "ab#c", T = "ad#c"
Output: true
Explanation: Both S and T become "ac".
Example 2:

Input: S = "ab##", T = "c#d#"
Output: true
Explanation: Both S and T become "".
Example 3:

Input: S = "a##c", T = "#a#c"
Output: true
Explanation: Both S and T become "c".
Example 4:

Input: S = "a#c", T = "b"
Output: false
Explanation: S becomes "c" while T becomes "b".
Note:

1 <= S.length <= 200
1 <= T.length <= 200
S and T only contain lowercase letters and '#' characters.
Follow up:

Can you solve it in O(N) time and O(1) space?
 */
public class BackspaceStringCompare {

	public boolean backspaceCompare1(String S, String T) {
		String s = getBackSpace(S);
		String t = getBackSpace(T);

		return s.equals(t);
	}

	public String getBackSpace(String str) {

		StringBuilder s = new StringBuilder();

		for (char c : str.toCharArray()) {
			if ('#' == c) {
				if (s.length() > 0) {
					s.deleteCharAt(s.length() - 1);
				}
			} else {
				s.append(c);
			}
		}
		return s.toString();
	}

	public boolean backspaceCompare(String S, String T) {

		Stack<Character> S1 = new Stack<>();
		Stack<Character> T1 = new Stack<>();
		for (char c : S.toCharArray()) {
			if ('#' == c) {
				if (!S1.isEmpty())
					S1.pop();
			} else
				S1.push(c);
		}
		for (char c : T.toCharArray()) {
			if ('#' == c) {
				if (!T1.isEmpty())
					T1.pop();
			} else
				T1.push(c);
		}

		return S1.equals(T1);
	}
	@Test
	public void test() {
		String S = "a##c", T = "#a#c";
		boolean res = backspaceCompare1(S, T);
		assertEquals(false, res);
		System.out.println(res);
	}

	public static void main(String args[]) {
		new BackspaceStringCompare().test();
	}
}

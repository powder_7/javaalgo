import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

// 862. Shortest Subarray with Sum at Least K. Sum is at K or larger
public class ShortestSubarrayWithSumAtLeastK {
	
	public int shortestSubarray(int[] A, int K) {
        int N = A.length, res = N + 1;
        int[] B = new int[N + 1];
        for (int i = 0; i < N; i++) B[i + 1] = B[i] + A[i];
        Deque<Integer> d = new ArrayDeque<>();
        for (int i = 0; i < N + 1; i++) {
            while (d.size() > 0 && B[i] - B[d.getFirst()] >=  K)
                res = Math.min(res, i - d.pollFirst());
            while (d.size() > 0 && B[i] <= B[d.getLast()]) d.pollLast();
            d.addLast(i);
        }
        return res <= N ? res : -1;
    }

	public int shortestSubarray2(int[] A, int K) {

		Map<Integer, Integer> hm = new HashMap<>();
		int current = 0;
		int result = -1;
		// hm.put(A[0],1);
		hm.put(0, 0);
		for (int i = 1; i <= A.length; i++) {
			current += A[i - 1];
			if (hm.containsKey(current - K)) {
				return i - hm.get(current - K);
			}
			hm.put(current, i);

		}

		return result;
	}
	
	public int solution (int[] A, int K) {
        int N = A.length;
        long[] P = new long[N+1];
        for (int i = 0; i < N; ++i)
            P[i+1] = P[i] + (long) A[i];

        // Want smallest y-x with P[y] - P[x] >= K
        int ans = N+1; // N+1 is impossible
        Deque<Integer> monoq = new LinkedList(); //opt(y) candidates, as indices of P

        for (int y = 0; y < P.length; ++y) {
            // Want opt(y) = largest x with P[x] <= P[y] - K;
            while (!monoq.isEmpty() && P[y] <= P[monoq.getLast()])
                monoq.removeLast();
            while (!monoq.isEmpty() && P[y] >= P[monoq.getFirst()] + K)
                ans = Math.min(ans, y - monoq.removeFirst());

            monoq.addLast(y);
        }

        return ans < N+1 ? ans : -1;
    }
	public static void main(String [] args) {
		int A[] = {48,99,37,4,-31};
		int K = 140;
		//output is 2
		
//		int A[] = {2,-1,2};
//		int K = 3;
//		// output 3
		ShortestSubarrayWithSumAtLeastK test = new ShortestSubarrayWithSumAtLeastK();
		int result = test.shortestSubarray(A, K);
		System.out.println(result);
		
		result = test.shortestSubarray2(A, K);
		System.out.println(result);
		
		result = test.solution(A, K);
		System.out.println(result);
		
	}
}
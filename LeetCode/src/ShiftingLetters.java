import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ShiftingLetters {

	//time expired too long
	public String shiftingLetters(String S, int[] shifts) {

		String alphabet = "abcdefghijklmnopqrstuvwxyz";
		// String res = "";
		Map<Character, Integer> hm = new HashMap<>();
		StringBuilder res = new StringBuilder(S);
		for (int i = 0; i < alphabet.length(); i++) {
			hm.put(alphabet.charAt(i), hm.getOrDefault(alphabet.charAt(i), 0) + i + 1);
		}

//		 for(Entry<Character, Integer> entry : hm.entrySet()) {
//		 System.out.println(entry);
//		 }
// 
		for (int j = 0; j < shifts.length; j++) {
			// res.setCharAt(j, S.charAt(j) );
			for (int k = 0; k <= j; k++) {
				int offset = (hm.get(res.charAt(k)) + shifts[j]) %26;
				 
				if(offset==0)
					res.setCharAt(k, alphabet.charAt(25));
				else
					res.setCharAt(k, alphabet.charAt(offset - 1));

			}
		}
 

		return res.toString();
	}

	public String shiftingLetters1(String S, int[] shifts) {
		
		return "";
	}
	
	public static void main(String[] args) {

//		String S = "abc";
//		int shifts[] = { 3, 5, 9 };
		
//		String S = "bad";
//		int shifts[] = {10, 20, 30};
		
//		String S = "z";
//		int shifts[] = {52};
		
		String S = "zvhez";
		int shifts[] = {10,16,10,26,26};  //"jvrez"
	
 
		String res = new ShiftingLetters().shiftingLetters(S, shifts);
		
		System.out.println(res);
		
	}
}

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

/*
733. Flood Fill
An image is represented by a 2-D array of integers, each integer representing the pixel value of the image (from 0 to 65535).

Given a coordinate (sr, sc) representing the starting pixel (row and column) of the flood fill, and a pixel value newColor, "flood fill" the image.

To perform a "flood fill", consider the starting pixel, plus any pixels connected 4-directionally to the starting pixel of the same color as the starting pixel, 
plus any pixels connected 4-directionally to those pixels (also with the same color as the starting pixel), and so on. 
Replace the color of all of the aforementioned pixels with the newColor.

At the end, return the modified image.

Example 1:
Input: 
image = [[1,1,1],[1,1,0],[1,0,1]]
sr = 1, sc = 1, newColor = 2
Output: [[2,2,2],[2,2,0],[2,0,1]]
Explanation: 
From the center of the image (with position (sr, sc) = (1, 1)), all pixels connected 
by a path of the same color as the starting pixel are colored with the new color.
Note the bottom corner is not colored 2, because it is not 4-directionally connected
to the starting pixel.
Note:

The length of image and image[0] will be in the range [1, 50].
The given starting pixel will satisfy 0 <= sr < image.length and 0 <= sc < image[0].length.
The value of each color in image[i][j] and newColor will be an integer in [0, 65535].
 */
public class FloodFill {
	public int[][] floodFill(int[][] image, int sr, int sc, int newColor) {

		int color = image[sr][sc];
		if (newColor == color)
			return image;
	 	
//		bfs(image,  sr,  sc,  newColor);
		dfs(image, sr, sc, color, newColor );
		return image;
	}
	
	public int[][]bfs(int[][] image, int sr, int sc, int newColor) {

		int color = image[sr][sc];
		if (newColor == color)
			return image;
		Queue<int[]> q = new LinkedList<>();
		// y,x
		int[] coord = { sr, sc };
		q.add(coord);
		while (!q.isEmpty()) {
			coord = q.poll();
			int y = coord[0];
			int x = coord[1];
			image[y][x] = newColor;

			// top, y
			if (y > 0 && image[y - 1][x] == color) {
				q.add(new int[] { y - 1, x });
			}
			// bottom
			if (y < image.length - 1 && image[y + 1][x] == color) {
				q.add(new int[] { y + 1, x });
			}
			// left
			if (x > 0 && image[y][x - 1] == color) {
				q.add(new int[] { y, x - 1 });
			}
			// right
			if (x < image[0].length - 1 && image[y][x + 1] == color) {
				q.add(new int[] { y, x + 1 });
			}
		}
		// for(int y = 0; y < image.length; y++) {
		// for(int x = 0; x < image[0].length; x++) {
		// System.out.print(image[y][x]);
		// }
		// System.out.println("");
		// }

		return image;
	}
	public void dfs(int[][]image, int sr, int sc, int color, int newColor) {
		if(image[sr][sc] == color) {
			image[sr][sc] = newColor;
			//top
			if(sr > 0) dfs(image, sr-1, sc, color, newColor);
			//bottom
			if(sr < image.length-1) dfs(image, sr+1, sc, color, newColor);
			//left
			if(sc > 0) dfs(image, sr, sc-1, color, newColor);
			//right
			if(sc < image[0].length-1) dfs(image, sr, sc+1, color, newColor);
		}
	}
	
	public void test() {
		int[][]image = {{1,1,1},{1,1,0},{1,0,1}};
		 
		int sr = 1, sc = 1, newColor = 2;
		System.out.println(Arrays.deepToString(image));
		floodFill(image, sr, sc, newColor );
		System.out.println(Arrays.deepToString(image));
	}
	
	public static void main(String args[]) {
		 new FloodFill().test();
		
	}
}

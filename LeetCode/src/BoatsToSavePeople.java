import java.util.Arrays;

/*
 881
The i-th person has weight people[i], and each boat can carry a maximum weight of limit.
Each boat carries at most 2 people at the same time, provided the sum of the weight of those people is at most limit.
Return the minimum number of boats to carry every given person.  (It is guaranteed each person can be carried by a boat.)
 */

/*
Example 1:
Input: people = [1,2], limit = 3
Output: 1
Explanation: 1 boat (1, 2)

Example 2:
Input: people = [3,2,2,1], limit = 3
Output: 3
Explanation: 3 boats (1, 2), (2) and (3)

Example 3:
Input: people = [3,5,3,4], limit = 5
Output: 4
Explanation: 4 boats (3), (3), (4), (5)
 */

//sort first, then use two pointer,
//if left and right sum larger than a boat, only put right on a boat
//otherwise could put both on a boat.
//each loop a new boat is added to result.
//time O(nlogn)
public class BoatsToSavePeople {

	public int numRescueBoats(int[] people, int limit) {
		 
		Arrays.sort(people);
		int res = 0;
		System.out.println(Arrays.toString(people));
		int i = 0;
		int j = people.length-1;
		while(i<=j) {

			// heavy put right on boat
			if(people[i]+ people[j] > limit) {
				j--;
			}
			// put both on boat
			else {
				i++;
				j--;
			}
			res++;
		}
		
 		return res;
	}

	public static void main(String[] arg) {
//		int people[] = {1,2}; //1
//		int people[] = {3,2,2,1}; //3
//		int limit = 3;
		
		int people[] = {3,5,3,4}; //4
		int limit = 5;
		
		int res = new BoatsToSavePeople().numRescueBoats(people, limit);
		System.out.println(res);
	}
}

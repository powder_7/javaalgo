import java.util.List;

/*
 https://leetcode.com/problems/remove-invalid-parentheses/description/
301. Remove Invalid Parentheses
Remove the minimum number of invalid parentheses in order to make the input string valid. Return all possible results.

Note: The input string may contain letters other than the parentheses ( and ).

Example 1:

Input: "()())()"
Output: ["()()()", "(())()"]
Example 2:

Input: "(a)())()"
Output: ["(a)()()", "(a())()"]
Example 3:

Input: ")("
Output: [""]
 */
public class RemoveInvalidParentheses {
	public List<String> removeInvalidParentheses(String s) {
    
		return null;
    }
	
	public void test() {
		String s = "()())()";
		List<String> list = removeInvalidParentheses(s);
		System.out.println(list);
	}
	
	public static void main(String[] args) {
		
		new RemoveInvalidParentheses().test();
		
	}
}

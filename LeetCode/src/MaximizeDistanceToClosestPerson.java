import java.util.ArrayList;
import java.util.List;

/*
 
849
In a row of seats, 1 represents a person sitting in that seat, and 0 represents that the seat is empty. 
There is at least one empty seat, and at least one person sitting.
Alex wants to sit in the seat such that the distance between him and the closest person to him is maximized. 
Return that maximum distance to closest person.
 */

public class MaximizeDistanceToClosestPerson {
	/*
	 * Store all the zero(array index) in list
	 * loop the list of list 
	 * 	if contains 0 or length-1
	 * 		max(max, innerlist.size()
	 *  else 
	 * 		max(max, innnerlist.size()+1/2)
	 */
	public int maxDistToClosest(int[] seats) {
		int max = Integer.MIN_VALUE;
		int res = 0;
		List<List<Integer>> list = new ArrayList<>();
		List<Integer> tmp = new ArrayList<>();
		for (int i = 0; i < seats.length; i++) {
			if (seats[i] == 0) {
				tmp.add(i);
			} else if (!tmp.isEmpty()) {
				list.add(tmp);
				tmp = new ArrayList<>();
			}
		}
		// need this incase last item is zero.
		if (!tmp.isEmpty())
			list.add(tmp);
		for (List<Integer> l : list) {
			if (l.contains(0) || l.contains(seats.length - 1)) {
				max = Math.max(max, l.size());
			} else {
				max = Math.max(max, (l.size() + 1) / 2);
			}
		}
		res = max;
		return res;
	}

	// loop index i of all the 1 until zero start.
	// assign j = i, this is where zero start
	// loop until zero change to 1
	// if j=0 || i==length //0 found at index 0 or at the end
	// max = distance of j-1
	// else max = distance/2 or (j-1+1)/2
	public int maxDistToClosest2(int[] nums) {
		int n = nums.length;
		int max = 0;
		int i = 0;
		while (i < n) {
			while (i < n && nums[i] == 1) {
				i++;
			}
			int j = i;// start
			while (i < n && nums[i] == 0) {
				i++;
			}
			if (j == 0 || i == n) {
				max = Math.max(max, i - j);
			} else {
				max = Math.max(max, (i - j + 1) / 2);
				System.out.println("i = " + i + " j = " + j);
			}
		}
		return max;
	}

	public static void main(String[] args) {

		int a[] = { 0, 0, 0, 0 };// 4
		// int a[] = {1,0,0,0,1,0,1};//2

		// int a[] = {1,1,0,0,0,1,0};//2

		// int a[] = {0,1,0,0,0,1,1,0,1,1};//2

		// int a[] = {0,0,1};//2

		// int a[] = {1,0,0,0};//3

		int s = new MaximizeDistanceToClosestPerson().maxDistToClosest2(a);
		System.out.println(s);

	}
}

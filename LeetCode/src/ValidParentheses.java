import java.util.Stack;

/*
 20. Valid Parentheses
 Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

An input string is valid if:

Open brackets must be closed by the same type of brackets.
Open brackets must be closed in the correct order.
Note that an empty string is also considered valid.
 */
/*
Example 1:

Input: "()"
Output: true
Example 2:

Input: "()[]{}"
Output: true
Example 3:

Input: "(]"
Output: false
Example 4:

Input: "([)]"
Output: false
Example 5:

Input: "{[]}"
Output: true
 */
public class ValidParentheses {
	public boolean isValid(String s) {

		Stack<Character> st = new Stack<>();

		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c == '(' || c == '[' || c == '{') {
				st.push(c);
			} else if (c == ')') {
				if (st.empty() || st.pop() != '(')
					return false;
			} else if (c == ']') {
				if (st.empty() || st.pop() != '[')
					return false;
			} else if (c == '}') {
				if (st.empty() || st.pop() != '{')
					return false;
			}

		}
		return (st.isEmpty() ? true : false);
	}

	public void test() {
		String[] s = { "{[]}", "{[}]" };
		for (String str : s) {
			System.out.println(str + " " + isValid(str));
		}
	}

	public static void main(String args[]) {

		new ValidParentheses().test();

	}
}

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/*
 * 859. Buddy Strings
 * Given two strings A and B of lowercase letters, return true if and only if we can swap two letters in A so that the result equals B.
 * 
Example 1:
Input: A = "ab", B = "ba"
Output: true
Example 2:

Input: A = "ab", B = "ab"
Output: false
Example 3:

Input: A = "aa", B = "aa"
Output: true
Example 4:

Input: A = "aaaaaaabc", B = "aaaaaaacb"
Output: true
Example 5:

Input: A = "", B = "aa"
Output: false
 */

/*
If A.length() != B.length(): no possible swap

If A == B, we need swap two same characters. Check is duplicated char in A.

In other cases, we find index for A[i] != B[i]. There should be only 2 diffs and it's our one swap.

 */
public class BuddyStrings {

	public boolean buddyStrings(String A, String B) {
		
		if (A.length() != B.length())   return false;
        
		// A=B true if set.size < A.length()
		// else false
        if (A.equals(B)) {
            Set<Character> set = new HashSet<>();
            for (char ch : A.toCharArray()) 
            	set.add(ch);
            return set.size() < A.length();
        }
        
        List<Integer> diff = new ArrayList<>();
        
        for (int i = 0; i < A.length(); i++) {
            if (A.charAt(i) != B.charAt(i)) {
                diff.add(i);
                if (diff.size() > 2)    return false;
            }
        }
        // difference is 2 and can swap
        return diff.size() == 2 && 
            A.charAt(diff.get(0)) == B.charAt(diff.get(1)) && 
            A.charAt(diff.get(1)) == B.charAt(diff.get(0));
    
	}

	public static void main(String[] args) {

//		//true
//		String A = "ab";
//		String B = "ba";
		
//		//true
//		String A = "aa";
//		String B = "aa";
		
//		//true
//		String A = "aaaaaaabc";
//		String B = "aaaaaaacb";
				
//		//false
		String A = "ab";
		String B = "ab";
		
		//false
//		String A = "abc";
//		String B = "acd";
		
		//false
//		String A = "abab";
//		String B = "abab";
		
		boolean res = new BuddyStrings().buddyStrings(A, B);
		System.out.println(res);
		
	}
}

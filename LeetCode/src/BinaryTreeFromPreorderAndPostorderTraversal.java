import java.util.LinkedList;
import java.util.Queue;

/*
 889 
Return any binary tree that matches the given preorder and postorder traversals.

Values in the traversals pre and post are distinct positive integers.

Example 1:

Input: pre = [1,2,4,5,3,6,7], post = [4,5,2,6,7,3,1]
Output: [1,2,3,4,5,6,7]

 */

/* pre =  [1, 2,4,5,  3,6,7]
 *      Root  |--L-|- ---R--|
 *       
 * post =   [4,5,2, 6,7,3, 1]
 *          |--L--|--R---| Root
 *          
 * Root is pre[0] or post[n]
 * L = all the node up to pre[1] or 2 from post
 * L = {4,5,2}
 * R = {6,7,3}// all element from node 2 to post n-1
 * 
 * recurisve make node bottom up
 * base case 
	 * postS > postE return null
	 * postS == postE return root;
 * 
 */
public class BinaryTreeFromPreorderAndPostorderTraversal {
	// Definition for a binary tree node.
	public class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode(int x) {
			val = x;
		}
	}

	int preStart = 0;

	public TreeNode constructFromPrePost(int[] pre, int[] post) {

		if (pre.length != post.length || pre.length <= 0)
			return null;

		preStart = 0;

		return buildTree(pre, post, 0, post.length - 1);
		// return constructFromPrePostFrom(pre, post, 0, post.length - 1);
		// return dfs(pre, 0, pre.length - 1, post, 0, post.length - 1);
	}

	public TreeNode buildTree(int[] pre, int[] post, int postS, int postE) {

		if (postS > postE || preStart > pre.length-1)
			return null;

		// root
		int r = pre[preStart];
		TreeNode root = new TreeNode(r);
		preStart++; // move to next child or node to create

		if (postS == postE)
			return root;

		// find index of left subtree in post
		int mid = pre[preStart];
		int sub_indx = postS;
		while (sub_indx <= postE) {
			if (post[sub_indx] == mid)
				break;
			sub_indx++;
		}
		
		root.left = buildTree(pre, post, postS, sub_indx); // 0 to mid or left subtree
		root.right = buildTree(pre, post, sub_indx + 1, postE - 1); // mid to n-1 or right subtree offset the root by postE-1

		return root;

	}

	public TreeNode constructFromPrePostFrom(int[] pre, int[] post, int postStart, int postEnd) {

		if (postStart > postEnd) {
			return null;
		}

		int rootVal = post[postEnd];
		TreeNode root = new TreeNode(rootVal);
		preStart++;

		if (preStart == pre.length || postStart == postEnd) {
			return root;
		}

		int leftVal = pre[preStart];
		int lri = postStart;
		for (; lri <= postEnd; lri++) {
			if (post[lri] == leftVal) {
				break;
			}
		}

		root.left = constructFromPrePostFrom(pre, post, postStart, lri);
		root.right = constructFromPrePostFrom(pre, post, lri + 1, postEnd - 1);

		return root;
	}

	private TreeNode dfs(int[] pre, int ps, int pe, int[] post, int pps, int ppe) {
		if (ps > pe || pps > ppe)
			return null;
		TreeNode root = new TreeNode(pre[ps]);
		if (ps + 1 > pe)
			return root;
		// this is the start of the left tree
		int val = pre[ps + 1], idx = pps;
		for (; idx < ppe; idx++) {
			if (post[idx] == val)
				break;
		}
		root.left = dfs(pre, ps + 1, ps + idx - pps + 1, post, pps, idx);
		root.right = dfs(pre, ps + idx - pps + 2, pe, post, idx + 1, ppe - 1);
		return root;
	}

	public void printInorder(TreeNode r) {
		if (r == null)
			return;
		
		System.out.print(" " + r.val);
		printInorder(r.left);
		printInorder(r.right);
	}

	public static String treeNodeToString(TreeNode root) {
        if (root == null) {
            return "[]";
        }
    
        String output = "";
        Queue<TreeNode> nodeQueue = new LinkedList<>();
        nodeQueue.add(root);
        while(!nodeQueue.isEmpty()) {
            TreeNode node = nodeQueue.remove();
    
            if (node == null) {
              output += "null, ";
              continue;
            }
    
            output += String.valueOf(node.val) + ", ";
            nodeQueue.add(node.left);
            nodeQueue.add(node.right);
        }
        return "[" + output.substring(0, output.length() - 2) + "]";
    }
	
	public static void main(String[] args) {

		int[] pre = { 1, 2, 4, 5, 3, 6, 7 };
		int[] post = { 4, 5, 2, 6, 7, 3, 1 };

		BinaryTreeFromPreorderAndPostorderTraversal tree = new BinaryTreeFromPreorderAndPostorderTraversal();
		TreeNode root = tree.constructFromPrePost(pre, post);
//		tree.printInorder(root);
		
		String s = treeNodeToString(root);
		System.out.println(s);
	}
}

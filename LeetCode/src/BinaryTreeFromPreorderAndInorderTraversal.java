import java.util.LinkedList;
import java.util.Queue;

/*
105 Given preorder and inorder traversal of a tree, construct the binary tree.

Note:
You may assume that duplicates do not exist in the tree.

For example, given

preorder = [3,9,20,15,7]
inorder = [9,3,15,20,7]
Return the following binary tree:

    3
   / \
  9  20
    /  \
   15   7
 */

/*
preorder = [3, 9, 20,15,7]
          Root L |--R----|
inorder  = [9, 3,   15,20,7]
           L   Root |--R---|
   
   preStart = 0;
   
   Base case:
   L >  R return null
  
   
   root = pre[preStart]
   preStart++;
   
   L == R return root
   sub_index = 0;
   loop sub_index >= inE
   	inorder[sub_index++] == pre[preStart] break;
   root.left = dfs(pre,inorder, inS, sub_index)
   root.right =dfs(pre, inorder, sub_index+1, inE)
   
   return root;
 * 
 */
public class BinaryTreeFromPreorderAndInorderTraversal {

	public class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode(int x) {
			val = x;
		}
	}

	int preStart;

	public TreeNode buildTree(int[] preorder, int[] inorder) {

		preStart = 0;
		return dfs(preorder, inorder, 0, preorder.length - 1);
//		return dfs(preorder, inorder, 0, 0, preorder.length - 1);

	}

	public TreeNode dfs(int[] preorder, int[] inorder, int inS, int inE) {

		// base case
		if (inS > inE)
			return null;

		int r = preorder[preStart];

		TreeNode root = new TreeNode(r);

		int mid = preorder[preStart]; // find left of root
		preStart++;

		// trick need to check after move to next root node or preStart++
		if (inS == inE)
			return root;

		int sub_index = inS;
		while (sub_index <= inE) {
			if (inorder[sub_index] == mid)
				break;
			sub_index++;
		}

		root.left = dfs(preorder, inorder, inS, sub_index - 1);
		root.right = dfs(preorder, inorder, sub_index + 1, inE);

		return root;

	}

// NEED TO LOOK OVER TO CLEAR THE INDEXES AND UNDERSTAND	
//	public TreeNode dfs(int[] preorder, int[] inorder, int preStart, int inS, int inE) {
//
//		// base case
//		if (inS > inE || preStart > preorder.length-1)
//			return null;
//
//		int r = preorder[preStart];
//
//		TreeNode root = new TreeNode(r);
//
//		int mid = preorder[preStart]; // find left of root
//		preStart++;
//
//		// trick need to check after move to next root node or preStart++
//		if (inS == inE)
//			return root;
//
//		int sub_index = inS;
////		while (sub_index <= inE) {
////			if (inorder[sub_index] == mid)
////				break;
////			sub_index++;
////		}
//
//		for (int i = inS; i <= inE; i++) {
//            if (inorder[i] == mid) {
//            	sub_index =i;
//            }
//        }
//		root.left = dfs(preorder, inorder, preStart+1, inS, sub_index - 1);
////		root.right = dfs(preorder, inorder, preStart + sub_index - inS + 1,sub_index + 1, inE);
//		root.right = dfs(preorder, inorder, preStart + sub_index - inS + 1,sub_index + 1, inE);
//
//		return root;
//
//	}
	
	public String treeNodeToString(TreeNode root) {
		if (root == null) {
			return "[]";
		}

		String output = "";
		Queue<TreeNode> nodeQueue = new LinkedList<>();
		nodeQueue.add(root);
		while (!nodeQueue.isEmpty()) {
			TreeNode node = nodeQueue.remove();

			if (node == null) {
				output += "null, ";
				continue;
			}

			output += String.valueOf(node.val) + ", ";
			nodeQueue.add(node.left);
			nodeQueue.add(node.right);
		}
		return "[" + output.substring(0, output.length() - 2) + "]";
	}

	public static void main(String[] args) {

		int[] preorder = { 3, 9, 20, 15, 7 };
		int[] inorder = { 9, 3, 15, 20, 7 };

		BinaryTreeFromPreorderAndInorderTraversal tree = new BinaryTreeFromPreorderAndInorderTraversal();
		TreeNode root = tree.buildTree(preorder, inorder);
		String s = tree.treeNodeToString(root);
		System.out.println(s);
	}
}

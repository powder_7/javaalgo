import java.util.*;

/*
 * Given a list accounts, each element accounts[i] is a list of strings, where the first element accounts[i][0] is a name, and the rest of the elements are emails representing emails of the account.

Now, we would like to merge these accounts. Two accounts definitely belong to the same person if there is some email that is common to both accounts. Note that even if two accounts have the same name, they may belong to different people as people could have the same name. A person can have any number of accounts initially, but all of their accounts definitely have the same name.

After merging the accounts, return the accounts in the following format: the first element of each account is the name, and the rest of the elements are emails in sorted order. The accounts themselves can be returned in any order.

Example 1:
Input: 
accounts = [["John", "johnsmith@mail.com", "john00@mail.com"], ["John", "johnnybravo@mail.com"], ["John", "johnsmith@mail.com", "john_newyork@mail.com"], ["Mary", "mary@mail.com"]]
Output: [["John", 'john00@mail.com', 'john_newyork@mail.com', 'johnsmith@mail.com'],  ["John", "johnnybravo@mail.com"], ["Mary", "mary@mail.com"]]
Explanation: 
The first and third John's are the same person as they have the common email "johnsmith@mail.com".
The second John and Mary are different people as none of their email addresses are used by other accounts.
We could return these lists in any order, for example the answer [['Mary', 'mary@mail.com'], ['John', 'johnnybravo@mail.com'], 
['John', 'john00@mail.com', 'john_newyork@mail.com', 'johnsmith@mail.com']] would still be accepted.
Note:

The length of accounts will be in the range [1, 1000].
The length of accounts[i] will be in the range [1, 10].
The length of accounts[i][j] will be in the range [1, 30].
 */
public class AccountsMerge {
	List<List<String>> res = new ArrayList<>();

	// public List<List<String>> accountsMerge(List<List<String>> accounts) {
	//
	// System.out.println(accounts);
	// // email, List<String>
	// Map<String, List<String>> tm = new TreeMap<>();
	// for (int account = 0; account < accounts.size(); account++) {
	// List<String> l = accounts.get(account);
	// String name = l.get(0);
	// System.out.println("name = " + name);
	// boolean is_merge = false;
	// for (int i = 1; i < l.size(); i++) {
	// String email = l.get(i);
	// System.out.println(email);
	// if (tm.containsKey(email)) {
	// List<String> tmp_acct = tm.get(email);
	// if (tmp_acct != l) {
	// is_merge = merge(tmp_acct, l);
	// break;
	// }
	// } else {
	// tm.put(email, l);
	// }
	// }
	// if (false == is_merge) {
	//
	// Set<String> set = new TreeSet<>();
	// for (String s : l) {
	// set.add(s);
	// }
	// res.add(new ArrayList<>(set));
	// }
	// System.out.println("account merge = " + res);
	// }
	//
	// return res;
	// }
	//
	// public boolean merge(List<String> a, List<String> b) {
	// Set<String> set = new TreeSet<>();
	// for (String s : a) {
	// set.add(s);
	// }
	// for (String s : b) {
	// set.add(s);
	// }
	// System.out.println("merge set = " + set);
	// System.out.println("before remove acct = " + res);
	// removeAccount(set);
	// System.out.println("after remove acct = " + res);
	// // res.add(new ArrayList<>(set));
	// // System.out.println("merge set = " + set);
	// return true;
	// }
	//
	// public void removeAccount(Set<String> set) {
	// List<String> prevList = null;
	// List<String> newList = null;
	// // skip over name
	// for (int i = 1; i < set.size(); i++) {
	// for (List<String> l : res) {
	// if(prevList !=null ) {
	// accountsMerge(res);
	// prevList = null;
	// }
	// if (l.contains(set.toArray()[i])) {
	// Set<String> tmp = new TreeSet<>(l);
	// res.remove(l);
	// tmp.addAll(set);
	// newList = new ArrayList<>(tmp);
	// prevList = newList;
	// res.add(newList);
	// break;
	// }
	// }
	// }
	// }
	/*
	 * public void test() {
	 * 
	 * List<List<String>> accounts = new ArrayList<>(); // accounts.add(new
	 * ArrayList<>(Arrays.asList("Gabe", "Gabe0@m.co", // "Gabe3@m.co",
	 * "Gabe1@m.co"))); // // accounts.add(new ArrayList<>(Arrays.asList("David",
	 * "David0@m.co", // "David4@m.co", "David3@m.co"))); // accounts.add(new
	 * ArrayList<>(Arrays.asList("David", "David5@m.co", // "David5@m.co",
	 * "David0@m.co"))); // accounts.add(new ArrayList<>(Arrays.asList("David",
	 * "David1@m.co", // "David4@m.co", "David0@m.co"))); // accounts.add(new
	 * ArrayList<>(Arrays.asList("David", "David0@m.co", // "David1@m.co",
	 * "David3@m.co"))); // accounts.add(new ArrayList<>(Arrays.asList("David",
	 * "David4@m.co", // "David1@m.co", "David3@m.co")));
	 * 
	 * accounts.add(new ArrayList<>(Arrays.asList("David", "David0@m.co",
	 * "David1@m.co"))); accounts.add(new ArrayList<>(Arrays.asList("David",
	 * "David3@m.co", "David4@m.co"))); accounts.add(new
	 * ArrayList<>(Arrays.asList("David", "David4@m.co", "David5@m.co")));
	 * accounts.add(new ArrayList<>(Arrays.asList("David", "David2@m.co",
	 * "David3@m.co"))); accounts.add(new ArrayList<>(Arrays.asList("David",
	 * "David1@m.co", "David2@m.co")));
	 * 
	 * // accounts.add(new ArrayList<>(Arrays.asList("John", "johnsmith@mail.com",
	 * // "john00@mail.com"))); // accounts.add(new
	 * ArrayList<>(Arrays.asList("John", "johnnybravo@mail.com"))); //
	 * accounts.add(new ArrayList<>(Arrays.asList("John", "johnsmith@mail.com", //
	 * "john_newyork@mail.com"))); // accounts.add(new
	 * ArrayList<>(Arrays.asList("Mary", "mary@mail.com")));
	 * 
	 * // accounts.add(new ArrayList<>(Arrays.asList("Alex", "Alex5@m.co", //
	 * "Alex4@m.co", "Alex0@m.co"))); // accounts.add(new
	 * ArrayList<>(Arrays.asList("Ethan", "Ethan3@m.co", // "Ethan3@m.co",
	 * "Ethan0@m.co"))); // accounts.add(new ArrayList<>(Arrays.asList("Kevin",
	 * "Kevin4@m.co", // "Kevin2@m.co", "Kevin2@m.co"))); // accounts.add(new
	 * ArrayList<>(Arrays.asList("Gabe", "Gabe0@m.co", // "Gabe3@m.co",
	 * "Gabe2@m.co"))); // accounts.add(new ArrayList<>(Arrays.asList("Gabe",
	 * "Gabe3@m.co", // "Gabe4@m.co", "Gabe2@m.co")));
	 * 
	 * // System.out.println(accounts);
	 * 
	 * System.out.println("res = " + accountsMerge(accounts));
	 * 
	 * }
	 */

	/*
	 * public List<List<String>> accountsMerge(List<List<String>> accounts) { //
	 * build the graph Map<String, Set<String>> graph = new HashMap<>(); for
	 * (List<String> account : accounts) { for (int i = 1; i < account.size(); i++)
	 * { if (!graph.containsKey(account.get(i))) graph.put(account.get(i), new
	 * HashSet<String>()); graph.get(account.get(i)).add(account.get(1));
	 * graph.get(account.get(1)).add(account.get(i)); } } // traverse the graph,
	 * find out all the connected subgraph Set<String> visited = new HashSet<>();
	 * List<List<String>> result = new ArrayList<>(); for (List<String> account :
	 * accounts) { if (!visited.contains(account.get(1))) { List<String> ans = new
	 * ArrayList<>(); bfs(graph, visited, account.get(1), ans); // or
	 * dfs(graph,visited,ls.get(1),ans) Collections.sort(ans); ans.add(0,
	 * account.get(0)); result.add(ans); } } return result; }
	 * 
	 * public void dfs(Map<String, Set<String>> graph, Set<String> visited, String
	 * s, List<String> ans) { ans.add(s); visited.add(s); for (String str :
	 * graph.get(s)) { if (!visited.contains(str)) { dfs(graph, visited, str, ans);
	 * } } }
	 * 
	 * public void bfs(Map<String, Set<String>> graph, Set<String> visited, String
	 * s, List<String> ans) { Queue<String> q = new LinkedList<>(); q.add(s);
	 * visited.add(s); while (!q.isEmpty()) { String t = q.poll(); ans.add(t); for
	 * (String str : graph.get(t)) { if (!visited.contains(str)) { q.add(str);
	 * visited.add(str); } } } }
	 */
	/*
	public List<List<String>> accountsMerge(List<List<String>> accounts) {
		Map<String, Set<String>> graph = new HashMap<>(); // <email node, neighbor nodes>
		Map<String, String> name = new HashMap<>(); // <email, username>
		// Build the graph;
		for (List<String> account : accounts) {
			String userName = account.get(0);
			for (int i = 1; i < account.size(); i++) {
				if (!graph.containsKey(account.get(i))) {
					graph.put(account.get(i), new HashSet<>());
				}
				name.put(account.get(i), userName);

				if (i == 1)
					continue;
				graph.get(account.get(i)).add(account.get(i - 1));
				graph.get(account.get(i - 1)).add(account.get(i));
			}
		}

		Set<String> visited = new HashSet<>();
		List<List<String>> res = new LinkedList<>();
		// DFS search the graph;
		for (String email : name.keySet()) {
//			List<String> list = new LinkedList<>();
			Set<String> ts = new TreeSet<>();
			if (visited.add(email)) {
				dfs(graph, email, visited, ts);
//				Collections.sort(list);
				List<String> list = new LinkedList<>(ts);
				list.add(0, name.get(email));
				res.add(list);
			}
		}

		return res;
	}

	public void dfs(Map<String, Set<String>> graph, String email, Set<String> visited, Set<String> ts) {
 		ts.add(email);
		for (String next : graph.get(email)) {
			if (visited.add(next)) {
				dfs(graph, next, visited, ts);
			}
		}
	}

	*/
	 
	public List<List<String>> accountsMerge(List<List<String>> accounts) {
        Map<String, Set<String>> graph = new HashMap<>();  //<email node, neighbor nodes>
        Map<String, String> name = new HashMap<>();        //<email, username>
        
	 	for(List<String> account : accounts) {
			List<String> email = account;
            String userName = email.get(0); //get name;
			for (int i = 1; i < email.size(); i++) {
                if(!graph.containsKey(email.get(i))) {
                    graph.put(email.get(i), new HashSet<>());
                }
            
                name.put(email.get(i), userName);
                //has more than 1 email.
                if(i>1) {
                    // add neighor email nodes
                    graph.get(email.get(i)).add(email.get(i-1));
                    graph.get(email.get(i-1)).add(email.get(i));
                }
                
            }
		}
        
        Set<String> visited = new HashSet<>();
        List<List<String>> res = new ArrayList<>();
        //dfs search graph
        for(String email : name.keySet()) {
           
            Set<String> ts = new TreeSet<>();
            //not contain
            if(visited.add(email)) {
                dfs(graph, email, visited, ts);
                List<String> list = new ArrayList<>(ts);
                list.add(0, name.get(email));
                res.add(list);
            }
        }
		
		return res;
	}
    
    public void dfs(Map<String, Set<String>> graph, String email, Set<String> visited, Set<String> ts) {
        ts.add(email);
        for(String next : graph.get(email)) {
            if(visited.add(next)) {
                dfs(graph, next, visited, ts);
            }
        }
        
    }
	public void testCode() {

		List<List<String>> accounts = new ArrayList<>();
		// accounts.add(new ArrayList<>(Arrays.asList("Gabe", "Gabe0@m.co",
		// "Gabe3@m.co", "Gabe1@m.co")));
		//
		// accounts.add(new ArrayList<>(Arrays.asList("David", "David0@m.co",
		// "David4@m.co", "David3@m.co")));
		// accounts.add(new ArrayList<>(Arrays.asList("David", "David5@m.co",
		// "David5@m.co", "David0@m.co")));
		// accounts.add(new ArrayList<>(Arrays.asList("David", "David1@m.co",
		// "David4@m.co", "David0@m.co")));
		// accounts.add(new ArrayList<>(Arrays.asList("David", "David0@m.co",
		// "David1@m.co", "David3@m.co")));
		// accounts.add(new ArrayList<>(Arrays.asList("David", "David4@m.co",
		// "David1@m.co", "David3@m.co")));
/*
		accounts.add(new ArrayList<>(Arrays.asList("John", "johnsmith@mail.com", "john00@mail.com")));
		accounts.add(new ArrayList<>(Arrays.asList("John", "johnnybravo@mail.com")));
		accounts.add(new ArrayList<>(Arrays.asList("John", "johnsmith@mail.com", "john_newyork@mail.com")));
		accounts.add(new ArrayList<>(Arrays.asList("Mary", "mary@mail.com")));
*/
		//input
//		[[John, johnsmith@mail.com, john00@mail.com], 
//		 [John, johnnybravo@mail.com], 
//		 [John, johnsmith@mail.com, john_newyork@mail.com], 
//		 [Mary, mary@mail.com]]

//		graph result
//		{johnnybravo@mail.com=[], 
//		 johnsmith@mail.com=[john00@mail.com, john_newyork@mail.com], 
//		 john00@mail.com=[johnsmith@mail.com], 
//		 john_newyork@mail.com=[johnsmith@mail.com], 
//		 mary@mail.com=[]}
//
//		key name map 
//		{johnnybravo@mail.com=John, johnsmith@mail.com=John, john00@mail.com=John, john_newyork@mail.com=John, mary@mail.com=Mary}
		
		 
//		//input
//		[David, David0@m.co, David1@m.co], 
//		[David, David3@m.co, David4@m.co], 
//		[David, David4@m.co, David5@m.co], 
//		[David, David2@m.co, David3@m.co], 
//		[David, David1@m.co, David2@m.co]]
////graph result
//		{David4@m.co=[David3@m.co, David5@m.co], 
//		David3@m.co=[David4@m.co, David2@m.co], 
//		David0@m.co=[David1@m.co], 
//		David2@m.co=[David3@m.co, David1@m.co], 
//		David1@m.co=[David0@m.co, David2@m.co], 
//		David5@m.co=[David4@m.co]}
//// name key map
//		{David4@m.co=David, David3@m.co=David, David0@m.co=David, David2@m.co=David, David1@m.co=David, David5@m.co=David}
		
		accounts.add(new ArrayList<>(Arrays.asList("David", "David0@m.co", "David1@m.co")));
		accounts.add(new ArrayList<>(Arrays.asList("David", "David3@m.co", "David4@m.co")));
		accounts.add(new ArrayList<>(Arrays.asList("David", "David4@m.co", "David5@m.co")));
		accounts.add(new ArrayList<>(Arrays.asList("David", "David2@m.co", "David3@m.co")));
		accounts.add(new ArrayList<>(Arrays.asList("David", "David1@m.co", "David2@m.co")));
		System.out.println("res = " + accountsMerge(accounts));

	}

	public static void main(String args[]) {
		new AccountsMerge().testCode();
	}
}

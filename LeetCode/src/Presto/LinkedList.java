package Presto;

public class LinkedList {

	public class ListNode {
		int val;
		ListNode next;

		ListNode(int x) {
			val = x;
			next = null;
		}
	}

	ListNode mHead = null;

	public void testDrive() {
//		testInterleave();
//		testMergeSort();
		testInterleave3();
	}

	public void testMergeSort() {

		ListNode a = null;
		ListNode b = null;

		for (int i = 10; i > 0; i--) {
			// odd
			if (i % 2 != 0)
				a = insertHead(a, i);
			else
				b = insertHead(b, i);
		}
//		print(a);
//		print(b);
		ListNode res = mergeSort(a, b);
		System.out.println(" mergeSort");
		print(res);
		
	}
	
	public ListNode mergeSort(ListNode a, ListNode b) {
		ListNode dummy = new ListNode(0);
		ListNode tmp = dummy;
		 
		while (a != null && b != null) {

			if (a.val <= b.val) {
				dummy.next = a;
				a = a.next;
			} else {
				dummy.next = b;
				b = b.next;
			}
			dummy = dummy.next;
		}

		return tmp.next;
	}

	public void testInterleave() {
		ListNode node1 = null;
		ListNode node2 = null;

		for (int i = 10; i > 0; i--) {
			// odd
			if (i % 2 != 0)
				node1 = insertHead(node1, i);
			else
				node2 = insertHead(node2, i);
		}

//		node1 = insertHead(node1, 13);
//		node1 = insertHead(node1, 15);

		System.out.println("\n odd link list");
		print(node1);

		System.out.println("\n even link list");
		print(node2);

		ListNode res = interleave(node1, node2);
 
		System.out.println("\nresult interleave ");
		print(res);
		

	}

	public void testInterleave3() {
		
		ListNode node1 = null;
		ListNode node2 = null;

		for (int i = 20; i > 0; i--) {
			// odd
			if (i % 2 != 0)
				node1 = insertHead(node1, 1);
			else
				node2 = insertHead(node2, 2);
		}

		ListNode res = interleave(node1, node2);
		node2 = null;
		for(int i = 0; i < 8; i++) {
			node2 = insertHead(node2,3);
		}
		print(node2);
		
		res = interleave3(res, node2);
		System.out.println("\nresult interleave3 ");
		print(res);
	}
	
	public ListNode interleave3(ListNode a, ListNode b) {
	 
		ListNode res = a;
		while(a != null && a.next != null && b != null) {
  
			ListNode a_next_next = a.next.next;
			ListNode b_next = b.next;
 
			a.next.next = b;
			b.next = a_next_next;
			
			a = a_next_next;
			b = b_next;
		}
		return res;
	}
	
	public ListNode interleave(ListNode n1, ListNode n2) {

		ListNode res = n1;

		while (n1 != null && n2 != null) {

			ListNode n1_next = n1.next;
			ListNode n2_next = n2.next;

			n1.next = n2;
			n2.next = n1_next;

			n1 = n1_next;
			n2 = n2_next;
		}

		return res;
	}

	public void testInsertAfterAndBefore() {
		ListNode node1 = null;

		for (int i = 10; i > 0; i--) {
			node1 = insertHead(node1, i);
		}

		int pos = 10;
		int val = 99;

		node1 = insertBefore(node1, 10, 99);
		System.out.println(val + " input before " + pos);
		print(node1);

		insertAfter(node1, pos, val);
		System.out.println(val + " input after " + pos);

		print(node1);

	}

	public void print(ListNode n) {
		while (n != null) {
			System.out.print(n.val + " ");
			n = n.next;
		}
		System.out.println("");
	}

	public ListNode insertHead(ListNode node, int x) {
		if (node == null) {
			node = new ListNode(x);
		} else {

			ListNode tmp = new ListNode(x);
			tmp.next = node;
			node = tmp;
		}
		return node;
	}

	public ListNode insertBefore(ListNode node, int before, int val) {
		if (node == null)
			return null;

		if (node.val == before) {
			ListNode tmp = new ListNode(val);
			tmp.next = node;
			node = tmp;
		} else {

			ListNode prev = null;
			ListNode curr = node;
			while (curr != null && curr.val != before) {
				prev = curr;
				curr = curr.next;
			}

			ListNode tmp = new ListNode(val);
			tmp.next = curr;
			prev.next = tmp;
		}
		return node;

	}

	public ListNode insertAfter(ListNode node, int after, int val) {

		if (node == null)
			return null;

		if (node.val == after) {
			ListNode tmp = new ListNode(val);
			tmp.next = node.next;
			node.next = tmp;
		} else {
			ListNode tmp_head = node;
			while (tmp_head != null && tmp_head.val != after) {
				tmp_head = tmp_head.next;
			}
			ListNode tmp = new ListNode(val);
			tmp.next = tmp_head.next;
			tmp_head.next = tmp;
		}

		return node;
	}

	public static void main(String args[]) {
		
		new LinkedList().testDrive();
	}
}

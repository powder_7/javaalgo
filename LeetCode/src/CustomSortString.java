import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

/*
791. Custom Sort String
S and T are strings composed of lowercase letters. In S, no letter occurs more than once.

S was sorted in some custom order previously. We want to permute the characters of T so that they match the order that S was sorted. 
More specifically, if x occurs before y in S, then x should occur before y in the returned string.

Return any permutation of T (as a string) that satisfies this property.

Example :
Input: 
S = "cba"
T = "abcd"
Output: "cbad"
Explanation: 
"a", "b", "c" appear in S, so the order of "a", "b", "c" should be "c", "b", and "a". 
Since "d" does not appear in S, it can be at any position in T. "dcba", "cdba", "cbda" are also valid outputs.
 

Note:

S has length at most 26, and no character is repeated in S.
T has length at most 200.
S and T consist of lowercase letters only.

 */
public class CustomSortString {

	public String customSortString(String S, String T) {
		Map<Character, Integer> hm = new HashMap<>();
		// String res = "";
		StringBuilder res = new StringBuilder();
		for (int i = 0; i < T.length(); i++) {
			char c = T.charAt(i);
			hm.put(c, hm.getOrDefault(c, 0) + 1);
		}

		for (char c : S.toCharArray()) {
			if (hm.containsKey(c)) {
				for (int i = 0; i < hm.get(c); i++) {
					// res += String.valueOf(c);
					res.append(c);

				}
				hm.remove(c);
			}
		}

		for (Map.Entry<Character, Integer> entry : hm.entrySet()) {
			for (int i = 0; i < entry.getValue(); i++) {
				// res += entry.getKey();
				res.append(entry.getKey());
			}
		}

		return res.toString();
	}

	public String customSortString1(String S, String T) {
		StringBuilder res = new StringBuilder();
		int[] map = new int[26];
		for (int i = 0; i < T.length(); i++) {
			map[T.charAt(i) - 'a'] += 1;
		}

		for (char c : S.toCharArray()) {
			for (int i = 0; i < map[c - 'a']; i++) {
				res.append(c);
			}
			map[c - 'a'] = 0;
		}
//		for (int i = 0; i < map.length; i++) {
//			for (int j = 0; j < map[i]; j++) {
//				res.append((char) (i + 'a'));
//			}
//
//			map[i] = 0;
//		}
		
        for (char c = 'a'; c <= 'z'; ++c)
            for (int i = 0; i < map[c - 'a']; ++i)
                res.append(c);
        
		return res.toString();
	}

	@Test
	public void test() {
		String S = "cba", T = "abcd", Output = "cbad";
		String res = customSortString(S, T);
		assertEquals(Output, res);
		System.out.println(res);

		customSortString1(S, T);
	}

	public static void main(String args[]) {
		new CustomSortString().test();
	}
}

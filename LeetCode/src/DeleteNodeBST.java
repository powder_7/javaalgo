
/*
450. Delete Node in a BST
Given a root node reference of a BST and a key, delete the node with the given key in the BST. Return the root node reference (possibly updated) of the BST.
Basically, the deletion can be divided into two stages:
Search for a node to remove.
If the node is found, delete the node.

 */
public class DeleteNodeBST {
	// Definition for a binary tree node.
	public class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode(int x) {
			val = x;
		}
	}

	public TreeNode deleteNode(TreeNode root, int key) {
		if (root == null)
			return null;
		// travel the tree
		if (key < root.val)
			root.left = deleteNode(root.left, key);
		else if (key > root.val)
			root.right = deleteNode(root.right, key);
		else {
			// case 1 : no child
			// case 2 child right
			if (root.left == null) {
				root = root.right;
			}
			// case 2 child left
			else if (root.right == null) {
				root = root.left;
			}
			// case 3:
			else {
				// find smallest(left) node of the right node
				TreeNode n = findMin(root.right);
				root.val = n.val; // copy delete
				root.right = deleteNode(root.right, root.val); // remove right node
			}
		}

		return root;
	}

	public TreeNode findMin(TreeNode n) {
		TreeNode tmp = n;
		while (tmp.left != null)
			tmp = tmp.left;

		return tmp;
	}

	public static void main(String[] args) {

	}
}

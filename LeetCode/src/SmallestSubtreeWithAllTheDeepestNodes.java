//https://leetcode.com/problems/smallest-subtree-with-all-the-deepest-nodes/description/
/* 865
Given a binary tree rooted at root, the depth of each node is the shortest distance to the root.
A node is deepest if it has the largest depth possible among any node in the entire tree.
The subtree of a node is that node, plus the set of all descendants of that node.
Return the node with the largest depth such that it contains all the deepest nodes in its subtree.
 */

/*
Input: [3,5,1,6,2,0,8,null,null,7,4]
Output: [2,7,4]
Explanation:
We return the node with value 2, colored in yellow in the diagram.
The nodes colored in blue are the deepest nodes of the tree.
The input "[3, 5, 1, 6, 2, 0, 8, null, null, 7, 4]" is a serialization of the given tree.
The output "[2, 7, 4]" is a serialization of the subtree rooted at the node with value 2.
Both the input and output have TreeNode type.
 */

public class SmallestSubtreeWithAllTheDeepestNodes {
	public class TreeNode{
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) { val = x; }
	}
	// post dfs
	/*	
	 * int dfs(root, level);
	 * 
	 * root null return level;
	 * l = dfs(root.left, level+1);
	 * r = dfs(root.right, level+1);
	 * if l == r //both node have same level
	 * 	if(r >= maxlevel) 
	 * 		upate maxlevel=r and result = root
	 * 
	 *	// get max of left or right level
	 * return max(l,r);
 	 */
	
	TreeNode res = null;
	int maxlevel = 0;
	public TreeNode subtreeWithAllDeepest(TreeNode root) {
		
		if(root == null) return null;
		if(root.left == null && root.right == null) return root;
		dfs(root, 0);
		
		return null;
	}
	
	public int dfs(TreeNode root, int level) {
		
		if(root == null) return level;
		int l = dfs(root.left, level+1);
		int r = dfs(root.right, level+1);
		
		if(l == r) {
			if(maxlevel <= r) {
				maxlevel = r;
				res = root;
			}
		}
		return Math.max(l, r);
		
	}
	
	public TreeNode dfs(TreeNode root) {
		if(root == null) return null;
		int l = dept(root.left);
		int r = dept(root.right);
		
		if(l>r) return dfs(root.left);
		else return dfs(root.right);
				
	}
	
	public int dept(TreeNode root) {
		if(root==null) return 0;
		return Math.max(dept(root.left), dept(root.left))+1;
	}

	public static void main(String[] args) {
		// create a binary tree to test
		// [3,5,1,6,2,0,8,null,null,7,4]
		/*
		 * 	     3
		 *    5     1
		 *  6  2   0  8
		 *    7 4
		 *    
		 *    test 2
		 *    3
		 *   2
		 *     5
		 *     
		 *     return 5 deepest node
		 */
		TreeNode root = null;
		TreeNode res = new SmallestSubtreeWithAllTheDeepestNodes().subtreeWithAllDeepest(root);
		
	}
}

/*
 13. Roman to Integer
  Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.
Symbol       Value
I             1
V             5
X             10
L             50
C             100
D             500
M             1000
For example, two is written as II in Roman numeral, just two one's added together. 
Twelve is written as, XII, which is simply X + II. 
The number twenty seven is written as XXVII, which is XX + V + II.

Roman numerals are usually written largest to smallest from left to right. 
However, the numeral for four is not IIII. Instead, the number four is written as IV. 
Because the one is before the five we subtract it making four. The same principle applies to the number nine, 
which is written as IX. There are six instances where subtraction is used:

I can be placed before V (5) and X (10) to make 4 and 9. 
X can be placed before L (50) and C (100) to make 40 and 90. 
C can be placed before D (500) and M (1000) to make 400 and 900.

Given a roman numeral, convert it to an integer. Input is guaranteed to be within the range from 1 to 3999.
 */
/*
 * Example 1:

Input: "III"
Output: 3
Example 2:

Input: "IV"
Output: 4
Example 3:

Input: "IX"
Output: 9
Example 4:

Input: "LVIII"
Output: 58
Explanation: C = 100, L = 50, XXX = 30 and III = 3.
Example 5:

Input: "MCMXCIV"
Output: 1994
Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.
 */
public class RomanToInteger {

	public int romanToInt(String s) {
		char prev = '0';
		char curr = '0';
		int res = 0;
		for(int i = 0; i < s.length(); i++) {
			curr = s.charAt(i);
			if( curr == 'I' ) res += 1;
			else if(curr == 'V' ) res += 5;
			else if(curr == 'X' ) res += 10;
			else if(curr == 'L' ) res += 50;
			else if(curr == 'C' ) res += 100;
			else if(curr == 'D' ) res += 500;
			else if(curr == 'M' ) res += 1000;
			
			// need to double offset because count twice for each letter
			// ie XL = 40 but read as X->10 + L->50 = 60, thus 60-20 = 40
			// ie IV is 4 but read as I-> + V->5 = 6, so 6-2 = 4  
			if(prev == 'I' && (curr == 'V' ||curr == 'X') ) res -=2;
			else if(prev == 'X' && (curr == 'L' || curr == 'C') ) res -= 20;
			else if(prev == 'C' && (curr == 'D' || curr == 'M') ) res -= 200;
 
			prev = curr;
		}
		return res;
	}

	public static void main(String args[]) {
//		String s ="MCMXCIV"; //1994
		String s = "LVIII";  // 58
		int res = new RomanToInteger().romanToInt(s);
		System.out.println(res);
	}
}

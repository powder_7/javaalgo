/*
 680 Valid Palindrome II
 Given a non-empty string s, you may delete at most one character. Judge whether you can make it a palindrome.

Example 1:
Input: "aba"
Output: True
Example 2:
Input: "abca"
Output: True
Explanation: You could delete the character 'c'.
 */
public class ValidPalindromeII {

	public boolean validPalindrome(String s) {
		int l = 0;
		int r = s.length() - 1;
		int k = 0,m = 0;
		while (l <= r) {
			if (s.charAt(l) != s.charAt(r)) {
				//remove left char
				for(k = l+1, m = r; k <= m; k++, m-- ) {
					if(s.charAt(k)!=s.charAt(m)) break;
				}
				if( k>=m )return true;
				//remove right char
				for(k = l, m = r-1; k <= m; k++, m--) {
					if(s.charAt(k) != s.charAt(m)) break;
				}
				// one of the above for loop remove char is palndrome
				if( k>=m )return true;
				
				return false;
			} 
			l++;
			r--;
		}
 
		return true;

	}

	public static void main(String[] args) {

//		String s = "abc"; // false
//		String s = "abca"; // true
//		String s = "deeee";//true
		
		String s = "cbbcc";// true;

		boolean res = new ValidPalindromeII().validPalindrome(s);
		System.out.println(res);

	}
}

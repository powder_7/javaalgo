import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* Easy
 * https://leetcode.com/problems/find-all-numbers-disappeared-in-an-array/
 * 448. Find All Numbers Disappeared in an Array
 * 
 * Given an array of integers where 1 ≤ a[i] ≤ n (n = size of array), some elements appear twice and others appear once.

	Find all the elements of [1, n] inclusive that do not appear in this array.
	
	Could you do it without extra space and in O(n) runtime? You may assume the returned list does not count as extra space.
	
	Example:
	
	Input:
	[4,3,2,7,8,2,3,1]
	
	Output:
	[5,6]
	
 */
public class AllNumbersDisappearedInArray {

	public List<Integer> findDisappearedNumbers(int[] nums) {

		List<Integer> res = new ArrayList<>(nums.length);
		boolean dummy[] = new boolean[nums.length + 1];

		for (int i = 0; i < nums.length; i++) {
			dummy[nums[i]] = true;
		}

		for (int i = 1; i < dummy.length; i++) {
			if (dummy[i] == false)
				res.add(i);
		}

		return res;
	}
	
	public void test() {
		int nums[] = {4,3,2,7,8,2,3,1};
		List<Integer> res = this.findDisappearedNumbers(nums);
		System.out.println(res);
	}

	public static void main(String args[]) {
		new AllNumbersDisappearedInArray().test();
	}
}

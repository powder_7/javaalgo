import static org.junit.Assert.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;

/*
 https://leetcode.com/problems/wiggle-sort/
 Given an unsorted array nums, reorder it in-place such that nums[0] <= nums[1] >= nums[2] <= nums[3]....

Example:

Input: nums = [3,5,2,1,6,4]
Output: One possible answer is [3,5,1,6,2,4]
 */
public class WiggleSort {

	public void wiggleSort(int[] nums) {

		for (int i = 1; i < nums.length; i++) {
			if (i % 2 == 0) {
				swapMax(nums, i, i - 1);
			} else {
				swapMin(nums, i, i - 1);
			}
		}
	}

	// put max on right side
	public void swapMax(int[] a, int x, int y) {
		if (a[x] > a[y]) {
			int tmp = a[x];
			a[x] = a[y];
			a[y] = tmp;
		}
	}

	// put min on right side
	public void swapMin(int[] a, int x, int y) {
		if (a[x] < a[y]) {
			int tmp = a[x];
			a[x] = a[y];
			a[y] = tmp;
		}
	}
	
	public static void main(String arg[] ) {
		int nums[] = {3,5,2,1,6,4};
		int unitest[] = {3,5,1,6,2,4};
		new WiggleSort().wiggleSort(nums);
		assertArrayEquals(unitest, nums);
		System.out.println(Arrays.toString(nums));
	}
}

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

public class DeckCard {

	public class Card {
		String mSuit;
		String mValue;

		Card(String suit, String value) {
			this.mSuit = suit;
			this.mValue = value;
		}

		@Override
		public String toString() {
			return mSuit + " " + mValue;
		}
	}

	public class Player {
		List<Card> hand;

		public Player() {
			this.hand = new ArrayList<>();
		}

		public List<Card> getHand() {
			return hand;
		}

		@Override
		public String toString() {
			return hand.toString();
		}

	}

	private String[] mSuits = { "Spade", "Heart", "Diamond", "Club" };
	private String[] mValues = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King", "Ace" };
	private int mSize = 52;
	private List<Card> mDeck = new ArrayList<>(mSize);

	public void createDeck() {

		for (int i = 0; i < mSuits.length; i++) {
			for (int j = 0; j < mValues.length; j++) {
				mDeck.add(new Card(mSuits[i], mValues[j]));
			}
		}
	}

	public int deckSize() {
		return mDeck.size();
	}

	public void shuffle() {
		Random rand = new Random();

		for (int i = 0; i < mSize; i++) {
			int j = rand.nextInt(mSize);
			// create 2 loction to swap
			Collections.swap(mDeck, i, j);
		}

	}

	public void print() {
		for (Card i : mDeck) {
			System.out.println(i.toString());
		}
	}

	public void deal(Player p) {

		if (!mDeck.isEmpty()) {
			Card c = mDeck.remove(0);
			p.hand.add(c);
		}

	}

	public void fold(Player p) {
		while (!p.hand.isEmpty()) {
			mDeck.add(p.hand.remove(0));
		}
	}

	public void totalSuits(Player p) {
		Map<String, Integer> hm = new HashMap<>();
		
		for(int i = 0; i < p.hand.size(); i++) {
			String s = p.hand.get(i).mSuit;
			hm.put(s, hm.getOrDefault(s, 0) +1);
		}
		
		for(Map.Entry<String, Integer> entry: hm.entrySet()) {
			System.out.println(entry.getKey() + " " + entry.getValue());
		}
		
	}
	public void game() {

		DeckCard dc = new DeckCard();
		dc.createDeck();
		// dc.print();
		dc.shuffle();
		// dc.print();

		Player p1 = new Player();
		dc.deal(p1);
		dc.deal(p1);
		dc.deal(p1);
		// dc.fold(p1);

		totalSuits(p1);
		System.out.println(p1.toString());
		System.out.println("deck size = " + dc.deckSize());
	}

	public static void main(String[] agrs) {

		DeckCard start = new DeckCard();
		start.game();

	}
}

package test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Stack;

public class AlgoJava {
	
	
	public void reverseArray() {
		
		int a[] = {1,2,3};
		int j = a.length-1;
		int tmp=0;
		for(int i=0; i<a.length/2; i++) {
			tmp = a[i];
			a[i] = a[j];
			a[j] = tmp;
			j--;
		}
		System.out.println(Arrays.toString(a));
	}
	//fib xn = xn-2+xn-1
	public int fib(int a) {
		
		int ary[] = new int[a];
		ary[0] = 0;
		ary[1] = 1;
		if(a <= 1) return a;
		for(int i=2; i<ary.length; i++) {
			ary[i] = ary[i-1] + ary[i-2];
		}
		
		System.out.println("fib = " + ary[a-1]);
		return ary[a-1];
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	//If V == 0, then 0 coins required.
	//If V > 0
	//minCoin(coins[0..m-1], V) = min {1 + minCoins(V-coin[i])} 
	//								where i varies from 0 to m-1 and coin[i] <= V 
	//Note..DFS and loop thru coin[], return 0 or min coin if found else max integer. backtrack to previous node.
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	static Stack<Integer> st = new Stack<Integer>();
	public int minCoin(int coins[], int V) {
		
		//base case, return 0 no more change
		if(0==V) return 0;
		
		//init result to max int
		int result = Integer.MAX_VALUE;
		
		//loop thru coin each node level
		for(int i=0; i< coins.length; i++) {
			
			// has more coin in V
			if(coins[i] <= V) {
				// recursion go down a level with new value V
				int backtrack = minCoin(coins, V-coins[i]) +1; // each backtrack is 1 additional coin
				//update min result
				if(backtrack < result) {
					result = backtrack;  
					
				}
			}

		}
		
		return result;
		
	}
	
    public int change(int amount, int[] coins) {
        //base case, return 0 no more change
		if(0==amount) return 0;
		
		//init result to max int
		int result = Integer.MAX_VALUE;
		
		//loop thru coin each node level
		for(int i=0; i< coins.length; i++) {
			
			// has more coin in amount
			if(coins[i] <= amount) {
				// recursion go down a level with new value V
				int backtrack = minCoin(coins, amount-coins[i]) +1; // each backtrack is 1 additional coin
				//update min result
				if(backtrack < result) {
					result = backtrack;  
					
				}
			}

		}
		
		return result;
		
    }
	
    public int coinChange(int[] coins, int amount) {
        
        //base case, return 0 no more change
		if(0==amount) return 0;
        if(0>amount) return -1;
        
		
		//init result to max int
		int result = Integer.MAX_VALUE;
		
		//loop thru coin each node level
		for(int i=0; i< coins.length; i++) {
			
			// has more coin in amount
			if(coins[i] <= amount) {
				// recursion go down a level with new value V
				int backtrack = coinChange(coins, amount-coins[i]) +1; // each backtrack is 1 additional coin
				if(backtrack <=0 )result =-1;
				//update min result
				else if(backtrack < result) {
					result = backtrack;  
					
				}
				
			}
		}
	 
		if(result == Integer.MAX_VALUE) return -1;
		return result;
		
      
    }
	public void minCointTest() {
		
		int V = 40;
		int coins[] = {5,2,1};
//		int coins[] = {2};
//		int coins[] = {6};
		
//		int result = minCoin(coins, V);
		int result =coinChange(coins, V);
		if(!st.isEmpty())
			for(int i =0; i< st.size(); i++) {
				System.out.println(st.get(i).toString());
			}
		
		System.out.println("min coins " + Arrays.toString(coins) +" is " + result + " for " + V);
		
	}
	// FInd sets of numbers that add up o N
	// {2,4,6 10}, 16 output: 2 because, {6,10}, {2,4,10}
	// Base case:	N=0 return 1;
	//				N<0, return 0;
	//				idx < 0 return 0; // because out of bound index
	//				N < array[i] return recursive(array, N, idx=len(array)-1) // move to smaller value in array because 
	//																			 value at idx is greater than N
	//	            return recursive(array, N, len(array)-1) + recursive(array, N-array[i], idx=len(array)-1)
	//					ie{6,4,2} + {2,4}, include current value at idx + exclude current value at idx	
	// O(total*array.length) or O(n^2)
	public int getAllSet(int a[], int total, int idx) {
		
		if(0==total) return 1;
		else if(total < 0) return 0;
		else if(idx < 0) return 0;
		
		if(total < a[idx]) 
			return getAllSet(a, total, idx-1);
		else 
			return getAllSet(a, total, idx -1) + getAllSet(a, total-a[idx], idx-1); //include + exclude
		
	}
	// dynamic programming
	// hashmap skip dup calucation
	public int getAllSet(int a[], int total, int idx, HashMap<String, Integer> hp, Stack<Integer> count ) {
		String key = String.valueOf(total) +":"+ String.valueOf(idx);
		if(hp.containsKey(key)) { return hp.get(key);}
		
		if(0==total) return 1;
		else if(total < 0) return 0;
		else if(idx < 0) return 0;
		
		int result = 0;
		if(total < a[idx]) {
			result = getAllSet(a, total, idx-1, hp, count);
		}
		else  {
			result = getAllSet(a, total, idx-1, hp, count) + getAllSet(a, total-a[idx], idx-1, hp, count);
		}
		
		hp.put(key, result);
		
		return result;
		
	}
	public void getAllSetTest() {
		
//		int a[] = {2,4,6,10};
		int a[] = {2,4,6,10,20,25,30};
		int total =38;
		int set = getAllSet(a, total, a.length-1);
		HashMap<String, Integer> hp = new HashMap<String, Integer>();
		Stack<Integer> st = new Stack<Integer>();
		int setHP = getAllSet(a, total, a.length-1, hp, st);
		
		System.out.println("array = " + Arrays.toString(a) + " total = " + total + " num of set = " + set);
		System.out.println("array = " + Arrays.toString(a) + " total = " + total + " num of set = " + setHP + " hashmp = " + hp + " hp size =  " + hp.size());
		System.out.println(st.size());
		
	}
	/*
	 * 0. depth first search
	 * 1. print level == string len
	 * 2. loop thru string, index = level
	 * 3. swap(level , index i)
	 * 4. recursive(s, level+1, string len)
	 * 5. swap back. 
	 */
	public void permutation(char[] s, int l, int r) {
		if(l==r)
			System.out.println(s);
		
		for(int i=l; i < s.length; i++) {
		
			//swap
			char tmp = s[l];
			s[l] = s[i];
			s[i] = tmp;
			permutation(s,l+1, r);
			//swap back
			tmp = s[l];
			s[l] = s[i];
			s[i] = tmp;
		}
		
	}
	public void doPermutation() {
		char s[] = {'a','b','c'};
		permutation(s, 0, s.length);
//		System.out.println(s);
	}
	
	// Given an array[4,2,0,0,2,0], the value are the height with respect index and 
	// the height is the amount of hops
	// Find if can possible to hop pass the array size/bound.
	// Solution: Loop thru the recurive such that for i=idx+1 to height+idx. trick is height=-0
	// if current= array[idx] = 0 return false
	// else if current + idx >= array.length return true;
	// return result = recursive
	public boolean towerHopper(int a[], int idx) {
		
		int height = a[idx];
		boolean result = false;
		if(height == 0) return false;
		else if(height + idx >= a.length ) return true;
		
		// height is compare bc case height = 0,break out of loop
		// and return false.
		for(int i=idx+1; i<=height+idx; i++) {
			result = towerHopper(a, i);
			if(result==true) return result;
		}
		
		return result;
	}
	
	public void doTowerHopper() {
		
//		int a[] = {1,1,0,1,3,1,0};
//		int a[] = {4,2,0,0,2,2,0};
		int a[] = {2,0,1,0};
		System.out.println("tower hopper = " + towerHopper(a, 0));
	}
	
	public static void main(String [] args) {

		AlgoJava test = new AlgoJava();
//		test.reverseArray();
//		test.fib(8);
		test.minCointTest();
//		test.getAllSetTest();
//		test.doPermutation();
		test.doTowerHopper();
	}
}

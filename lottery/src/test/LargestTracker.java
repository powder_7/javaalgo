package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

public class LargestTracker {
	
	class Node {
		int x;
		Node l;
		Node r;
		Node (int a) {
			x=a;
		}
	}
	@Test
	public void test() {
		try {
			int numberOfTopLargestElements = 4;
			LargestTracker lt = LargestTracker.getInstance();
			
			lt.add(5);
			lt.add(1);
			lt.add(2);
			lt.add(3);
			lt.add(4);
			lt.add(5);

			assertEquals("[3, 4, 5, 5]", lt.getNLargest(4).toString());

			System.out.println(lt.getNLargest(numberOfTopLargestElements).toString());
			lt.clear();
			
			if (lt.getNLargest(1).isEmpty())
				System.out.println("list is empty");
			
			int a[] = {5,6,8,2,4,1,9,3,9};
			numberOfTopLargestElements = 6;
			for(int i: a) {
				lt.add(i);
			}
			System.out.println(lt.getNLargest(numberOfTopLargestElements).toString());
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	private static LargestTracker largest_tracker_stance = null;
	private Node root=null;
	
	/**
	 * Guarantees the creation of a single instance across the virtual machine.
	 * Assumed to be called very frequently.
	 *
	 * @return an instance of LargestTracker
	 */
	static  LargestTracker  getInstance() {
		if(null==largest_tracker_stance) {
			 synchronized(LargestTracker.class) {
				largest_tracker_stance = new LargestTracker();
			}
		}
		return largest_tracker_stance;
	}

	/**
	 * Returns a list in O(n log m) time OR BETTER where n is the number of
	 * entries added to LargestTracker and m is numberOfTopLargestElements.
	 * Duplicates are allowed
	 *
	 * @param numberOfTopLargestElements
	 *            the number of topmostelements to return
	 * @return the topmostelements in the tracker sorted in ascending order
	 */
	List<Integer> getNLargest(int numberOfTopLargestElements) {

		int k[] = new int[1];
		k[0] = numberOfTopLargestElements;
		List<Integer> tmp_list = new ArrayList<Integer>();
		
		if(numberOfTopLargestElements <= 0 || null == root) 
			return tmp_list;
 		
		tmp_list = getList(root, k, tmp_list );

		Collections.reverse(tmp_list);

		return tmp_list;
	}

	/**
	 * Adds an entry to the tracker. This method must operate in O(log n) time
	 * OR BETTER
	 * 
	 * @param anEntry
	 *            the entry to add to the tracker. Entries need not be unique.
	 */
	void add(int anEntry) {
		root = insert(root, anEntry );
	}

	/**
	 * Removes all the entries from the tracker. This should return in constant
	 * time.
	 */
	void clear() {
		root = null;
	}
	
	/**
	 * 	O(log n) Insert.
	 * 
	 * @param n
	 * 			n is node.
	 * @param a
	 * 			a is integer to be inserted
	 *
	 * @return	return BST node.
	 */
	Node insert(Node n, int a) {
		
		if(n == null) 
			return new Node(a);
		if(a <= n.x  )  
			n.l = insert(n.l, a);
		else  
			n.r = insert(n.r, a);
		
		return n;
	}
	
	/**
	 * O(n)   n is the number of entries added to LargestTracker.
	 * 
	 * @param n
	 *          n is root node.
	 * @param k
	 *          k[0] is number of top largest elements. 
	 * @param tmp_list
	 *          is a List to store elements.
	 * @return
	 *         return a List of stored elements.
	 */
	List<Integer> getList(Node n, int k[], List<Integer> tmp_list) {
  
		if(null==n || k[0] <= 0 ) 
			return null;
		
		List<Integer> tmp = getList(n.r, k, tmp_list);
		if(null!=tmp) 
			return tmp;
 
		tmp_list.add(n.x);
		if(--k[0] == 0) 
			return tmp_list;
		
		return getList(n.l, k, tmp_list);
	}
 	
}

package test;

import java.util.Stack;

import org.junit.Test;

public class FillWithColor {

	@Test
	public void test() {
		System.out.println("test");
		fillWithColorTest();
		
	}
	
	
	public void fillWithColorTest() {
		//a[x[y]
		//need to init array y,x instead of x,y
//		case 1
//		int a[][] = {{0,1,0},
//					 {0,1,0},
//					 {1,1,0} };
//		bitmap bm = new bitmap(3,3);
		
//		case 3
		int a[][] = {{1,2,0},
					 {1,1,2},
					 {2,2,0} };
		bitmap bm = new bitmap(3,3);
		
//		case 4
//		int a[][] = {{0,1,0,0},
//					 {0,4,0,5},
//					 {1,2,0,1},
//					 {0,0,0,1}};
//		bitmap bm = new bitmap(4,4);

		// 		case 5
//		int a[][] = {{1,1,3,4,4},
//					 {2,1,2,2,4},
//					 {2,1,1,2,2},
//					 {3,2,2,2,1},
//					 {3,5,5,4,1}};
//		bitmap bm = new bitmap(5,5);
		
		//setPixel
		for(int j = 0; j < a.length; j++) {
			for(int i=0; i< a[0].length; i++) {
//				System.out.print(a[i][j] + " ");
				bm.setPixel(i, j, a[i][j]);
			}
//			System.out.println("");
		}
		bm.printBitMap();
		System.out.println("result");
		bitmap test;
		//fill 1 at 0,0
		test = fillWithColor(bm, 1, 1, 3);
		test.printBitMap();
		
	}
	
	// tree with DFS using stack
	public bitmap fillWithColor(bitmap input, int x, int y, int color) {
		
		Stack<bitmapXY> st = new Stack<bitmapXY>();
		//get init color aka root
		int init_color = input.getPixel(x, y);
		bitmapXY bm = new bitmapXY(x,y);
		
		st.add(bm);
		while(!st.empty()) {
			 
			//hack if color is same as init color
			if(st.size() > (input.getHeight()*input.mWidth)) break;
			bitmapXY  tmp = st.pop();
			//set color to input
			input.setPixel(tmp.mLocX, tmp.mLocY, color);
			// add all the nodes to tree
			// or add adj to stack
			//check top adj
			if(tmp.mLocY > 0) {
				if(init_color == input.getPixel(tmp.mLocX, tmp.mLocY-1)) {
					//add top adj to stack
					st.add(new bitmapXY(tmp.mLocX, tmp.mLocY-1));
 				}
			}
			//check bottom adj
			if(tmp.mLocY < input.getHeight()-1) {
				if(init_color== input.getPixel(tmp.mLocX, tmp.mLocY+1))  {
					//add bottom adj to stack
					st.add(new bitmapXY(tmp.mLocX, tmp.mLocY+1));
				}
			}
			//check left adj
			if(tmp.mLocX > 0) {
				if(init_color == input.getPixel(tmp.mLocX-1, tmp.mLocY)) {
					//add left adj to stack
					st.add(new bitmapXY(tmp.mLocX-1, tmp.mLocY));
 				}
			}
			//check right adj
			if(tmp.mLocX < input.getWidth()-1) {
				if(init_color == input.getPixel(tmp.mLocX+1, tmp.mLocY)) {
					//add right adj
					st.add(new bitmapXY(tmp.mLocX+1, tmp.mLocY));
				}
			}
		}
		return input;
	}
	
	class bitmapXY {
		public int mLocX;
		public int mLocY;
		bitmapXY(int x, int y) {
			mLocX = x; 
			mLocY = y;
		}
	}
	class bitmap {
		int mWidth;
		int mHeight;
		int mColor[][];
		
	
		
		bitmap(int width, int height) {
			mWidth = width;
			mHeight = height;
			mColor = new int[width][height];
			
		}
		
		int getPixel(int x, int y) {
			return mColor[x][y];
		}
		
		void setPixel(int x, int y, int color) {
			mColor[x][y] = color;
		}
		
		int getWidth() {return mWidth;}
		
		int getHeight() {return mHeight;}
		
		void printBitMap() {
			int width = getWidth();
			int height = getHeight();
			
			for(int j=0; j< height; j++) {
				for(int i=0; i< width; i++) {
					System.out.print(getPixel(i, j) + " " );
					
				}
				System.out.println("");
			}
		}
	}
}

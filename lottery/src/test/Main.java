package test;

import java.io.IOException;
import java.math.BigDecimal;
//import org.json.*;

class AirPlane {
	public AirPlane() throws IOException {
		System.out.println("Airplane");
		throw new IOException();
	}
}

class AirJet extends AirPlane {
	public AirJet() throws IOException {
		
//		try {
////			super();
//		}
//		catch (IOException e) {
//			System.out.println("IOException is thrown in AirJet");
//		}
	}
}

class CreditCard {
	
	private int cardId;
	public void setCardId(int cardId) {
		cardId = cardId;
	}
	
	public int getCardId() {
		return cardId;
	}
}

public class Main {

	private static final String NO_TRANSACTIONS_ERR = "No transactions found.";
	private static final String NO_POSITIVE_ERR = "No positive transactions found.";

	/**
	 * Complete this function.
	 *
	 * @param inputValues an array of double values.
	 * @return "Average: XX.XX"
	 */
	static String getStatistics(Double[] inputValues) {
		String average = "Average: ";
        if(inputValues.length == 0) {
			return NO_TRANSACTIONS_ERR;
		}
		
		double avg = 0;
		int count = 0;
		for(Double d : inputValues) {
			if(d >0) {
				avg += d;
			}
			else  {
				count++;
			}
			if(count==inputValues.length) {
				
				return NO_POSITIVE_ERR;
			}
			
		}
        int total = inputValues.length-count;
        double tmp = avg/total;
		System.out.println(average + decimalFormatter(tmp));
        
		return average + decimalFormatter(tmp);
	}

	/**
	 * A helper method that returns a Double as a string in a 2 decimal format
	 *
	 * @param value the double value to format
	 * @return the value as a String with a 2 decimal format
	 */
	private static String decimalFormatter(Double value) {
		return String.format("%.2f", value);
	}
	
	
//	public static BigDecimal getBalanceForCategory(String inputJSON, String category) throws Exception {
//
//		BigDecimal balance = new BigDecimal(0.0);
//	  
//		JSONParser parser = new JSONParser();
//	    JSONObject jsonObject = (JSONObject) parser.parse(inputJSON);
//	    try {
//	        String transactions = (String)jsonObject.get("transactions");
//	        JSONArray arry = jsonObject;
//	        System.out.println("test = " + arry);
//	        
//	    }
//	     catch (Exception e) {
//			e.printStackTrace();
//	         
//		}
//	    
//		return balance;  
//	}
	
	public static void main(String [] args) {
		
		System.out.println("hello");
		
		CreditCard creditCard = new CreditCard();
		System.out.print("cardId = " + creditCard.getCardId());
		
//		try {
//			int coins[]={25,10,5,1};
//			int total = 30;
//			System.out.println("min coins = " + minNumberOfCoins(total, coins));
////			AirPlane aj = new AirJet();
//						 
//		}
//		catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
 
	}
	
	static int minNumberOfCoins(int total, int[] coinDenominations) {

	    if(total == 0) return 0;
	    int min = total;
	    
	    for(int i=0; i<coinDenominations.length; i++) {
	        if(coinDenominations[i] <= total) {
	            int noOfCoins = minNumberOfCoins(total-coinDenominations[i], coinDenominations)+1;
	            min = Math.min(min, noOfCoins);   
	        }
	    }
	    return min;
	}
}

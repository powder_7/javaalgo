package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Stack;

import org.junit.Test;

/*
 * Nathen Lam
 * 
 * Take mod 7 of string to get total double digits. Get double digits from string until mod value is zero. 
 * Get single digit from string. Store values in stack and hashset. Use hashet to detect duplication
 * If duplication or digit is zero, backtrack and remove until get a value that is double digits from stack. 
 * Get single digit from string then get double digit until mod value is zero. Check for zero,  duplication value and range.
 * 
 * O(N)
 * 
 *  Winning Ticket!

Your favorite uncle, Morty, is crazy about the lottery and even crazier about how he picks his “lucky” numbers. And even though his “never fail” strategy has yet to succeed, Uncle Morty doesn't let that get him down.

Every week he searches through the Sunday newspaper to find a string of digits that might be potential lottery picks. But this week the newspaper has moved to a new electronic format, and instead of a comfortable pile of papers, Uncle Morty receives a text file with the stories.

Help your Uncle find his lotto picks. Given a large series of number strings, return each that might be suitable for a lottery ticket pick. Note that a valid lottery ticket must have 7 unique numbers between 1 and 59.

For example, given the following strings:

[ “1”, “42". “100848", “4938532894754”]

Your function should return:

4938532894754 -> 49 38 53 28 9 47 54 
1234567 -> 1 2 3 4 5 6 7

 */


public class LotterPick {
	final int MAX_NUM=7;
	final int MAX_LEN=14;
	final boolean DEBUG=false;
	@Test
	public void test() {
  
		try {
			String str[] = { "1", "42", "100848", "4938532894754", "1234567" };
 
//	        System.out.println(Arrays.toString(str));
//	        for(String s: str) 
//	        	lot(s);
//
	        assertEquals("11 8 3 50 5 2 33 ", lot("1183505233"));
	        assertEquals("14 13 40 1 46 34 16 ", lot("1413401463416"));
	        assertEquals("49 38 53 20 40 47 5 ", lot("4938532040475"));
	        System.out.println(lot("9385320404710"));

//			List<Integer> al = new ArrayList<Integer> ();
//			List<Integer> list_lot = lot("1234567", al);
//			if(null!=list_lot)
//				System.out.println(list_lot.toString() );
//			else
//				System.out.println("Is NULL");
//			
//			al = new ArrayList<Integer> ();
//			list_lot = lot("1413401463416", al);
//			if(null!=list_lot)
//				System.out.println(list_lot.toString() );
//			else
//				System.out.println("Is NULL");
			
			
//	        for(String s: str) {
//	        	List<Integer> al = new ArrayList<Integer> ();
////	        	List<Integer> list_lot = lot(s, al);
//	        	List<Integer> list_lot = lot("1234567", al);
//				if(null!=list_lot)
//					System.out.println(list_lot.toString() );
//				else
//					System.out.println("Is NULL");
//	        }
	        
	        
 		} catch(Exception e){
           e.printStackTrace();
    	}
		 
	}
	
public List<Integer> lot(String s, List<Integer> list_lot) {
		
		List<Integer> result = list_lot;
 
		
		if(s.length()==0 && list_lot.size() == 7) return result;
		
		if(s.length() == 0 || list_lot.size() > 7) return null;
		
		if(s.length() >= 2 && list_lot.size() < 7) {
			int tmp = Integer.valueOf(s.substring(0, 2));
			if(tmp <= 59 ) {
				if(!list_lot.contains(tmp) && !s.substring(0,1).equals("0") )  {
					System.out.println(" len = " + s.length() +  " dd  = " + s.substring(0, 2) );
					list_lot.add(Integer.valueOf(s.substring(0, 2)) );
					result = lot(s.substring(2), list_lot);
				}
			}
		}
		else if(s.length() >=1 && list_lot.size() < 7 ) {
			if (!list_lot.contains( Integer.valueOf(s.substring(0, 1))) && !s.substring(0,1).equals("0") ) {
				System.out.println(" len = " + s.length() + " sd = " + s.substring(0, 1) );
				list_lot.add(Integer.valueOf(s.substring(0, 1)) );
				result = lot(s.substring(1), list_lot);
				
			}
	    }
		if(null == result && !list_lot.isEmpty()) {
			int tmp_list_size = list_lot.size()-1;
			int tmp = list_lot.remove(tmp_list_size);
			System.out.println("rm = " + tmp);
			if(s.length() >=1) {
				if(tmp > 9) {
					
					System.out.println(" len = " + s.length() + " sd = " + s.substring(0, 1) + " backtrack");
					result = lot(s.substring(0, 1), list_lot);
				}
			}
		}
		System.out.println(list_lot.toString());
		return result;
	}
	
	class LotObj {
		int lot;
		int start;
		int end;
		LotObj (int a, int b, int c) {
			lot = a;
			start = b;
			end = c;
		}
	}
	
	/*
	 * Returns and print a string with a valid lottery numbers. 
	 * 
	 * @param   str 	is a string of numbers.
	 * @return  String  of a valid lottery numbers.
	 */
	public String lot(String str) {
		
		int double_dig = str.length() % MAX_NUM;
		int end_idx = 1;
		int start_idx = 0;
		int tmp_num = -1;
		String tmp_str = "";
		Stack<LotObj> st_lot = new Stack<LotObj>();
		HashSet<Integer> hs_lot = new HashSet<Integer>();
		LotObj tmp_lot_obj = null;
		String result = "";
		
		if(DEBUG)
			System.out.println(" str = " + str + " dd = " + double_dig );
		
		// return if string length invalid length
		if(str.length() < MAX_NUM || str.length() >MAX_LEN) 
			return null;
				
		// return if string is not a valid integer
		for (int i = 0; i < str.length(); i++) {
			try {
				int tmp = Integer.parseInt(String.valueOf(str.charAt(i)));
				if(tmp < 0)
					return null;
			} catch (NumberFormatException e) {
				
				return null;
			}
		}
		// return if string start with zero
		if(str.charAt(0) == '0') 
			return null;
		
		// return if string has a number greater than 59
		for(int i = 0; i < str.length(); i++) {
			if(i > 0 && str.charAt(i) == '0') {
				int tmp = Character.getNumericValue(str.charAt(i-1));
				if(tmp > 5)
					return null;
			}
		}
		
		if(str.length() == MAX_LEN) 
			double_dig = MAX_NUM;
		
		if(double_dig>0) {
			end_idx += 1;
			double_dig--;
		}
		 
		while(end_idx <= str.length() && st_lot.size() <= MAX_NUM) {
		 
			if(start_idx == end_idx) {
				//no solution after backtrack
				if(DEBUG)
					System.out.println("Fail");
				break;
			}
			tmp_str = str.substring(start_idx, end_idx);
			tmp_num = Integer.valueOf(tmp_str);
			if(DEBUG)
				System.out.println(" lot = " + tmp_num + " s = " + start_idx + " e = " + end_idx );
			
			//check range
			if((tmp_num >= 1 && tmp_num <= 59) || tmp_num == 0) {
 
				if(!hs_lot.contains(tmp_num) && hs_lot.size() < MAX_NUM && tmp_str.charAt(0) != '0') {
					LotObj lot_obj = new LotObj(tmp_num, start_idx, end_idx);
					st_lot.add(lot_obj);
					hs_lot.add(tmp_num);
					if(DEBUG)
						System.out.println("add = " + tmp_num + " dd = " + double_dig);
				}
				else {
					//case 0X, x={0,..9}
					if(tmp_num == 0 || tmp_str.charAt(0) == '0') {
						if(tmp_str.length() > 1) 
							double_dig++;
					}
					// dup with 2 digit
					// XX, X={1,..,9}
					if(tmp_str.charAt(0) != '0' && tmp_str.length() > 1) {
						tmp_lot_obj = new LotObj(tmp_num, start_idx, end_idx);
						if(DEBUG)
							System.out.println("2 tmp_str.charAt(0) = " + tmp_str.charAt(0));
					}
					// dup with 1 digit or 0X, pop until get 2 digits
					else {
						while (!st_lot.isEmpty() ) {
						 	//remove in stack and hashset
							tmp_lot_obj = st_lot.pop();
							hs_lot.remove(tmp_lot_obj.lot);
							// double digit
							if (9 < tmp_lot_obj.lot) {
								if(!hs_lot.isEmpty()) {
									hs_lot.remove(tmp_lot_obj.lot);
									if(DEBUG)
										System.out.println("hs_lot.remove = " + tmp_lot_obj.lot);
									break;
								}
							}
						}
					}
				}
				// backtrack due to dup num
				if (null != tmp_lot_obj) {
					start_idx = tmp_lot_obj.start;
					end_idx = tmp_lot_obj.end - 1;
					double_dig++;
					if(DEBUG)
						System.out.println("debug backtrack num = "
								+ tmp_lot_obj.lot + " dd = " + double_dig);
					tmp_lot_obj = null;
				}
 				// normal
				else {
					start_idx = end_idx;
					if (double_dig > 0) {
						end_idx += 2;
						double_dig--;
					} else
						end_idx += 1;
				}
			}
			//out of range
			else {
				end_idx -= 1;
				double_dig++;
			}
		}
		
		if(DEBUG)
			System.out.print(str + " -> ");	
		if(st_lot.size() == MAX_NUM) {
			System.out.print(str + " -> ");	
			for(LotObj b: st_lot) {
				System.out.print(" " + b.lot);
				result += b.lot + " ";
			}
		}
		System.out.println("");
		return result;
	} 
	
}

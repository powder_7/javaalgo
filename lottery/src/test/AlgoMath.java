package test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

public class AlgoMath {

	//Make a data structure to represent a deck a cards and support operations including shuffle. 
	// lattice point is all the points on the circle r.
	// x^2 + y^2 = r^2
	// r=1, then 1=0+1^2, 1=1^2+0, 1=0+(-1^2), 1=(-1^2+0), total 4 lattice points
	// Or points in first quadrant X 4. special case when x=0 or y=0 and on r
	private void latticePtTest() {
		int r=25, k=11;
		int input[][] = new int[5][2];
		input[0][0]=1;
		input[0][1]=3;
		
		input[1][0]=1;
		input[1][1]=4;
		
		input[2][0]=4;
		input[2][1]=4;
		
		input[3][0]=25;
		input[3][1]=11;
		
		input[4][0]=25;
		input[4][1]=12;
		
		for(int i=0; i<input.length; i++) {
			r = input[i][0];
			k = input[i][1];
			System.out.println("r[" + r + "], k[" + k + "] lattice pt : " + latticePoint(r, k));
		}
	}
	
	/**
	 * 
	 * @param r is radius
	 * @param k max radius
	 * @return true if r<=k
	 */
	private boolean latticePoint(int r, int k) {
		int count=0;
		for(int i=0; i<=r; i++) {
			for(int j=0; j<=r; j++) {
				if(r == i*i+j*j) {
					//only 1 reflection point
					if(i==0 || j==0) {
						count+=2;
					}
					// 4 reflection points
					else {
						count+=4;
					}
				}
				else if(i*i>r || j*j > r || (i*i + j*j) > r) {
					break;
				}
			}
		}
		System.out.println("lattice pts count : "  + count);
		return (count<=k);
	}
	
 
	//Find if two rectangles overlap
	//
	//Given two rectangles, find if the given two rectangles overlap or not.
	//
	//Note that a rectangle can be represented by two coordinates, top left and bottom right. So mainly we are given following four coordinates.
	//l1: Top Left coordinate of first rectangle.
	//r1: Bottom Right coordinate of first rectangle.
	//l2: Top Left coordinate of second rectangle.
	//r2: Bottom Right coordinate of second rectangle.
	/*
	 *  x
	 *  l1----
	 *  |     |
	 *  -----r1
	 *  
	 *  l1>r2 or l2 > r1
	 *  
	 *  y
	 *  l2----
	 *  |     |
	 *  -----r2
	 *  
	 *  l1 < r2 or l2 <r1
	 * 
	 */
	public void isRecOverlapTest() {
		int l1[] = {0,10}; // top left
		int r1[] = {10,0}; // bottem right
		int l2[] = {9, 5};  
		int r2[] = {15,0};
		
		System.out.println("is Overlap : " + isOverLap(l1, r1, l2, r2));
	}
 
	public boolean isOverLap(int l1[], int r1[], int l2[], int r2[]) {
		
		// check 4 cases out of bound
		
		//x
		//left or right are out of bound
		//Rect 1 top left > Rect 2 bottom right or
		//Rect 2 top left > Rect 1 bottom right
		if(l1[0] > r2[0] || r1[0] <l2[0]) {
			return false;
		}
		//y
		//Rect 1 top left < Rect 2 bottom right or
		//Rect 2 top left < Rect 1 bottom right
		//top or bottom out of boud
		if(l1[1] < r2[1] || r1[1] > l2[1]) {
			return false;
		}
		
		return true;
	}
	public void doCoinChange() {
//		int a[]={1,5, 10};
//		int total =25;
		
		int a[]={2};
		int total = 3;
		
		
		System.out.println(" min coin change = " + minCoinChange(a, total));
//		System.out.println(" min coin change = " + minCoins(a, total));
	}
	// RunTime = O(NumOfCoins*Sum)
	// space   = O(Sum);
	// T[i] = min(1+T[i-coins[j], T[i]);
	public int minCoinChange(int []coins, int sum) {
		int T[] = new int[sum+1];
		T[0]=0;
		for(int i=1; i< T.length; i++) {
			T[i] = Integer.MAX_VALUE-1;
		}
		// Compute minimum coins required for all
        // values from 1 to sum
		for(int i=1; i<=sum ; i++) {
			 // Go through all coins smaller than i
			for(int j=0; j<coins.length; j++) {
				//coin <= total
				if(coins[j]<=i) {
					T[i] = Math.min((1+T[i-coins[j]]), T[i]);
				}
			}
		}
		
		return (T[sum]==Integer.MAX_VALUE-1)?-1: T[sum];
	}
	
	public int coinChange(int[] coins, int amount) {
        if(coins.length==0)
            return -1;
        
        
        int dp[] = new int[amount+1];
        for(int i =1; i<dp.length;i++){
            dp[i] = amount + 1;
        }
    
        for(int coin : coins){
            for(int i = coin;i<=amount; i++){

                dp[i] = Math.min(dp[i],dp[i-coin]+1);
                
            }
        }
        
        return dp[amount]>=amount+1?-1:dp[amount];
        
        
        
    }
	
	// 
	// If V == 0, then 0 coins required.
	// If V > 0
	// minCoin(coins[0..m-1], V) = min {1 + minCoins(V-coin[i])}
	// where i varies from 0 to m-1 and coin[i] <= V
	// Note..DFS and loop thru coin[], return 0 or min coin if found else maxbacktrack to previous node.
	// O(2^n)
	public int minCoins(int coins[], int V) {
		if (V == 0)
			return 0;
		int result = Integer.MAX_VALUE;

		for (int coin : coins) {
			if (coin <= V) {
				int backtrack = minCoins(coins, V - coin) + 1;
				result = Math.min(result, backtrack);
			}
		}
		return result;
	}
	
	// doordash interview question
	// test suggest using binary search
	public void sqrtTest() {
	
//	    System.out.println(squareRoot(4.0f, 0.00000002f));
//	    // 2.05^2 = 4.2025
	    System.out.println(squareRoot(150.0f, 0.2f));
	    System.out.println(squareRoot(4.0f, 0.01f));
//	    System.out.println(squareRoot(36.0f, 0.001f));
	    
	    System.out.println(squareRoots(150.0d, 0.2d));
	    System.out.println(squareRoots(4.0d, 0.01d));
//	    System.out.println(squareRoots(36.0d, 0.001d));
	}
	
	public float squareRoot(float n, float pre) {
	    
	    float x = n;
	    float y = 1;
	    
	    while((x*x -n) > pre) {
	      x = (x+y)/2;
	      y = n /x;
	    }
	    
	    return x;
	}
	
	public double squareRoots(double n, double pre) {
		
		double lower = 1;
		double upper = n;
		double guess = 0;
		
		//KEY
		//upper - lower less than pre
		while((upper-lower) > pre ) {
			
			guess = (lower+upper)/2.0;
			//lower bound
			if(guess*guess > n ) {
				upper = guess;
			}
			// upper bound
			else {
				lower = guess;
			}
		}
		return guess;
	}
	
	public void doAddToArray() {
		
		int a[] = {9,9,9,9};
		AddToArrayV2(a);
		AddOneArray(a);
	}
	// Given an arraay [1,2,3,4] the value is 1,234 add 1 and output is 1,235 break 
	// Result is [1,2,3,5]
	public void AddToArrayV2(int a[]) {
		
		String s ="";
		for(int x: a) {
			s += String.valueOf(x);
		}
		System.out.println(s);
	
		int total = Integer.valueOf(s);
		total++;
		s = String.valueOf(total);
		System.out.println(Arrays.toString(s.toCharArray()));
	}
	//convert String to int
	public void StrToInt() {
		
		String s = "a23456789";
		int len = s.length();
		int answer = 0;
		for(char a : s.toCharArray()) {
			int x =  (int) Math.pow(10, len-1) * Integer.valueOf(""+a); 
			answer += x;
			len--;
		}
		
		System.out.println(answer);
	}
	
	public void StrToHash() {
		String s = "a23456789";
		String acii="";
		for(char c : s.toCharArray()) {
			acii += (int) c;
		}
		System.out.println(acii);
	}
	//if 9 ,[1,0,9], add one to the right most and carry one if 10 and set to 0, loop
	//else, add one and break;
	//[9,9,9], resize array and output is [1,0,0,0] break
	public void AddOneArray(int a[]) {
		
		int len = a.length;
		int carry = 1;
		int b[]= null;
		for(int i=len-1; i>= 0; i--) {
			//carry because of 10
			if(a[i]==9) { 
				carry =1;
				a[i]=0;
			}
			// add one no carrying
			// less than 10
			else {
				carry = 0;
				a[i] +=1;
				break;
			}
			//carry
			if(carry==1) {
				// at left most and idx 0 so create new array and set 1
				if(i==0) {
					b = new int[len+1];
					b[0]=1;
					System.out.println(Arrays.toString(b));
				}
			}
		}
		if(b==null)
			System.out.println(Arrays.toString(a));
	}
	class Points {
		int x;
		int y;
		Points(int a, int b) {
			x=a;
			y=b;
		}
		public double distance() {
			return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
		}
	}
	
	public void doKthOrigin() {
		List<Points> list = new ArrayList<Points>();
		int k = 2;
		list.add(new Points(1,1));
		list.add(new Points(2,2));
		list.add(new Points(-1,-1));
		list.add(new Points(3,3));
		
		KthOrigin(list, k);
	}
	
	//Given a list of points, find the Kth closest to origin (0,0)
	public void KthOrigin(List<Points> list, int k) {
		int originx = 0,originy=0;
		List<Points> sortList = new ArrayList<Points>();
		//distant formual. d = sqt(deta(x)^1 - delta(y)^2)
		 Queue<Points> pq = new PriorityQueue<>((a, b) -> Double.compare(b.distance(), a.distance()));
		
		list.forEach(p->  {
			sortList.add(p);
			pq.add(p);
		});

		
//		Collections.sort(sortList,  new Comparator<Points>() {
//		    @Override
//		    public int compare(Points o1, Points o2) {
//		    	return Double.compare(o1.distance(), o2.distance());
//		    };
//		});
		
		System.out.println("Kth closest origin ");
		pq.forEach(p->System.out.println(p.distance() + " " + p.x + "," + p.y));
		
		sortList.sort((a,b)->  Double.compare(a.distance(), b.distance()));
//		while(sortList.size() > k) {
//			sortList.remove(sortList.size()-1);
//		}
		sortList.forEach(p->System.out.println(p.distance() + " " + p.x + "," + p.y));
		
		

	}
 	/*
	 *  counter is the size of powerset
	 	set size is length of array
	 	loop thru powerset
	    if counter
		1. Get the size of power set
	    powet_set_size = pow(2, set_size)
		2  Loop for counter from 0 to pow_set_size
	     (a) Loop for i = 0 to set_size
	          (i) If ith bit in counter is set
	               Print ith element from set for this subset
	     (b) Print seperator for subsets i.e., newline
	 *  https://www.geeksforgeeks.org/power-set/
	 */
	// Given a set of distinct integers, nums, return all possible subsets (the power set).
	//http://www.goodtecher.com/leetcode-78-subsets-java/
	public void printPowerSet() {
		char set[] = {'a','b','c'};
//		int set[] = {1,2,2};
		int nums[] = {1,2,2};
		int set_size = set.length;
		/*
		 * set_size of power set of a set with set_size n is (2**n -1)
		 */
		long pow_set_size = (long) Math.pow(2, set_size);
		int counter, j;

		/*
		 * Run from counter 000..0 to 111..1
		 */
		for (counter = 0; counter < pow_set_size; counter++) {
			for (j = 0; j < set_size; j++) {
				/*
				 * Check if jth bit in the counter is set If set then pront jth element from set
				 */
				// left shift 1 by j value
				if ((counter & (1 << j)) > 0) {
					System.out.print(set[j]);
				}
			}
			System.out.println();
		}
		
        List<Integer> list = new ArrayList<>();
        List<List<Integer>> result = new ArrayList<>();
        int len = nums.length;
        long power_size = (long)Math.pow(2, len);
        
        for(int i = 0; i < power_size; i++) {
            
            for(j = 0; j < len; j++) {
                if( (i & (1<<j)) > 0 )
                    list.add(nums[j]);
            }
            
            result.add(list);
            list = new ArrayList<>();
        }
        System.out.println(result);
        

//		Set<ArrayList<Integer>> results = new HashSet<ArrayList<Integer>>();
//
//		Arrays.sort(nums);
//		int size = nums.length;
//		results.add(new ArrayList<Integer>());
//
//		for (int i = 0; i < size; i++) {
//			Set<ArrayList<Integer>> newResult = new HashSet<>(results);
//			for (ArrayList<Integer> listt : results) {
//				ArrayList<Integer> newList = new ArrayList<Integer>(listt);
//				newList.add(nums[i]);
//				newResult.add(newList);
//			}
//			results = newResult;
//		}
//		ArrayList<ArrayList<Integer>> finalResult = new ArrayList<ArrayList<Integer>>(results);
//
//		System.out.println(finalResult);
	}
	//Given a collection of integers that might contain duplicates, nums, 
	// return all possible subsets (the power set).
	//https://leetcode.com/problems/subsets-ii/description/
	public List<List<Integer>> subsetsWithDup() {
//		int nums[] = {1,2,2};
		int nums[] = {4,4,4,1,4};
		Arrays.sort(nums);
		List<Integer> list = new ArrayList<>();
		List<List<Integer>> result = new ArrayList<>();
		Set<List<Integer>> set = new HashSet<>();
		int len = nums.length;
		long power_size = (long) Math.pow(2, len);

		for (int i = 0; i < power_size; i++) {

			for (int j = 0; j < len; j++) {
				if ((i & (1 << j)) > 0)
					list.add(nums[j]);
			}
			set.add(list);
			list = new ArrayList<>();
		}
		result.addAll(set);
		System.out.println(result);
		
		return result;
	}

	// Given a set of distinct integers, nums, return all possible subsets (the power set).
	//http://www.goodtecher.com/leetcode-78-subsets-java/
	//
	public void subsetTest() {
		int a[] = {1,2,3};
		System.out.println(subsets(a));
	}

	public List<List<Integer>> subsets(int[] nums) {
		List<List<Integer>> results = new ArrayList<>();

		if (nums == null || nums.length == 0) {
			return results;
		}

		Arrays.sort(nums);

		List<Integer> subset = new ArrayList<>();
		toFindAllSubsets(nums, results, subset, 0);

		return results;
	}

	private void toFindAllSubsets(int[] nums, List<List<Integer>> results, List<Integer> subset, int startIndex) {
		results.add(new ArrayList<>(subset));

		for (int i = startIndex; i < nums.length; i++) {
			subset.add(nums[i]);
			toFindAllSubsets(nums, results, subset, i + 1);
			subset.remove(subset.size() - 1);
		}
	}

	class feq { 
		int val;
		int feq;
		
		feq(int a, int b) {
			this.val = a;
			this.feq = b;
			
		}
 
	}

	public void topKFrequent() {
		int a[] = {1,9,9,9,2,2,2,3};
		int k=2;
		System.out.println(topKFrequent(a,k));
		System.out.println(topKFrequent2(a,k));
		
	}
	
	// fb interview questions.
	// Given a non-empty array of integers, return the k most frequent elements.
	// Given [1,1,1,2,2,3] and k = 2, return [1,2].
	public List<Integer> topKFrequent2(int[] nums, int k) {

		Map<Integer, Integer> map = new HashMap<>();
		for (int it : nums)
			map.put(it, map.getOrDefault(it, 0) + 1);
		
		PriorityQueue<Map.Entry<Integer, Integer>> pq = new PriorityQueue<>((a, b) -> b.getValue() - a.getValue());
		
		for (Map.Entry<Integer, Integer> entry : map.entrySet())
			pq.offer(entry);
		
		List<Integer> res = new ArrayList<>();
		
		while (k > 0) {
			res.add(pq.poll().getKey());
			k--;
		}
		
		return res;

	}
	
    public List<Integer> topKFrequent(int[] nums, int k) {
        List<feq> list = new ArrayList<feq>();
        List<Integer> result = new ArrayList<>();
        TreeMap<Integer, Integer> tm = new TreeMap<Integer, Integer>();
        
        for(int x: nums) {
			if(tm.containsKey(x)) 
				tm.put(x, tm.get(x)+1);
			else 
				tm.put(x, 1);
		}
        // or 
//        for(int x: nums) {
//        	tm.put(x, tm.getOrDefault(x, 1) +1);
//        }
        for(Entry<Integer, Integer> entry : tm.entrySet()) 
			list.add(new feq(entry.getKey(), entry.getValue()));
        
        list.sort((o1,o2)->Integer.compare(o2.feq, o1.feq));
        
        int i = 0;
        for(feq f: list) {
            if(i<k) {
                result.add(f.val);
            }
            else  break;
            i++;
        }
        
        return result;
    }
	//Given array with the height of histagram with respect with its index.
	//Find the largest area under the graph
	//Solution: Loop thru the array, push in stack the index steping up the graph,
	// if stack.peek <= array[i] push, i++;
	// else if(stack.empty) area = aray[i] * i
	// else area = ary[i] * delta I -1 or (i-stack.peek-1). i, is right edge, stack.peek is left edge
	// process the stack when loop is done.
	public int histagramArea(int hist[]) {
		
//		int hist[] = {1,1,1};
		int area = 0;
		int maxArea = 0;
		int top = 0;
		Stack<Integer> st = new Stack<Integer>();
		
		for(int i=0; i< hist.length; ) {
			
			// walk the stair
			if(st.empty() || st.peek() <= hist[i]) {
				st.push(hist[i++]);
			}
			// down stair pop stack
			else {
				top = st.pop();
				//stack empty mean everything up to i is greater or equal to hist[top]
				if(st.empty()) {
					area = hist[top]*i;
				}
				else {
					// delta i = rigtt edge - left edge - offset
					area = hist[top] * (i - st.peek() -1);
				}
				
				maxArea = Math.max(area, maxArea);
			}
			// proces stack
			while(!st.empty()) {
				top = st.pop();
				//stack empty mean everything up to i is greater or equal to hist[top]
				if(st.empty()) {
					area = hist[top]*i;
				}
				else {
					// delta i = rigtt edge - left edge - offset
					area = hist[top] * (i - st.peek() -1);
				}
				
				maxArea = Math.max(area, maxArea);
			}
		}
		
		return maxArea;
	}
	public void doHistArea() {
		
//		int hist[] = {2,2,2,6,1,5,4,2,2,2,2};
		int hist[] = {2,2,3};
		
		System.out.println("max area histagram = " + histagramArea(hist));
		
	}
	
	// Given array, find two numbers that multiple n
	// Solution:  loop thru array and hash the value to hold already read 
	// and module n to list
	// if module=0, check answer contain in list
	// if 20/4 = 5, check if 5 contain in list, then print
	public void findTwoMulitples() {
		int a[] = {2,3,4,5,10,25};
		int n = 20;
		 
	    Set<Integer> list = new HashSet<Integer>();
 
	    for(int x: a) {
	    	list.add(x);
	    	if(n%x==0) {
	    		int div = n/x;
	    		if(list.contains(div)) {
	    			System.out.println(n + " = " + x + " * " +div);
	    		}
	    	}
	    }
 	}
	
	//Given an integer N that represent the total stair steps.
	//Find all the combonations of steps to reach top. Only 1 or 2 steps
	// or an array with {1,3,5} steps to take.
	// IE. Input 1: Output: {[0,1]} ->1
    //	   Input 2: Output: {[0,1,2],[0,2] -> 2
	//     Input 3: Output: {[0,1,2,3],[0,1,3],[0,2,3]}->3
	// Fibbinaci Sequence.
	public void stairWays() {
		
		int n=1, n2=2;
		int num=5;
		int result=0;
		for(int i=3; i<=num; i++) {
			
			result = n+n2;
			n=n2;
			n2=result;
		}
		
		System.out.println("num of ways staircase walk = " +result);
	}
	// Find 2 elements in array add to sum
	// Trick: check if diff y=sum-x is in set
	// if contain then found 
	// else add to set
	public void subSetSum()  {
		int a[]= {1,2,3,4};
		int sum = 5;
		Set<Integer> set = new HashSet<>();
		int y=0;
		for(int x : a) {
			y = sum-x;
			if(set.contains(y)) {
				System.out.println(y + " + " + x + " = " + sum);
			}
			set.add(x);
		}
	}
	
	public void doSubarraySum() {
		
		int nums[] =  {1, 2, 3, 4, -2 ,-3, -4};
		System.out.println(subarraySum(nums));
	}
	/*
	 * Given an integer array, find a subarray where the sum of numbers is zero. 
	 * Your code should return the index of the first number and 
	 * the index of the last number.
	 * 
	 * Trick, hash all the numbers of sums, when you add x and minus x, it
	 * goes back to where that has contain of the sums.
	 * ie. 1,2,3,4,-2,-3,-4
	 *     0 1 2 3  4  5  6
	 *     
	 *     sum=0=2+3+4-2-3-4
	 *     idx 1,6
	 * 
	 *   hash z: +x+x+x
	 *           -x-x-x
	 *   go back to z. walk 3 step up walk to step back you are back at z
	 *   where you start. 
	 */
	public ArrayList<Integer> subarraySum(int nums[]) {
	
		// write your code here
		ArrayList<Integer> res = new ArrayList<>();
		if (nums == null || nums.length == 0)
			return res;
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		int sum = 2;
		int cur_val = 0;
		map.put(0, 0); //trick
		for (int i = 0; i < nums.length; i++) {
			cur_val += nums[i];
			if (map.containsKey(cur_val - sum)) {
				res.add(map.get(cur_val - sum));
				res.add(i);
//				return res;
			}
			map.put(cur_val, i + 1);
		}
		return res;
	}
	/*
	 * Find subarray with given sum. 
	 * IE, [1,2,3,4], sum = 5
	 * output: 1,2
	 * 
	 * Trick:
	 *  loop
	 *  add cur += array[i]
	 *  if cur==sum print 0,i
	 *  if hashmap contains cur-sum, print hashmap value, i, 
	 *  because need to store/resturn index of array
	 *  
	 *  else hashmap cur-sum
	 * 
	 * Trick: This is same concept as given array find 2 product of N
	 * Loop array and hash value
	 *  if(N % array[i]==0)
	 *  Print hash containkey N/array[i]
	 *  
		1,2,3,4     5    
		cur+ ary[i]    i     cur-sum    hash
		1              0     -4         1,0
		3              1     -2         3,1
		6              2      1         6,2
		
		print hash(1)+ 1, i -> 1,2
 
	 * 
	 */
	public void subArraySum() {
//		int a[] = { 2, 2, 3, -5 };
		int a[]= {1, 2, 3, 4, -2 ,-3, -4};
		int sum = 2;
		int cur_val = 0;
		// hash store key,current sume, value is index
		Map<Integer, Integer> hm = new HashMap<>();
		
		hm.put(0, 0);// trick*****
		// loop thru array
		for (int i = 0; i < a.length; i++) {
			cur_val += a[i];

			if (hm.containsKey(cur_val - sum)) {
				int hash = hm.get(cur_val - sum);
				System.out.println(hash + " to " + i + " = " + sum + " cur sum = " + (cur_val - sum));
			}
			// hash cur_val
			hm.put(cur_val, i + 1);
		}
	}
	
	/*
	 * Given a string '1234' how many ways to decode it with a given alphabet hash values
	 * a:1, b:2,c:3,...z:26.
	 * Output: 3.
	 * Trick: Fibbinocci sequence, a[n] = a[n-1] + a[n-2]
	 *  case 1: single digit  n = a[n-1]
	 *  case 2: double digit n = a[n-1] + a[n-2]
	 *  Dynamic programming or use 2 variables update and return 3rd variable
	 * WOW!!!!!!!
	 */
	public void doNumWays() {
		
		String s = "1210";
		System.out.println("num ways = " + numWays(s));
		
	}
	public int numWays(String s) {
		
		if(s.length()==0 || s==null || s.charAt(0)=='0') return 0;
		if(s.length()==1) return 1;
//		int a[] = new int[s.length()+1];
//		a[0]=1;a[1]=1;
//		for(int i=2; i <= s.length(); i++) {
//			int singleDigit = Integer.valueOf(s.substring(i-1, i));
//			int doubleDigit = Integer.valueOf(s.substring(i-2, i));
// 			
//			if(singleDigit != 0) 
// 				a[i] = a[i-1];
//			if(doubleDigit >= 10 && doubleDigit <= 26)  
//				a[i] +=a[i-2];
//			 
//		}
//	 	return a[s.length()];
		
		int a=1,b=1,c=1;
		
		for(int i=2; i <= s.length(); i++) {
            
//			if(s.charAt(i-1) > '0')
//                c = b;
//            
//            if((s.charAt(i-2) == '2' && s.charAt(i-1) <='6') || s.charAt(i-2) == '1')
//              c += a;  
			
			int singleDigit = Integer.valueOf(s.substring(i-1, i));
			int doubleDigit = Integer.valueOf(s.substring(i-2, i));
 			
			if(singleDigit != 0) 
				 c = b;
			if(doubleDigit >= 10 && doubleDigit <= 26)  
				 c += a;  
			a=b;
			b=c;
			c=0;
		}
		return b;
	}
	
	/*
	 * Ugly Number I Write a program to check whether a given number is an ugly
	 * number. Ugly numbers are positive numbers whose prime factors only include 2,
	 * 3, 5 . For example, 6, 8 are ugly while 14 is not ugly since it includes
	 * another prime factor 7 . Note that 1 is typically treated as an ugly number.
	 * 
	 * Answer: loop thru prime array.
	 * find all factors by 
	 * repeat (prime mod(%) num == 0) { 
	 * then, num = num / prime; } 
	 * //one is ugle num 
	 * if num==1 return true else false;
	 */

	public void doIsUglyNum() {
		int num = 14;
//		System.out.println("ugly prime = " + isUglyNum(num));
		int k = 20;
		System.out.println("kth ugly num =" + nthUglyNumber(k));
		
	}
	public boolean isUglyNum(int num) {
		if (num < 1)
			return false;
		int prime[] = { 2, 3, 5 };
		// loop array prime.
		for (int i = prime.length - 1; i >= 0; i--) {
			//find all factors
			while (num % prime[i] == 0) {
				num /= prime[i];
			}
		}
		return num == 1;
	}
	
	/*
	 * 
	 * Write a program to find the n-th ugly number. Ugly numbers are positive
	 * numbers whose prime factors only include 2, 3, 5 . 
	 * For example, 1, 2, 3, 4, 5, 6, 8, 9, 10, 12 is the sequence of the first 10 ugly numbers. 
	 * Note that 1 is typically treated as an ugly number.
	 *
	 * 1        = 1
	 * 2,3,5 x 1 = 2,3,5 
	 * 2,3,5 x 2 = 4,6,10 
	 * 2,3,5,x 3 = 6,9,15 
	 * 2,3,5 x 4 = 8,12,20
	 *       x 5 = 10,15,25
	 *       x 6 = 12,18,30
	 * 
	 * loop to nth /2 bc there have 3 primes 
	 * a=2 x i add to list(a,a) 
	 * a=3 x i add to list(a,a) 
	 * a=5 X i add to list(a,a)
	 * 
	 * loop thru to n of list and get value
	 */
	
	//O(nlogn) insert sort
	public int kthUglyNum(int k) {

		if (k < 1)
			return 0;
		int prime[] = { 1, 2, 3, 5 };
		int a = 0;
		if(k < 5 ) return prime[k-1];
		
		TreeSet<Integer> list = new TreeSet<Integer>();
		list.add(1);
		for (int i = 1; i <= k; i++) {
			a = prime[1] * i;
			list.add(a );
			a = prime[2] * i;
			list.add(a );
			a = prime[3] * i;
			list.add(a);
		}

		int j = 1;
		for (int num : list) {
			if (j == k)
				return num;
			j++;
		}
		return -1;
	}
	//O(n) linear time 
	public long nthUglyNumber(int k) {
		long[] memo = new long[k + 1];
		memo[1] = 1;
		int t2 = 1, t3 = 1, t5 = 1;
		for (int i = 2; i <= k; i++) {
			memo[i] = Math.min(memo[t2] * 2, Math.min(memo[t3] * 3, memo[t5] * 5));
			if (memo[i] == memo[t2] * 2)
				t2++;
			if (memo[i] == memo[t3] * 3)
				t3++;
			if (memo[i] == memo[t5] * 5)
				t5++;
		}
		return memo[k];
	}
	
	public int binarySearch() {
		int data[] = {1,2,3,4,5,6,7,8,9,10};
		int low=0;
		int high= data.length-1;
		int key = 1;

		while(high >= low) {
             int middle = (high + low) / 2;
            if(data[middle] == key) {
            	System.out.println(key);
                 return data[middle];
             }
             if(data[middle] < key) {
                 low = middle + 1;
             }
             if(data[middle] > key) {
                 high = middle - 1;
             }
        }
		return -1;
	}
	public void sortS1S2Test() {
		
		String s1 = "program";
		String s2 = "grapo";
		System.out.println(sortS1S2(s1, s2));
	}
	
	/*
	 * Given two lowercase strings, S1 and S2, sort S1 in same order as S2. 
	 * If a character in S1 doesn't exist in S2, put them at the end. 
	 * If S1 is "program" and S2 is "grapo", then return "grrapom".
	 */
	// Sort s1 base on S2, add char to the end of not in S2
	// hashmap s1, key is char, value is number of repeats
	// loop s2, 
	// check hashmap if containskey, add to sort string n times. or value.
	// remove in hashmap.
	// loop hashmap add key to sort string n times or value time
	// bc it was not in s2
	public String sortS1S2(String s1, String s2) {

		String sortStr="";
		Map<Character, Integer> hm = new HashMap<>();
		for( char c : s1.toCharArray() ) {
			if(hm.containsKey(c) ) {
				hm.put(c, hm.get(c)+1);
			}
			else {
				hm.put(c,1);
			}
		}

		for(char c : s2.toCharArray() ) {
			if(hm.containsKey(c)) {
				for(int i=0; i<hm.get(c); i++) {
					sortStr +=c;
				}
				hm.remove(c);
			}
		
		}
		
		for(Entry<Character, Integer> entry: hm.entrySet() ) {
			for(int j=0; j< entry.getValue(); j++ ) {
				sortStr += entry.getKey();
			}
		}

		return sortStr;
	}
	public void findMedianSortArraysTest() {
		
		int a[] = {1,3,};
		int b[] = {2,4};
		System.out.println(findMedianSortedArrays(a,b));
		
	}
	//4. Median of Two Sorted Arrays
	//https://leetcode.com/problems/median-of-two-sorted-arrays/description/
	public double findMedianSortedArrays(int[] nums1, int[] nums2) {
		double median = 0;
 
		int c[] = new int[nums1.length+nums2.length];
		int i=0;
		int j=0;
		int k=0;
		for(i=0, j=0; i<nums1.length && j<nums2.length; ) {
			if(nums1[i] < nums2[j]) {
				c[k++] = nums1[i++];
			}
			else {
				c[k++]= nums2[j++];
			}
		}
		
		while(i < nums1.length) {
			c[k++] =nums1[i++];
		}
		while(j < nums2.length) {
			c[k++] = nums2[j++];
		}
		 
		//even soo (n+n-1)/2
		if(c.length %2==0) {
			 median = (c[c.length/2] + c[(c.length/2-1)])/2.0 ;
		}
		// odd length
		else  {
			median = c[c.length/2];
		}
		
		return median;
	}
	
	public void mergeTwoArray() {
		
		int a[] = {1,3,5};
		int b[] = {2,4};
		int c[] = new int[a.length+b.length];
		int i=0;
		int j=0;
		int k=0;
		for(i=0, j=0; i<a.length && j<b.length; ) {
			if(a[i] < b[j]) {
				c[k++] = a[i++];
			}
			else {
				c[k++]= b[j++];
			}
		}
		while(i<a.length) {
			c[k++] =a[i++];
		}
		while(j<b.length) {
			c[k++] = b[j++];
		}
		int len=0;
		//even soo (n+n-1)/2
		if(c.length %2==0) {
			 double median = (c[c.length/2] + c[(c.length/2-1)])/2.0 ;
			System.out.println("median = " + median);
		}
		// odd length
		else  {
			len = + (c.length-1)/2;
			double median = c[c.length/2];
			System.out.println("median = " + median);
		}
		
		System.out.println(Arrays.toString(c));
	
	}
	
	// 53. Maximum Subarray
	/* Given an integer array nums, find the contiguous subarray 
	 * (containing at least one number) which has the largest sum and return its sum.
	 * https://leetcode.com/problems/maximum-subarray/description/
	 * 
	 * answer: loop thru array, add up all the interger n
	 * if sum <= 0 reset sum
	 * max = max(max,sum)
	 */
	public int maxSubArray(int[] nums) {
		int max = Integer.MIN_VALUE;
		int sum = 0;
		for(int a : nums) {
			sum += a;
			max = Math.max(sum, max);
			//reset sum 
			if(sum<=0) sum=0;
		}
		return max;
	}
	/*
	 *  
	 *  Given a permutation of a set, return the index of the permutation.
	 *
	 * Sample Input: {3, 1, 2}
	 * Sample Output: 4
	 *                                    x  y  z  0
	 * Now we can calculate the index of {2, 4, 3, 1} as: x=1, y=2, z=1:
	 *   x*3!+y*2!+z*1!+w*0 = 1*3! + 2*2! + 1*1! = 6 + 4 + 1 = 11.
	 *   
	 * Time Complexity of Solution:
	 *   Best = Average = Worst = O(n^2)
	 * Space Complexity of Solution:
	 *   Best = Average = Worst = O(1)
	 */
	public int permutationIndex() {
//		int[] permutation = { 3, 1, 2 };
		int[] permutation = { 2, 4, 3, 1};

		int index = 0;
		int position = 2;// position 1 is paired with factor 0 and so is skipped
		int factor = 1;
		for (int p = permutation.length - 2; p >= 0; p--) {
			int successors = 0;
			for (int q = p + 1; q < permutation.length; q++) {
				// compare the right 2 indexed values till end array
				if (permutation[p] > permutation[q]) {
					successors++;
				}
			}
			//                                x      y      z					
			// calcuate  x*3!+y*2!+z*1!+w*0 = 1*3! + 2*2! + 1*1! = 6 + 4 + 1 = 11.
			// successors are x,y z
			index += (successors * factor);
			// increase position and do factor n!
			// ie 1*2*3
			factor *= position;
			position++;
		}
		
		System.out.println(index);
		
		return index;
	}
	
	/*
	 * 0. depth first search
	 * 1. print level == string len
	 * 2. loop thru string, index = level
	 * 3. swap(level , index i)
	 * 4. recursive(s, level+1, string len)
	 * 5. swap back. 
	 */
	public void permutation(char[] s, int l, int r) {
		if(l==r)
			System.out.println(s);
		
		for(int i=l; i < s.length; i++) {
		
			//swap
			char tmp = s[l];
			s[l] = s[i];
			s[i] = tmp;
			permutation(s,l+1, r);
			//swap back
			tmp = s[l];
			s[l] = s[i];
			s[i] = tmp;
		}
		
	}
	public void doPermutation() {
		char s[] = {'a','b','c'};
		permutation(s, 0, s.length);
	
	}
//	// Returns true if two rectangles (l1, r1) and (l2, r2) overlap
//	public boolean doOverlap(Point l1, Point r1, Point l2, Point r2)
//	{
//	    // If one rectangle is on left side of other
//	    if (l1.x > r2.x || l2.x > r1.x)
//	        return false;
//	 
//	    // If one rectangle is above other
//	    if (l1.y < r2.y || l2.y < r1.y)
//	        return false;
//	 
//	    return true;
//	}
	
	@Test
	public void test() {
//		latticePtTest();
//		isRecOverlapTest();
//		doCoinChange();
//		sqrtTest();
//		doAddToArray();
//		doKthOrigin();
//		doAllSubSet();
//		printPowerSet();
//		topKFrequent();
//		doHistArea(); // look again over
//		findTwoMulitples();
//		stairWays();
//		doSubarraySum();
//		doNumWays();
//		doIsUglyNum();
//		binarySearch();
		subArraySum();
//		subSetSum();
//		sortS1S2Test();
//		subsetTest();
//		subsetsWithDup();
//		mergeTwoArray();
//		findMedianSortArraysTest();
//		StrToInt();
		StrToHash();
//		permutationIndex();
//		doPermutation();
	}
	
}

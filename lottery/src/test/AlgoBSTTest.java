package test;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;

import org.junit.Test;
 
public class AlgoBSTTest {
	public class Node {
		public int x;
		public char c;
		public Node l;
		public Node r;
		public Node(int a) {
			x=a;
			l=null;
			r=null;
		}
		Node(char a) {
			c=a;
			l=null;
			r=null;
		}
	}
	
	/*
	 * 	Ternary Expression
		no (), evaluate from right
		
		a?b:c
		if (a) {
		  return b;
		} else {
		  return c;
		}
		  a
		 / \
		b   c
		
		a?b?c:d:e
		    a
		   / \
		  b   e
		 / \
		c   d
		
		a?b:c?d:e
		   a
		  / \
		 b   c
		    / \
		   d   e
		
		input: String expr
		output: Expression      binary tree
	 *
	 */
	
	// root = s.charAt(0); push root to stack.
	// trick use stack to store node and update left, right node so have links
	// if ? push, add left node, push to stack, update left node
	// else : pop till right is null, add right node, push stack, update right node
	// note: push node is the parent of left and right *****
	public void ternary2BST() {
//		String s = "a?b:c";
//		String s = "a?b?c:d:e";
		String s = "a?b:c?d:e";
		Stack<Node> st = new Stack<Node>();
		Node rt = new Node(s.charAt(0));
		Node n = rt;
		System.out.println(s);
		for(int i = 1; i<s.length(); i+=2) {
			//left node
			if(s.charAt(i)=='?') {
				n.l = new Node(s.charAt(i+1));
				st.add(n);
				n = n.l;
			}
			//right node
			else if(s.charAt(i)==':') {
				// node is parent.
				n = st.pop();
				//pop till right is null, bc of definition a?b?c:d:e
				while(!st.empty() && n.r!=null)
					n = st.pop();
				n.r = new Node(s.charAt(i+1));
				st.add(n);
				n = n.r;
			}
		}
//		printBFS(rt);
//		String str = "2147483648";
//		if(str.length() > 0) {
//            int len = str.length()-1;
////            for(Character c : str.toCharArray() ) {
////                if (!Character.isDigit(c)) S;
////            }
////            for(int i=0;i<len;i++)
////            System.out.println( Math.pow(10, i));
//            System.out.println(Integer.MAX_VALUE);
//        }
		
		System.out.println("Ternary2BST");
		
//		Queue<Node>q = new LinkedList<Node>(); 
//		q.add(rt);
//		Node tmp;
//		int curr_lvl=0;
//		int next_lvl=0;
//		while(!q.isEmpty()) {
//			tmp = q.remove();
//			System.out.print(" " + tmp.c);		
//			
//			if(tmp.l!=null) {
//				q.add(tmp.l);
//				next_lvl++; 
//			}
//
//			if(tmp.r!=null) {
//				q.add(tmp.r);
//				next_lvl++;
//			}
//
//			if(curr_lvl == 0) {
//				curr_lvl = next_lvl;
//				next_lvl=0;
//				System.out.println("");
//			}
//			curr_lvl--;
//			
//		}
		printBFS(rt);
	}
	
	/*
	 * Transform a BST to greater sum tree
	 * Given a BST, transform it into greater sum tree 
	 * where each node contains sum of all nodes greater than that node
	 * Trick do Inorder reverse. Recursive and add leaf.
	 * Assign current node value to total sum 		
	 *  sum[0] += n.x;
	 *	n.x = sum[0];
	 * store current x in tmp, update node x with sum and update sum with tmp
             50
           /      \
         30        70
        /   \      /  \
      20    40    60   80 

      The above tree should be modified to following 

              260
           /      \
         330        150
        /   \       /  \
      350   300    210   80
	 * 
	 * https://www.geeksforgeeks.org/add-greater-values-every-node-given-bst/
	 * O(n)
	 */
	public void BSTSumGreaterTreeTest() {
		int sum[] = new int[1];
		sum[0]=0;
		int a[] = {50,30,20,40,70,60,80};
		Node root = null;
		root = buildTree(root, a);
		
		System.out.println("Before");
		printBFS(root);
		BSTSumGreaterTree(root, sum);
		System.out.println("After");		
		printBFS(root);
	}
	
	public void BSTSumGreaterTree(Node n, int sum[]) {
		if(null==n) return;
		BSTSumGreaterTree(n.r, sum);
 
		sum[0] += + n.x;
		n.x = sum[0];
		
		BSTSumGreaterTree(n.l, sum);
	}
	
	/*
	 * Given two values k1 and k2 (where k1 < k2) and a root pointer to a Binary Search Tree.
	 * Find all the keys of tree in range k1 to k2. i.e. 
	 * print all x such that k1<=x<=k2 and x is a key of given BST. 
	 * Return all the keys in ascending order.
	
		If k1 = 10 and k2 = 22, then your function should return[12, 20, 22].
		
		    20
		   /  \
		  8   22
		 / \
		4   12


	 */
	
	public void BSTRangeTest(Node root) {
		int r1 = 1;
		int r2 = 5;
		List<Integer> list = new ArrayList<Integer>();
		
		BSTRange(root, r1, r2, list);
		System.out.println("r1 = " + r1 + " r2 = " + r2 + 
				           "bst rang = " + list.toString());
	}
	
	
	public void BSTRange(Node n, int r1, int r2, List<Integer> list) {
		if(null==n) return;
		BSTRange(n.l, r1, r2, list);
		BSTRange(n.r, r1, r2, list);
		if(r1<=n.x && n.x <=r2)
			list.add(n.x);
	}
	
	/*
	 * Given a binary tree, flatten it to a linked list in-place.
	 * Answer: init: prev=root, cur=null;
	 * DFS, cur=st.pop, pre.left=null, pre.rigt = cur, update pre = cur;
	 */
	
	public void BST2LLTest() {
		
//		int a[] = {11,2,1,7,29,15,40,35,41, 42};
		int a[] = {11,2,29};
		
		Node root = null;
		root = buildTree(root, a);
		printBFS(root);
		
		Node prev = root;
		Node curr = null;
		Stack<Node> st = new Stack<Node>();
		st.push(prev);
		while(!st.isEmpty()) {
			curr = st.pop();

			if(null!=curr.r)
				st.push(curr.r);
			
			if(null!=curr.l)
				st.push(curr.l);
			
			if(curr!=prev) {
				prev.l = null;
				prev.r = curr;
				prev = curr;
			}
		}
		
		System.out.println("");
		while(null!=root) {
			System.out.print(" " + root.x);
			root = root.r;
		}
		System.out.println("");
			
	}
	
	//Check if binary tree balance
	public void isBSTBalanceTest(Node root) {
		
		if(-1 == isBSTBalance(root)) {
			System.out.println("NOT isBalance");
		}
		else {
			System.out.println("isBalance");
		}
		
	}
	// return -1 if difference left and right node greater than 1
	// return height of tree
	public int isBSTBalance(Node root) {
		
		if(root==null) return 0;
	
		int l = isBSTBalance(root.l);
		if(-1==l) return -1;
		int r = isBSTBalance(root.r);
		if(-1==r) return -1;
		
		//check if left and right node has a different of more than 1
		//not balance
		if(Math.abs(l-r) > 1) return -1;
		
		return Math.max(l, r)+1;
	}
	
	/*
	 * Determine if a given tree fulfills all requirements to be a binary search tree.
	 * Should be O(n)
	 * DFS and use min, max range compare to each node
	 * 
	 * Base case: Node is null return true.
	 * Node out of range, return false.
	 * 
	 * return DFS(root.l, min, root.x) && DFS(root.r, root.x, max)
	 */
	
	public void isBSTTest() { 
		int a[] = {3,2,5,1,4};
		Node root = null;
		root = buildTree(root, a);
		printBFS(root);
		System.out.println("is BST = " + isBST(root, Integer.MIN_VALUE, Integer.MAX_VALUE));
		root.l.r= new Node(6);

		printBFS(root);
		System.out.println("is BST after changed = " + isBST(root, Integer.MIN_VALUE, Integer.MAX_VALUE));
	 
	}
 
	
	// BFS, compare root, left and right
	// O(n) go thru nodes
	/*
	 *       3
	 *     2   5
	 *   1  4
	 * FAIL
	 * 
	 */
	public Boolean isBST(Node n, int min, int max) {
		
		if(n == null) return true;
		//check range.
		if(n.x <= min || n.x>= max ) return false;
		// left node min, n.x, 
		// right node n.x, max
		return isBST(n.l, min, n.x) && isBST(n.r, n.x, max);
	}
	public void LCAtest() {
		
		int a[] = {3,2,5,1,4,6};
		Node root = null;
 		root = buildTree(root, a);
		printBFS(root);
		
		System.out.println("LCA = " + LCA(root, root.r.r, root.r.l).x);
	}
	/*
	 * LCA: Find least common ancestor
	 *
	 * Base case: Node is null return null
	 * if root = n1 or root == n2 return root;
	 * Left =  DFS(root.l, n1, n2);
	 * Right = DFS(root.r, n1, n2);
	 * 
	 * if Left and Right not null return root;
	 * else if Left and Right is null return null
	 * 
	 * ele if Left != null and Right is null return Left
	 * else return Right
	 * 
	 * DFS : O(n);
	 */
	public Node LCA(Node root, Node n1, Node n2) {
		
		//base case:
		if(root == null) return null;
		
		if(root == n1 || root == n2) return root;
		
		Node l = LCA(root.l, n1, n2);
		Node r = LCA(root.r, n1, n2);
		
		if(l != null && r!=null) return root;
		
		else if(l == null && r == null) return null;
		
		else if(l != null) return l;
		
		else return r;
		
	}
	
	
	/*
	 * Given an array where elements are sorted in ascending order,
	 * convert it to a height balanced BST.
	 * Base case start > end return null
	 * root = ary[mid], mid=start+(end-start)/2   <-- ticks
	 * left = rec_fun(a,start, mid-1)
	 * right = rec_fun(a,mid+1,end);
	 * return root
	 */
	public void array2BSTtest() {
//		int a[] = {1,2,3};
		int a[] = {1,4,6,7,8, 9, 10,11,12,14,15}; 
		Node root=null;
		root = array2BST(a, 0, a.length-1);
//		printBFS(root);
		printDFS(root);
	}
	
	public Node array2BST(int a[], int start, int end) {
 
		 if(start > end) return null;
		 int mid = start+(end-start)/2;
		 Node n = new Node(a[mid]);
		 n.l = array2BST(a, start, mid-1);
		 n.r = array2BST(a, mid+1, end);
		 
		 return n;
	}
	
	
	/*
	 * Given a Binary Tree, print left view of it. 
	 * Left view of a Binary Tree is set of nodes visible when tree is visited from left side. 
	 * Left view of following tree is 12, 10, 25.

          12
       /     \
     10       30
            /    \
          25      40 
          
      BFS and print left node
	 */
	public void printLeftSide(Node root) {
		
		Queue<Node> st = new LinkedList<Node>();
		st.add(root);
		while(!st.isEmpty()) {
			Node n = st.remove();
			if(null!=n.l) {
				st.add(n.l);
				System.out.print(" " + n.l.x);
			}
			if(null!=n.r) {
				st.add(n.r);
			}
		}
	}
	
	/*
	 * Find two elements in balanced BST which sums to a given a value. 
	 * Constraints Time O(n) and space O(n).
	 * 
	 * answer: inorder into array then 
	 * i=0, j=len-1;
	 * loop(i<j) 
	 * a[i]+[j] = amt
	 * if a[i]+a[j] < amt, i++ else j--
	 */
	public void twoEleSumTest(Node root) {
		ArrayList<Integer> al = new ArrayList<Integer>();
		twoEleSum(root, al);
		Integer a[] = al.toArray(new Integer[al.size()]);
		int amt = 13;
		int i=0;
		int j=a.length-1;
		System.out.println(Arrays.toString(a));
		while(i<j) {
			if(a[i]+a[j] == amt ){
				System.out.println(" 2 elm = " + a[i] + " + " + a[j] + " = " + amt);
				break;
			}
			else if(a[i]+a[j] < amt) i++;
			else j--;
		}
	}
	
	public void twoEleSum(Node root, ArrayList<Integer> al) {
		
		if(null==root) return;
		twoEleSum(root.l, al);
		al.add(root.x);
		twoEleSum(root.r, al);
	}
	
	/*
	 * Given two values k1 and k2 (where k1 < k2) and a root pointer to a Binary Search Tree. 
	 * Find all the keys of tree in range k1 to k2. 
	 * i.e. print all x such that k1<=x<=k2 and x is a key of given BST. 
	 * Return all the keys in ascending order.
	 */
	
	public void getNodeRangTest(Node root) {
		int r1 = 4;
		int r2 = 8;
		List<Integer> list = new ArrayList<Integer>();
		getNodeRang(root, r1,r2, list );
		System.out.println(list.toString());
	}
	public void getNodeRang(Node root, int r1, int r2, List<Integer> list) {
		
		if(null==root) return;
		
		getNodeRang(root.l, r1, r2, list);
		
		if(r1 <= root.x  && root.x <=r2) {
			list.add(root.x);
		}
		getNodeRang(root.r, r1, r2, list);
 
	}

	/*
	 * Given a binary search tree and an integer K, find K-th smallest element in BST.
	 * Trick use array of 1 bc of reference
	 */
	
	public void kthElement(Node root) {
		int k[] = new int[1];
		k[0]=2;
		Node n = kSmallest(root, k);
//		n = kSmallestLoop(root, k);
		
		if(null!=n)
			System.out.println("k smallest Element = " + n.x);
		else 
			System.out.println("is NULL");
 
	}
	
	public Node kSmallest(Node root, int k[] ) {
 		if(null==root || k[0] <= 0) return null;
 		
 		Node l = kSmallest(root.l, k );
 		if(null!=l) return l; // return root bc of this
// 		System.out.println("k = " + k[0]);
 		--k[0];
 		// return kth smallest node
 		if(k[0]==0) return root;
 	 
		return kSmallest(root.r, k );
	}
	/*
	 * Inorder traversal kth is the index 
	 * left mid right
	 */
	public Node kSmallestLoop(Node root, int k) {
		
		Node n = root;
		Stack<Node> st = new Stack<Node>();
		while(!st.isEmpty() || n != null) {
			// push all left node
			if(null!=n) {
				st.push(n);
				n = n.l; // key, keep popping till a right node
			}
			//pop node, goto right 
			else {
				// need to use tmp
				Node tmp = st.pop();
				if(--k==0) return tmp;
				System.out.println(tmp.x);
				if(null!=tmp.r)
					n = tmp.r;
			}
		}
		
		return null;
		
	}
	
	
	/*
	 *               7 
	 * level--   4     8  level++
	 *         1   6     10
	 *  	            9 
	 *  
	 *  Use TreeMap to keep ascending order of level, ArrayList to store all values (nodes)
	 *  at the level. Do recursive in-order and store values with respect to level.
	 *  Left level--, right  level++
	 *  https://www.youtube.com/watch?v=PQKkr036wRc
	 *  Time O(n)
	 */
	public void printVerticalBST(Node root) {

		TreeMap<Integer, ArrayList<Integer>> tm = new TreeMap<Integer, ArrayList<Integer>>();
		verticalBST(root, 0,tm);
		//right to left
		// tm.descendingMap();
		Set<Integer> s = tm.keySet();
		
		for(int i : s) {
			System.out.println(tm.get(i));
		}
 
	}
	
	public void verticalBST(Node root, int level, TreeMap<Integer, ArrayList<Integer>> tm) {
		
		if(null==root) return;
		ArrayList<Integer>al = new ArrayList<Integer>();
		
		
 		verticalBST(root.l, level-1, tm);
	
		// if tmap contain level, get and add to arraylist
		// else create new arraylist and level, add to tmap
		if(tm.containsKey(level)) {
			al = tm.get(level);		
			al.add(root.x);
			tm.put(level, al);
		} else {
			al.add(root.x);
			tm.put(level, al);
		}
		
		verticalBST(root.r, level+1, tm);
	}
	
	/*
	 * Trick: Need to store the level with respect the Queue/Stack.
	 */
	public void verticalBSTLoop(Node root) {
	
		if (root == null) return;
		
		Node n = root;
		ArrayList<Integer> al = new ArrayList<>();
		TreeMap<Integer, ArrayList<Integer>> tm = new TreeMap<>();
		
		Queue<Node> queue = new LinkedList<>();
		Queue<Integer> levelQ = new LinkedList<>();
 
		levelQ.add(0);
		queue.add(n);

		while (!queue.isEmpty()) {

			Node tmpNode = queue.poll();
			int level = levelQ.poll();

//			if (tm.get(level) == null) {
//				tm.put(level, new ArrayList<Integer>());
//			}
//
//			tm.get(level).add(tempNode.x);
			
			if(tm.containsKey(level)) {
				tm.get(level).add(tmpNode.x);
			}
			else {
				al = new ArrayList<Integer>();
				al.add(tmpNode.x);
				tm.put(level, al);
			}

			if (tmpNode.l != null) {
				levelQ.add(level - 1);
				queue.add(tmpNode.l);
			}

			if (tmpNode.r != null) {
				levelQ.add(level + 1);
				queue.add(tmpNode.r);
			}
		}
		
 
		Set<Integer> set = tm.keySet();
	 	for(int i : set) {
			System.out.println(tm.get(i));
		}
		
		for(Entry<Integer, ArrayList<Integer>>  e: tm.entrySet()) {
			System.out.println(e);
		}
		
		tm.forEach((k,v) -> System.out.println(k + " " + v));
		
	}
	
	
	/*
	 * Find the contiguous subarray within an array (containing at least one number) 
	 * which has the largest product.
	 * 
	 * loop array, find max(pos, neg, cur idx), find min(pos, neg, cur idx);
	 * result is max product
	 * Time O(n);
	 */
	public  void maxProducSubAry() {
		
		int a[] = {2,2,0,0,0,2};
		int pos_product = a[0];
		int neg_product = a[0];
		int max = a[0];
		int p=0;
		int n=0;
		for(int i=1; i<a.length;i++) {
			p = pos_product*a[i];
			n = neg_product*a[i];
			
			pos_product = Math.max(Math.max(p, n), a[i]);
			neg_product = Math.min(Math.min(p, n), a[i]);
			
			max = Math.max(pos_product, max);
		}
		
		System.out.println("max product of sub array " + Arrays.toString(a) + " = " + max);
 
	}

	public Node buildTree (Node root, int []t) {
		for(int a : t) {
			root=insert(root, a);
		}
		
		return root;
	}
	public Node insert(Node n, int a) {
		
		if(n == null) {
			return new Node(a);
		}
		if(a <= n.x) {
			n.l = insert(n.l, a);
		}
		else 
			n.r = insert(n.r, a);
		
		return n;
	}
	
	public void topStreamNumTest() {
	
		Node n = null;
		int total[] ={11};
		int max_val[] = {Integer.MIN_VALUE};
		for(int i=3;i < 23; i++) {
			n = topStreamNum(n, i, total, max_val );
			System.out.println("topStreamNumTest");
		}
		n = topStreamNum(n, 1, total, max_val );
		printDFS(n);
	}
	
	public Node topStreamNum(Node n, int x, int total[], int max_val[]) {

		if(total[0] > 0) {
			n = insert(n, x);
			total[0]--;
			max_val[0] = Math.max(max_val[0], x);
		} 
		else if(max_val[0] > x) {
			max_val[0] = Math.max(max_val[0], x);
			//get largest and remove if greater than x
			Node p = n;
			Node c = n.r;
			while(c!=null) {
				if(c.r == null) break;
				p = c;
				c = c.r;
				System.out.println("debug");
			}
			// remove bst largest and insert x
			if(c.x > x) {
				p.r = c.r;
				n = insert(n,x);
			}

		}
		return n;
	}
	
	public int totalNode(Node n) {
		if(n==null) return 0;
		return 1+ totalNode(n.l) + totalNode(n.r);
	}
	
	public void printDFS(Node r) {
		if(null==r) return;
		printDFS(r.l);
		System.out.print(" " + r.x);
//		System.out.print(" " + r.c);
		printDFS(r.r);
	}
	
	public void printDFSPreOrder(Node r) {
		if(null==r) return;
		System.out.print(" " + r.x);
		printDFS(r.l);
		printDFS(r.r);
	}
	public void printBFS(Node r) {
		
		if(r==null) return;
		Queue<Node> q = new LinkedList<Node>(); 
		q.add(r);
		Node tmp;
		int curr_level = 1;
		int next_level  = 0;
		while(!q.isEmpty()) {
			tmp = q.poll();
			System.out.print(" " + tmp.x);
//			System.out.print(" " + tmp.c);
			if(tmp.l!=null) {
				q.add(tmp.l);
				next_level++;
			}
			if(tmp.r!=null) {
				q.add(tmp.r);
				next_level++;
			}
			curr_level--;
			if(curr_level ==0) {
				System.out.println("");
				curr_level = next_level;
				next_level = 0;
			}
			
		}
		
	}
	
	public void printBFSLoop(Node r) {
		if(r == null) return;
		Queue<Node> q = new LinkedList<>();
		q.add(r);
		while(!q.isEmpty()) {
			int size = q.size();
			while(size > 0) {
				Node n = q.remove();
				System.out.print(n.x + " ");
				if(n.l != null) q.add(n.l);
				if(n.r != null) q.add(n.r);
				size--;
			}
			System.out.println("");
		}
	}
	
	public void printDFSST(Node r) {
	
		if(r==null) return;
		Stack<Node> st = new Stack<Node>(); 
		st.add(r);
		Node tmp;
 
		while(!st.isEmpty()) {
			tmp = st.pop();
			System.out.print(" " + tmp.x);
 
			if(tmp.r!=null) {
				st.add(tmp.r);
	 
			}
			if(tmp.l!=null) {
				st.add(tmp.l);
				 
			}
		}
		
	}
	
	public void printDFSLoop(Node r) {
		if(r == null) return;
		Stack<Node> st = new Stack<>();
		Node n = r;
		while( n!= null || !st.isEmpty()) {
			while(n != null) {
				st.push(n);
				n = n.l;
			}
			//end of all left node, pop node and go right
			Node tmp = st.pop();
			System.out.print(tmp.x + " ");
			n = tmp.r;
		}
	}
	
	/*
	 * 	Ternary Expression
		no (), evaluate from right
		
		a?b:c
		if (a) {
		  return b;
		} else {
		  return c;
		}
		  a
		 / \
		b   c
		
		a?b?c:d:e
		    a
		   / \
		  b   e
		 / \
		c   d
		
		a?b:c?d:e
		   a
		  / \
		 b   c
		    / \
		   d   e
		
		input: String expr
		output: Expression      binary tree
	 *
	 */
	// loop and look for "?" to push stack node, ":" pop stack
	// case : "?"
	// if stack empty push, 
	// else push left node if null else push right node.
	// case : ":"
	// n=stack.peek instead of pop because need to be in stack to print.
	// if n.right is null push left and right node
	// else loop thru stack and find right is null then push right
	// push node back to stack to print/trace
	public void testTenary2BSTRe() {
//		String s="a?b:c";
//		String s = "a?b?c:d:e";
//		String s = "a?b:c?d:e";
		
		String s_array[] = {"a?b:c", "a?b?c:d:e", "a?b:c?d:e"};
		Node n = null;
		Node root =null;
		Stack<Node> st = new Stack<Node>();
		for(String s : s_array) {
			for(int i=1; i<s.length();i+=2)  {
				
				//push stack
				if(s.charAt(i)=='?') {
					if(st.isEmpty()) {
						n = new Node(s.charAt(i-1));
					}
					//n get from pop in elseif condition
					else {
						Node tmp = new Node(s.charAt(i-1));
						if(n.l==null) {
							n.l = tmp;
							n = n.l;
						}
						else {
							n.r = tmp;
							n=n.r;
						}
					}
					st.push(n);
				}
				//pop stack
				else if(s.charAt(i)==':') {
					// peek back need to be in stack to print
					n = st.peek();
					
					if(n.r == null) {
						n.l = new Node(s.charAt(i-1));
						n.r = new Node(s.charAt(i+1));
					}
					else {
						//pop until right node is null
						// case:a?b?c:d:e
						while(!st.isEmpty()) {
							n = st.pop();
							if(n.r==null) {
								n.r = new Node(s.charAt(i+1));
							}
						}
						//end of stack, push node back
						st.push(n);
					}
				}
			}
			while(!st.isEmpty()) root = st.pop();
			printBFS(root);
			st.clear();
			System.out.println("");
		}
	}
	
    public Node fn(char[] ar, int i) {
        if(i>=ar.length)return null;
        else{
            Node root = new Node(ar[i++]);
            if(i<=ar.length-1 && ar[i]=='?'){
                root.l = fn(ar, ++i);
            }
            else{
                root.r = fn(ar, ++i);
            }
            return root;
        }
    } 
 	 
	public void doTenaryRec() {
		String s_array[] = {"a?b:c", "a?b?c:d:e", "a?b:c?d:e"};
		Node n = new Node(s_array[0].charAt(0));
//		Node root = tenary2BSTRe(n, s_array[0], 1);
		for(String s : s_array) {
			int x[]= {0};
			Node root = convertExpression(s.toCharArray(), x);
			System.out.println(s);
			printBFS(root);
		}
	}
		 
	public Node convertExpression(char []str,int []i) {
		
		// create left or right node
	    Node temp = new Node( str[i[0]] ); 

	    // check if last node
	    // base case
	    if(i[0]==str.length-1)
	        return temp;

	    //look ahead to check ?
 	    if(str[i[0]+1]=='?'){
	    	i[0] += 2;
	        temp.l = convertExpression(str,i);
	        i[0] += 2;
	        temp.r = convertExpression(str,i);
	    }
 	    
	    return temp;
	}
	
	public void findDulicationSubTreesTest() {
		/*
		 *  1
	       / \
	      2   3
	     /   / \
	    4   2   4
	       /
	      4
		 */
		// return [ [2,4], [4] ]
		int a[] = {1};
		
		Node root = null;
		root = buildTree(root, a);
		root.l = new Node(2);
		root.l.l = new Node(4);
		root.r = new Node(3);
		root.r.l = new Node(2);
		root.r.l.l = new Node(4);
		root.r.r = new Node(4);
		
//		printBFS(root);
		List<Node> list = findDuplicateSubtrees(root);
		
		list.forEach(node ->  {
			
			printBFS(node);
			System.out.println();
		});
		System.out.println(list.size());
		
		
	}
	
	// 652. Find Duplicate Subtrees
	//	Given a binary tree, return all duplicate subtrees. 
	//	For each kind of duplicate subtrees, you only need to return the root node of any one of them.
	// Answer: Inorder DFS, create a string of values and store in hashmap
	// if containskey then store/print duplcation.
	//
	public List<Node> findDuplicateSubtrees(Node root) {
        
		Map<String, Integer> hm = new HashMap<>();
		List<Node> list = new ArrayList<>();
        inorder(root, hm, list);
//        postorder(root, new HashMap<String, Integer>(), list);
		return list;
    }
	public String inorder(Node root, Map<String, Integer> m, List<Node> list) {
		
		if (root == null)
            return "";
      
//        String str = "(";
//        str += inorder(root.l, m, list);
//        str += Integer.toString(root.x);
//        str += inorder(root.r, m, list);
//        str += ")";
      
        String str = "";
        str += inorder(root.l, m, list);
        str += Integer.toString(root.x);
        str += inorder(root.r, m, list);
        
        // Subtree already present (Note that we use
        // HashMap instead of HashSet
        // because we want to print multiple duplicates
        // only once, consider example of 4 in above
        // subtree, it should be printed only once.     
        
        if (m.get(str) != null && m.get(str)==1 ) {
//            System.out.print( root.x + " ");
            list.add(root);
        }
     
        if (m.containsKey(str)) 
            m.put(str, m.get(str) + 1);
        else
            m.put(str, 1);
        
//        if (m.getOrDefault(str, 0) == 1) list.add(root);
//        m.put(str, m.getOrDefault(str, 0) + 1);
        
        return str;
	}
	
	public String postorder(Node cur, Map<String, Integer> map, List<Node> res) {
	    if (cur == null) return "#";  
	    String serial = cur.x + "," + postorder(cur.l, map, res) + "," + postorder(cur.r, map, res);
	    if (map.getOrDefault(serial, 0) == 1) res.add(cur);
	    map.put(serial, map.getOrDefault(serial, 0) + 1);
	    return serial;
	}
	
	public void StringToHash() {
		
		String s ="abcd";
	
		System.out.println("s = " + s + " hash = " + s.hashCode());
	}
	
	@Test
	public void test() {
		
//		BST  bst = new BST();
//		int a[] = {7,4,8, 1, 6, 10, 9};
//		int a[] = {5,2,13};
		
		int a[] = {11,2,1,7,29,15,40,35,41, 42};
		
		Node root = null;
		root = buildTree(root, a);
//		printBFS(root);
//		printBFSLoop(root);
		printDFSLoop(root);
		printDFS(root);
//		verticalBSTLoop(root);
//		printVerticalBST(root);
//		maxProducSubAry();

//		kthElement(root);
		
//		getNodeRangTest(root);
//		twoEleSumTest(root);
//		printLeftSide(root);
//		array2BSTtest();
//		isBSTTest();
//		LCAtest();
//		isBSTBalanceTest(root);
//		printDFSST(root);
//		BST2LLTest();
//		BSTRangeTest(root);
//		BSTSumGreaterTreeTest();
//		topStreamNumTest();
//		ternary2BST();
//		testTenary2BSTRe();
//		doTenaryRec();
//		findDulicationSubTreesTest();
//		StringToHash();
	}
}

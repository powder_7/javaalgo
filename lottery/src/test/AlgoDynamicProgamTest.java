package test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class AlgoDynamicProgamTest {

//	static String doesCircleExist(String commands) {
//
//		int initialX = 0;
//		int initialY = 0;
//		
//		int x = 0;
//		int y = 0;
//		String direction = "north";
//		
//		for (int i = 0; i < commands.length(); i++) {
//			
//			if (direction.equals("north")) {
//				if (commands.charAt(i) == 'G') {
//					y++;
//				} else if (commands.charAt(i) == 'L') {
//					direction = "west";
//				} else if (commands.charAt(i) == 'R') {
//					direction = "east";
//				} else {
//					System.out.println("Wrong command");
//				}
//			} else if (direction.equals("east")) {
//				if (commands.charAt(i) == 'G') {
//					x++;
//				} else if (commands.charAt(i) == 'L') {
//					direction = "north";
//				} else if (commands.charAt(i) == 'R') {
//					direction = "south";
//				} else {
//					System.out.println("Wrong command");
//				}
//			} else if (direction.equals("south")) {
//				if (commands.charAt(i) == 'G') {
//					y--;
//				} else if (commands.charAt(i) == 'L') {
//					direction = "east";
//				} else if (commands.charAt(i) == 'R') {
//					direction = "west";
//				} else {
//					System.out.println("Wrong command");
//				}
//			} else if (direction.equals("west")) {
//				if (commands.charAt(i) == 'G') {
//					x--;
//				} else if (commands.charAt(i) == 'L') {
//					direction = "south";
//				} else if (commands.charAt(i) == 'R') {
//					direction = "north";
//				} else {
//					System.out.println("Wrong command");
//				}
//			}
//		}
//		
//		if (direction.equals("north") && (((x-initialX)*(x-initialX) + (y-initialY)*(y-initialY)) > 0)) {
//			return "NO";
//		} else {
//			return "YES";
//		}
//	}
//	
	//hackerrank
	// trick num = 1234
	// tmp=num/10, tmp =123.4
	// take the difference and time 10 and get 4. 
	static int solvePuzzle(int num) {
        int holes = 0;
        do{
        	float tmp = (float) num / 10;    
	        num /= 10;
	        int var = (int) ((tmp - num) * 10);
	
	        if (var == 0 || var == 4 || var == 6 || var == 9)    
	        	holes++;             
	        if (var == 8)  
	        	holes += 2;

        }while (num != 0);       
        
        return holes; 
    }
	
	static String doesCircleExist(String commands) {

		int initialX = 0;
		int initialY = 0;
		
		int x = 0;
		int y = 0;
		String direction = "north";
		
		for (int i = 0; i < commands.length(); i++) {
			
			if (direction.equals("north")) {
				if (commands.charAt(i) == 'G') {
					y++;
				} 
				else if (commands.charAt(i) == 'L') {
					direction = "west";
				} 
				else if (commands.charAt(i) == 'R') {
					direction = "east";
				} 
			} 
			else if (direction.equals("east")) {
				if (commands.charAt(i) == 'G') {
					x++;
				} 
				else if (commands.charAt(i) == 'L') {
					direction = "north";
				} 
				else if (commands.charAt(i) == 'R') {
					direction = "south";
				} 
			} 
			else if (direction.equals("south")) {
				if (commands.charAt(i) == 'G') {
					y--;
				} 
				else if (commands.charAt(i) == 'L') {
					direction = "east";
				} 
				else if (commands.charAt(i) == 'R') {
					direction = "west";
				} 
				
			} 
			else if (direction.equals("west")) {
				if (commands.charAt(i) == 'G') {
					x--;
				} 
				else if (commands.charAt(i) == 'L') {
					direction = "south";
				} 
				else if (commands.charAt(i) == 'R') {
					direction = "north";
				} 
			}
		}
		
		System.out.println("x = " + x + " y = " + y);
		//r^2 = (x1-x2)^2 + (y1-y2)^2, circle formula
		if (((x-initialX)*(x-initialX) + (y-initialY)*(y-initialY)) > 0) {
			return "NO";
		} else {
			return "YES";
		}
	}
	//	Given a set of non-negative integers, and a value sum, 
	//	determine if there is a subset of the given set with sum equal to given sum.
	//	Examples: set[] = {3, 34, 4, 12, 5, 2}, sum = 9
	//	Output:  True  //There is a subset (4, 5) with sum 9.
	/*  sum is horizontal, set is vertical
	    
	    sum
	 *  0		1		2		3		4		5		6		7		8		9
set	 0  true	false	false	false	false	false	false	false	false	false	
	 3	true	false	false	true	false	false	false	false	false	false	
	 34	true	false	false	true	false	false	false	false	false	false	
	 4	true	false	false	true	true	false	false	true	false	false	
	 12	true	false	false	true	true	false	false	true	false	false	
	 5	true	false	false	true	true	true	false	true	true	true	
	 2	true	false	true	true	true	true	true	true	true	true
	 */
	public void isSubsetSumTest() {
		int set [] = {3, 34, 4, 12, 5, 2};
		int sum =9;
		
		System.out.println("isSubsetSume = " + isSubsetSum(set, sum));
	}
	
	boolean isSubsetSum(int set[], int sum) {
		
		boolean T[][] = new boolean[set.length+1][sum+1];
		
		for(int i=0; i < T.length; i++) {
			T[i][0] = true;
		}
 		
		// y or i  is set[], vertical,
		// x or j is sum, horizontal
		for(int i=1; i <=  set.length; i++) {
			for(int j=1; j <= sum; j++) {
				
				if(set[i-1] <= j) {
					// use above cell OR above cell offset with j-set[i-1], go up one and back left set[i-1]
					T[i][j] = T[i-1][j] || T[i-1][j-set[i-1]];
				}
				else {
					//set i > sum i
					// use above cell if sum is less than  set[i-1]
					T[i][j] = T[i-1][j];
				}
			}
		}
		
		for(int i=0; i< T.length; i++) {
			for(int j=0; j<T[0].length; j++) {
				System.out.print(T[i][j] + "\t");
			}
			System.out.println("");
		}
		
		return T[set.length][sum];
	}
	public void lcsTest() {
		String a = "152637";
		String b = "56712";
		
		System.out.println(lcs(a,b));
	}
	// Given two strings, write a function that returns the longest common substring.
	// https://www.youtube.com/watch?v=NnD96abizww
	// https://rosettacode.org/wiki/Longest_common_subsequence#Recursion_2
	// https://www.geeksforgeeks.org/printing-longest-common-subsequence/
	// if same, previous diagonal value +1
	// else max(left, top) of maxtrix.
	public String lcs(String a, String b) {
	    int[][] lengths = new int[a.length()+1][b.length()+1];
	    int max = 0;
	    // row 0 and column 0 are initialized to 0 already
	    for (int i = 1; i <= a.length(); i++) {
	        for (int j = 1; j <=b.length(); j++) {
 
                if (a.charAt(i-1) == b.charAt(j-1))
	                lengths[i][j] = lengths[i-1][j-1] + 1;
	            else
	                lengths[i][j] = Math.max(lengths[i-1][j], lengths[i][j-1]);
 	             
	        }
	    }
 	    // read the substring out from the matrix
	    StringBuffer sb = new StringBuffer();
	    for (int x = a.length(), y = b.length(); x != 0 && y != 0; ) {
	        if (lengths[x][y] == lengths[x-1][y])
	            x--;
	        else if (lengths[x][y] == lengths[x][y-1])
	            y--;
	        else {
	            assert a.charAt(x-1) == b.charAt(y-1);
	            sb.append(a.charAt(x-1));
	            x--;
	            y--;
	        }
	    }
	 
	    return sb.reverse().toString() + " = " + lengths[a.length()][b.length()];
	}
	// https://www.youtube.com/watch?v=8LusJS5-AGo
	public void knapSackTest() {

		int val[] = new int[] { 1, 4, 5, 7 };
		int wt[] =  new int[] { 1, 3, 4, 5 };
		int W = 7;
		int n = val.length;
		System.out.println(knapSack(W, wt, val, n));
	}  
	/*        Weight = 7
		      0  1  2   3   4   5   6   7
	val wt	0 0	 0	0	0	0	0	0	0	
	1		1 0  1	1	1	1	1	1	1	
	4	   .3 0	 1	1	4	5	5	5	5	
	5	   .4 0	 1	1	4	5	6	6	9	
	7		5 0	 1	1	4	5	7	8	9
		 
	*/
	// Returns the maximum value that can be put in a knapsack of capacity W
	public int knapSack(int W, int wt[], int val[], int n) {
		int i, j, w;
		int K[][] = new int[n + 1][W + 1];
		List<Integer> list = new ArrayList<>();
		// Build table K[][] in bottom up manner
		for (i = 0; i <= n; i++) {
			for (w = 0; w <= W; w++) {
				if (i == 0 || w == 0)
					K[i][w] = 0;
				else if (wt[i - 1] <= w)
					// max of go up and back w weight + weight , go up
					K[i][w] = Math.max(val[i - 1] + K[i - 1][w - wt[i - 1]],    K[i - 1][w]);
				else
					K[i][w] = K[i - 1][w];
			}
		}
		
		for(i = 0; i < K.length; i++) {
			for(w = 0;  w < K[0].length; w++ ) {
				System.out.print(K[i][w] + "\t");
			}
			System.out.println("");
		}
		
		for(i = K.length-1, j = K[0].length-1;  i!=0 && j!=0 ;) {
			// not included
			if(K[i][j] == K[i-1][j]) {
				i--;
			}
			// inlcuded soo add to list
			else {
				list.add(val[i-1]);
				// go up on and back weight i
				i--;
				j= j-wt[i];
			}
		}
		System.out.println(list);
		return K[n][W];
	}
	
	// Count all possible paths from top left to bottom right of a mXn matrix
	// The problem is to count all the possible paths from top left to bottom right of a mXn matrix 
	// with the constraints that from each cell you can either move only to right or down
	// Returns count of possible paths to reach 
    // cell at row number m and column number n from
    //  the topmost leftmost cell (cell at 1, 1)
	public int numberOfPaths(int m, int n) {
		// Create a 2D table to store results
		// of subproblems
		int count[][] = new int[m][n];

		// Count of paths to reach any cell in
		// first column is 1
		for (int i = 0; i < m; i++)
			count[i][0] = 1;

		// Count of paths to reach any cell in
		// first column is 1
		for (int j = 0; j < n; j++)
			count[0][j] = 1;

		// Calculate count of paths for other
		// cells in bottom-up manner using
		// the recursive solution
		for (int i = 1; i < m; i++) {
			for (int j = 1; j < n; j++)

				// By uncommenting the last part the
				// code calculatest he total possible paths
				// if the diagonal Movements are allowed
				count[i][j] = count[i - 1][j] + count[i][j - 1]; // + count[i-1][j-1];

		}
		return count[m - 1][n - 1];
	}
	
	public void doCoinChange() {
		int a[]={1,2, 3};
		int total = 5;
		
//		int a[]={2};
//		int total = 3;
		
		
		System.out.println(" min coin change = " + minCoinChange(a, total));
//		System.out.println(" coin change num of ways = " + coinChangeNumWay(a, total));
	}
	// RunTime = O(NumOfCoins*Sum)
	// space   = O(Sum);
	// T[i] = min(1+T[i-coins[j], T[i]);
	public int minCoinChange(int []coins, int sum) {
		int T[] = new int[sum+1];
		T[0]=0;
		for(int i=1; i< T.length; i++) {
			T[i] = Integer.MAX_VALUE-1;
		}
		// Compute minimum coins required for all
        // values from 1 to sum
		for(int i=1; i<=sum ; i++) {
			 // Go through all coins smaller than i
			for(int coin : coins) {
				//coin <= total
				if(coin <=i) {
					// min go back i-coin index + 1, current value at i
					T[i] = Math.min((1+T[i-coin]), T[i]);
				}
			}
		}

		return (T[sum]==Integer.MAX_VALUE-1)?-1: T[sum];
	}
	
	/*
	 * Given a value N, if we want to make change for N cents, 
	 * and we have infinite supply of each of S = { S1, S2, .. , Sm} valued coins, 
	 * how many ways can we make the change? T
	 * https://www.geeksforgeeks.org/coin-change-dp-7/
	 * T = O(nm);
	 * S = O(n);
	 */
	public static long coinChangeNumWay(int[] coins, int money) {
        long[] DP = new long[money + 1]; // O(N) space.
        DP[0] = (long) 1; 	// n == 0 case.
        // loop coins
        for(int coin : coins) {
            for(int j = coin; j < DP.length; j++) {
            	// The only tricky step.
            	// current value = current value + DP[current j minus go back coin space];
                DP[j] = DP[j] +  DP[j - coin];
            }
        }       
        return DP[money];
	}
	 
	
	@Test
	public void test() {
		
//		isSubsetSumTest();
//		System.out.println(doesCircleExist("LGRGRGLGRGRGLGRGRGLGRG"));
//		System.out.println(solvePuzzle(1288));
//		lcsTest();
//		knapSackTest();
		doCoinChange();
		int k[][] = new int[1][1];
	}
}

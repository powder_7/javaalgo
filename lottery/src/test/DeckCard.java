package test;

import java.util.Random;

import org.junit.Test;

//Make a data structure to represent a deck a cards and support operations including shuffle. 
public class DeckCard {
	
	
	@Test
	public void test() { 
		createDeck();
		print();
		shuffle();
		print();
	}
	
	public String [] mSuits =  {"Spade", "Heart", "Diamond", "Club"};
	public String [] mValues = {"2","3", "4", "5", "6", "7","8", "9", "10", "Jack", "Queen", "King", "Ace"};
	public int mSize = 52;
	public Card [] mDeck = new Card[mSize];
	
	public void print() {
		for(Card i : mDeck) {
			System.out.println(i.toString());
		}
	}
	
	
	public void createDeck() {
		int count = 0;
		for(int i=0; i < mSuits.length; i++) {
			for(int j=0; j < mValues.length; j++) {
				mDeck[count++] = new Card(mSuits[i], mValues[j]);
			}
		}
	}
	
	public void shuffle() {
		
		 Random rand= new Random();
		 int numOfShuffle = 2;
		 
		 for(int i=0; i<mSize; i++) {
			 // create 2 loction to swap
			 int n = rand.nextInt(mSize);
			 int m = rand.nextInt(mSize);
			 
			 Card tmp = mDeck[n];
			 mDeck[n] = mDeck[m];
			 mDeck[m] = tmp;
		 }
	}
	
	class Card {
		String mSuit;
		String mValue;
		Card(String suit, String value) {
			this.mSuit = suit;
			this.mValue = value;
		}
		@Override
		public String toString() {
			return mSuit + " " + mValue;
		}
	}

}

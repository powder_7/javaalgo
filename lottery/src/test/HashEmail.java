package test;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.codec.digest.DigestUtils;

public class HashEmail {

	private List<String> mPermList = new ArrayList<String>();
	private final String mStringChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_.@+";
	private String myEmailHash = null; 

	private final String mGivenHashStr = 
			  "80ebc494a86b5321942392d286a7e677"
			+ "8f2dc44c26e9ee51b9b88165174c923a"
			+ "42f53429d48a0ea4239ecf4d70d7957f"
			+ "c4535ff7aba0ba76fcea7247b1eee408"
			+ "04510c4856cc333dc50f9f09f1f79c75"
			+ "cff259f54dfae450898c9adb7ad48ce1"
			+ "92c4a96fd498f2be44e08085d99a47c8"
			+ "54d0f8cb7dd6636effab7c6a718e22b3"
			+ "6ec7d532d5d293127395f06fb6aba3c9"
			+ "d0898b7916305610027ad0aa24bdb6aa"
			+ "a9365534368093ed95b2a6aef9a8c4f9"
			+ "8fa715f7a17b294f6dbbde552f068f32"
			+ "4d7a4e53d38a19677e8d656f672b808f"
			+ "b972201fffb6b0158afb23ce773308bc"
			+ "f50f3b6605bc52912ae8fcc764cd9029"
			+ "a73e920acf31dff4478bdbb060162063"
			+ "ebdcf24791aab4cc2237e88cc49be9f0"
			+ "95e3bf990b2242388931b3ee4fcc895a"
			+ "5e2ede07c954e73a2c7e2da1b2e52344"
			+ "37bb6db7dedde95f177296102530a219"
			+ "ab4699bda1fd17db7502ffdcd08bc76d"
			+ "eb4718b34d3d25fa64485f5e83ca544d"
			+ "572b37be3e3836aeff7d3e5c9581b965"
			+ "ea93d4b66d5d455605d8f4c15ddc2d64"
			+ "9c7990fdfac36cad81f0dcbfc97e6e29"
			+ "a58abbbe3737ad71cf85e0d1ebb2991e"
			+ "59f81d6f9bd1c930cfbaee99bf2031bd"
			+ "4ba0cf28d587764a3914847186236594";

	private final String lastHash = "4ba0cf28d587764a3914847186236594";
 
	public void Run() {

		myEmailHash = DigestUtils.md5Hex("NathenLam@yahoo.com");
		permutationStr(mStringChar);
		String secretEamil = "";
		String tmpHash = null;

		while (true) {
			for (String s : mPermList) {
				tmpHash = getCollisionHash(secretEamil + s);
				if (mGivenHashStr.contains(tmpHash)) {
					secretEamil += s;
					System.out.println(tmpHash + "   " + secretEamil);
				}
				//return if hash value is equal to last given hash value.
				if (tmpHash.equals(lastHash)) {
					System.out.println("secret email = " + secretEamil);
					return;
				}
			}
		}
	}

	/**
	 * @param s is the string to be md5 hash.
	 * @return md5 hash.
	 */
	private String getCollisionHash(String s) {
		return DigestUtils.md5Hex(myEmailHash + s + DigestUtils.md5Hex(s));
	}

	/**
	 * @param s is a string with all the alphanumeric characters and 4 _.@+ 
	 */
	private void permutationStr(String s) {

		String tmp = null;
		for (int j = 0; j < s.length(); j++) {
			for (int i = 0; i < s.length(); i++) {
				tmp = String.valueOf(s.charAt(j)) + String.valueOf(s.charAt(i));
				mPermList.add(tmp);

			}
		}
	}
	
	public static void main(String[] args) {
		HashEmail hMail = new HashEmail();
		hMail.Run();
	}
}



	/*
 	public void invokeRESTGet() {
		mList.clear();
 		String url =  "https://s3.amazonaws.com/anvato-resumes/candidates/2015_12/Nathen_Lam_565de3f2d965f";

		StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {

						Log.e(TAG, " str len = " + response.length() + " ***** test = " + response);
						write("nate.txt", response);
					}
				},
				new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {

						// Error handling
						Log.e(TAG, "Something went wrong!");
						error.printStackTrace();

					}
				})
		{
				@Override
				protected Response<String> parseNetworkResponse(NetworkResponse response) {
//				    Map<String, String> responseHeaders = response.headers;
//				    Log.e(TAG, "header = " + responseHeaders.get(0));

					String type = response.headers.toString();
					int sc = response.statusCode;
					try {
						String json = new String(
								response.data,
								HttpHeaderParser.parseCharset(response.headers));
						Log.e(TAG, " response data = " + json + " json len = " + json.length());
					}
					catch (UnsupportedEncodingException e) {
						return Response.error(new ParseError(e));
					}

					if(Const.DEBUG) {
						if (type != null) {
							Log.e(TAG, "header = " + type + " statusCod = " + sc );
						}else {
							Log.e(TAG, "header is NULL");
						}
					}
					return super.parseNetworkResponse(response);
				}
				@Override
				public HashMap<String, String> getHeaders() {
					HashMap<String, String> headers  = new HashMap<String, String>();
					headers.put("Referer", "http://www.anvato.com/candidates/Nathen_Lam");
					headers.put("User-Agent", "Mozilla/5.0 (Linux; Android 4.1.1; Galaxy Nexus Build/JRO03C) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19");
					return headers;
				}


		};

		// interface
//		mGetDataListner.onGetDataListener(mList);

		stringRequest.setShouldCache(false);
		requestQueue.getCache().clear();
		requestQueue.add(stringRequest);
	}*/

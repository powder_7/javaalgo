package test;

import org.junit.Test;

public class Interview {


	@Test
	public void test() {
//		printStarTest();
//		lastWordTest();
//		reveseStrSubTest();
//		largestProdPalinTest();
//		findSumPrimeTest();
//		sumEvenFibTest();
//		findNthPrimeTest();
		largestPrimeFactorTest();
	}
	
	/**
	 * https://projecteuler.net/problem=3
	 */
	void largestPrimeFactorTest() {
		int n = 49;
		System.out.println("largest prime factor = " +  largestPrimeFactor(n));
	}
	
	int largestPrimeFactor(int n) {
		
		int i;
		for(i=2; i<=n; i++) {
			if(n%i == 0) {
				n = n/i;	// assign factor
				i--;		// repeat same value to divide
			}
		}
		return i;
	}
	
	/**
	 * https://projecteuler.net/problem=7
	 */

	void findNthPrimeTest(){
		int nth_prime = 7;
		System.out.println("nth prime = " + findNthPrimt(nth_prime) );
	}
	
	int findNthPrimt(int n) {
		
		if( n<= 3) return n;
		int count = 3;
		int prime=3;
		int i;
		boolean isPrime= true;
		while(count < n) {
			isPrime = true;
			prime++;
			// not need to equal to sqrt bc floor of value
			for( i=2; i <= Math.sqrt(prime); i++) {
				if(prime % i == 0) {
					isPrime = false;
					System.out.println("i = " + i + " count = " + count + " isPrime " +isPrime + " prime =  " + prime);
					break;
				}
				
			}
			//update nth prime
			if(true == isPrime) { 
				count++;
				System.out.println("  count = " + count + " isPrime " +isPrime + " prime =  " + prime);
			}
			
		} 
		return prime;
	}
	//https://projecteuler.net/problem=2
	// find even fib num
	void sumEvenFibTest() {
		int n = 6;
//		System.out.println("fib = " + sumEvenFib(n));
		System.out.println("fib = " + evenFibo(n));
	}
	//a=0, b=2, c =a+4b
	long evenFibo(int n)  {
        long sum = 0;
        long a = 0, b = 2, c = 0;
         
       sum =  a + b;
        for(int i=0; i<=n; i++) {
        	System.out.println( sum + "   ");
        	sum += a +4*b;
            a = b;
            b = sum;
           
        }
        return sum;
    }
	long sumEvenFib(int n) {
		
		long f0 = 0, f1=1, fn=0;
		long sum=0;
		if(0==n || 1==n) return n;
		
		for(int i=2; i<=n; i++) {
			fn = f0 + f1;
			f0 = f1;
			f1 = fn;
//			if(fn%2==0) sum +=fn;
			
			//even plus 1 is odd so odd&1 is 1
			if((fn&1)==0) sum+=fn; 
		}
		return sum;
	}
	// https://projecteuler.net/problem=10
	// find sum of all print given n
	// O(n*lg(n)) ~ n*sqrt(n)
	void findSumPrimeTest() {
		int n = 2000000;
		System.out.println(" sum of prime [" + findSumPrime(n) + "] given n = " + n );
	}
	
	long  findSumPrime(int n) {
	
		long sum=0;
		boolean is_prime=true;
		for(int i=2; i<= n; i++) {
			is_prime=true;
			//test for prime
			for(int j=2; j<=Math.sqrt(i); j++) {
				//test n mod j is zero then not prime
				if(i % j == 0) {
					is_prime = false;
					break;
				}
			}
			// add n to sum
			if(is_prime) sum += i;
		}
		
		return sum;
	}
	
	// Euler prob 4
	// largest product palindrome
	void largestProdPalinTest() {
		System.out.println(" largestProdPalinTest " + largestProdPalin(99));
	}
	
	// x is max multiple, ie 2 digits is 99, 3 digits is 999
	private int largestProdPalin(int max_digits) {
		int max_prod=-1;
		int tmp = -1;
		StringBuffer sb = new StringBuffer();
		// 100*100 isn't pali
		for(int i=max_digits; i > 10; i--) {
			// i*j == j*i, so j=1
			// ie 10*10,10*9..., 10*1
			// 9*9,.. 9*1
			// no need to do 9*10
			for(int j=i; j > 10; j--) {
				tmp = i*j;
				if(max_prod < tmp && palindrome(tmp) ) {
					max_prod = tmp;
//					System.out.println(max_prod + " i " + i + " j " + j);
				}
			}
			
		}
		
		return max_prod;
	}
	
	boolean palindrome(Object x) {
		String s = String.valueOf(x);
//		StringBuffer sb = new StringBuffer(s);
		StringBuilder sb = new StringBuilder();
		sb.append(s);
		return s.equals(sb.reverse().toString());
	}
	//nauto
	// given n print * in pyramid, where n the number of rows with odd star
	//   *
	//  ***
	// *****
	void printStar(int n) {

		int i, j, k;
		for (i = 1; i <= n; i++) {
			// print space, n-i, n minus current row
			for (j = 1; j <= n - i; j++) {
				System.out.print(" ");
			}
			// print odd *, odd = 2*i-1 bc index=1
			for (k = 1; k <= 2 * i - 1; k++) {
				System.out.print("*");
			}

			System.out.println("");
		}

	}
	void printStarTest() {
		int n = 1;
		printStar(n);
	}
	// INPUT: "These are some words"
	// OUTPUT:  "sdrow"
	void lastWordTest() {
		String s ="These are some words";
		System.out.println(lastWord(s));
	}
	
	String lastWord(String s) {
	    String sp[] = s.split(" ");
	    StringBuffer bf = new StringBuffer(sp[sp.length-1]);
	    
	    return bf.reverse().toString();
	}
	
	//input string s
	//output reverse s
	void reveseStrSubTest() {
		String s = "abcd";
//		System.out.println(reveseStrSub(s, s.length()-1, s.length() ) );
		
		System.out.println(reveseStrSub(s, 0, 1) );
	}
	
	//trick, return a, then ba,then cba, then dcba, start at end of string
	// input string s, a=len-1, b=len, sub(inclusion, exclusion);
	String reveseStrSub(String s, int a, int b) {
 
//		if(0==a) return s.substring(a,b);
//		return s.substring(a, b)+ reveseStrSub(s, a-1, b-1);
		
		if(s.length()==b) return s.substring(a, b);
		return reveseStrSub(s, a+1, b+1)+ s.substring(a,b);
	}
	
}

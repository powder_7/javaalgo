package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.hamcrest.CoreMatchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;

import com.lottery;
import com.abs.animal;
import com.abs.cat;
import com.abs.dog;
import com.ds.BST;
import com.ds.DLList;
import com.ds.DsExe;
import com.ds.Linklist;
import com.ds.Tree;
import com.exe.Exe;
import com.exercise;
import com.recursion.Recursion;

public class mytest {
	@Rule
	public ErrorCollector collector= new ErrorCollector();

	@Test
	public void test() {
//		lottery mylottery = new lottery();

		int[] a = {1,2,3};
		int[] b = {1,2};
//		assertEquals("array test ", b, lottery.reverse(a) );
     		
//		exercise  my_exer  = new exercise();
		Linklist  my_ll    = new Linklist();
		Recursion my_recur = new Recursion();
		Tree      my_tree  = new Tree();
		Exe       my_exe   = new Exe();
		DLList   my_dll   = new DLList();
		BST       my_bst   = new BST();
		DsExe     my_ds_exe = new DsExe();
		
		try {
			// stop on assert

//		    my_exer.run_it();
//	        my_ll.test();
//	        my_recur.test();
//		    my_tree.test();
//		    my_exe.test();
//	        my_dll.test();
	        my_bst.test();
//	        my_ds_exe.runTest();
 
		} catch(Exception e) {
           e.printStackTrace();
    	}
	}
	
	public int MinSum(int a[]) {
		int min = Integer.MAX_VALUE;
		int sum = 0;
		for(int i:a) {
			sum += i;
			if(min > sum )
				min = sum;
			else if(sum > 0) 
				sum=0;
		}
		return min;
	}
	
	public static int MaxSum(int[] array) {
		int maxsum = 0;
		int sum = 0;
		for (int i = 0; i < array.length; i++) {
			sum += array[i];
			if (maxsum < sum) {
				maxsum = sum;
			} else if (sum < 0) {
				sum = 0;
			}
		}
		return maxsum;
	}
	
	public void absTest() {
		
		animal a;
		cat c = new cat();
		dog d = new dog();
		a = c;
		a.talk();
//		a = d;
//		a.talk();
		a.animalTalk();
		List<Integer> al = new ArrayList<Integer>();
		
		al.add(1);
		al.add(5);
		al.add(2);
		Collections.sort(al );
		String s = Arrays.toString(al.toArray());
		System.out.println(s);
		
		
	}

}

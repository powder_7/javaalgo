package test;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class RunTest {
	@Test
	public void test() {
  
		try {
	        int z[]={2, 3, -2, -1, 5, -7, 1, -9};
	         assertEquals(-15, MinSum(z));
	         System.out.println("Min sum = " + MinSum(z));
	         
		} catch(Exception e) {
           e.printStackTrace();
    	}
	}
	
	public int MinSum(int a[]) {
		int min = Integer.MAX_VALUE;
		int sum = 0;
		for(int i:a) {
			sum += i;
			if(min > sum )
				min = sum;
			else if(sum > 0) 
				sum=0;
		}
		return min;
	}
}

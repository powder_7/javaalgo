package test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;

import org.junit.Test;

public class AlgoStringTest {

	/**
	 * Reverse string using substring function
	 */
	public void reserseStrTest() {
		
		String s = reverseStr("abcdef");
		
		System.out.println("reverse str = " + s );
	}
	//trick, append the first char from parameter to the end of the recursive return.
	public String reverseStr(String s) {

		//base case
		if(s.length()==1) return s;
		else { 
			// recursive case
			// parse the first char out and pass the remaining string into recursive.
			String tmp = s.substring(1, s.length());
			return reverseStr(tmp) + s.substring(0,1);
		}
	}
	/**
	 *  Given a string, return the first NON-repeating character that occurs in the string. EX: "adzbdcab" returns 'z'. 
	 *  Trick: use String function indexOf() and lastIndexOf(), if start and end index is the same, then it's only char.
	 */
	public void nonRepeaterTest() {
		String s = "adzbdcab";
        char[] chars = s.toCharArray();
        for(char c: chars){
            if(s.indexOf(c)==s.lastIndexOf(c)){
                System.out.println(" character is "+ c );
                break;
            }
        }
	}
	
	/**
	 *Given string str1, string str2, return first idx of sub string in str1 
	 */
	
	// Given Two Strings, check whether one string is permutation of other
	public void isPermutationTest() {
		String s1 = "abca";
		String s2 = "cbaa";
		System.out.println("String Permutatoin = " + isPermutation(s1, s2));
		
	}
	/* solution: 
	 * 1. Store first string s1 in hashmap and increase count if hash value is true
	 * 2. Loop thru hashmap and compare with string s2, 
	 * 		a.) return false if not in hashmap
	 * 		b.) if match then decrease count value.
	 * 		c.) if count < 0 return false;
	 * 3. Loop thru hashmap values, if values < 0 return false.
	*/
    public boolean isPermutation(String s1, String s2) {
    	
    	if(s1.length() != s2.length()) return false;
    	
    	Map<Character, Integer> hp = new HashMap<Character, Integer>();
    	
    	
    	char char1[] = s1.toCharArray();
    	char char2[] = s2.toCharArray();
    	
    	for(int i = 0; i< char1.length; i++) {
    		if(!hp.containsKey(char1[i])) {
    			hp.put(char1[i], 1);
    		}
    		else {
    			//get val and update
    			int val = hp.get(char1[i]);
    			val++;
    			hp.put(char1[i], val);
    		}
    	}
    	
    	// decrease hp value if match
    	for(int i=0; i<char2.length; i++) {
    		if(!hp.containsKey(char2[i]) ) {
    			return false;
    		}
    		else {
    			int val = hp.get(char2[i]);
    			val--;
    			if(val <0) return false;
    			hp.put(char2[i], val);
    		}
    	}
    	
    	// check if all value in hashmap is zero
//    	for(Character c:hp.keySet()) {
//    		if(hp.get(c)!=0) return false;
//    	}
    
    	for(Integer c:hp.values()) {
//    		System.out.println(c);
    		if(0 > c) return false;
    	}
    
    	
    	return true;
    }
    /*
     * Sort s1, s2. compare and return if true, else false.
     */
//    boolean isPermutation(String a, String b) {
//
//        if(a.length()!=b.length())
//            return false;
//        
//        char [] s1 = a.toCharArray();
//        char [] s2 = b.toCharArray();
//        
//        Arrays.sort(s1);
//        Arrays.sort(s2);
//        
//        return Arrays.equals(s1, s2);
//    }

	public void printSpiralArrayTest() {
		int n = 4;
		int m = 4;
		int input_array[][] = new int[n][m];
		int k=1;
		for(int i=0; i<n; i++) {
			for(int j=0; j<m; j++)
				input_array[i][j] = k++; 
		}
		
		printSpiralArray(input_array);
		
	}
	/*
	 * From a nxn matrix, read the matrix in spiral way 
	 * (top row, right column, bottom row, left column...)
	 */
	/* input:
			123
			456
			789
		output:
			123698745
			
		input 
			1  2  3  4
			5  6  7  8
			9  10 11 12
			13 14 15 16
			
		output
			1 2 3 4 8 12 16 15 14 13 9 5 6 7 11 10
	*/
	/**
	 * Trick is print top, fix top, print left to right. increase top toward bottom.
	 * print right, fix right, print top to bottom. increase right toward left
	 * print bottom, fix bottom, print right to left. decease bottom toward top
	 * print left, fix left, print bottom to top. increase left toward right
	 * @param input_array
	 */
	public void printSpiralArray(int input_array[][]) {

		int top=0;
		int left=0;
		// not minus 1 for array size
		// array[vertical][horizontal]
		// horizontal size 
		int right=input_array[0].length-1;
		// vertical size
		int bottom = input_array.length-1;
		
		System.out.println("right = " + right + " bottom = " + bottom);
		System.out.println("print spiral array : " + Arrays.deepToString(input_array));

		while(top <= bottom && left <=right) {
			
			//print top, vertical is fix, iterate horizontal
			for(int i = left; i <= right; i++) {
//				System.out.print("top = " + input_array[top][i] + " " );
				System.out.print(input_array[top][i] + " " );
			}
			
			// increase top by 1
			top++;
			//print right, fix horizontal, iterate vertical
			for(int i=top; i<=bottom; i++) {
//				System.out.print("right = " + input_array[i][right] + " " );
				System.out.print(input_array[i][right] + " " );
			}
			
			// decrease right by 1
			right--;
			if(top < bottom) {
				//print bottom, fix vertical, iterate horizontal 
				for(int i=right; i>=left; i--) {
//					System.out.print("bottom = " + input_array[bottom][i] + " " );
					System.out.print(input_array[bottom][i] + " " );
				}
				//decrease bottom by 1, move up
				bottom--;
			}
			
			if(left < right) {
				//print left, fix vertical, iterate horizontal
				for(int i=bottom; i>=top; i--) {
//					System.out.print("left = "+ input_array[i][left] + " " );
					System.out.print(input_array[i][left] + " " );
				}
				// increase left by 1
				left++;
			}
		}
	}
	
	// Pangram is a string that use all the alphabet
	public void pangramTest() {
		String s = "The quick brown fox jumps over the lazy dog.";
		System.out.println(s + " pangram : " + isPangram(s));
	}
	
	/**
	 * 
	 * @param s string of characters
	 * @return true if hashset size is 26 else false
	 */
	public boolean isPangram(String s) {
		Set<Character> hp = new HashSet<>();
		String tmp = s.toLowerCase();
		for(int i=0; i<tmp.length(); i++) {
			if(Character.isLetter(tmp.charAt(i)) && !hp.contains(tmp.charAt(i))) {
				hp.add(tmp.charAt(i));
			}
		}
		
		return (hp.size()==26);
	}
	
	/*
	 * String permutation
	 */
	public  void permutationTest() { 
		String str="aab";
		System.out.println("safasd " + str.substring(0,0));
	    permutation("", str); 
	}
	/*
	 * substing[), include start, exclude end
	 */
	private  void permutation(String prefix, String str) {
	    int n = str.length();
	    if (n == 0) System.out.println(prefix);
	    else {
	        for (int i = 0; i < n; i++) {
	        	System.out.println("pre["+prefix+ "] str[" +str + "] i = " + i + " sub [" + str.substring(0, i) + "] sub 2[" + str.substring(i+1, n) + "]" );
	            permutation(prefix + str.charAt(i), str.substring(0, i) + str.substring(i+1, n));
	           
	        }
	    }
	}

	/*
	 * Rotate an array of n elements to the right by k steps.
	 * For example, with n = 7 and k = 3, 
	 * the array [1,2,3,4,5,6,7] is rotated to [5,6,7,1,2,3,4].
	 */
	public void rotationArrayTest() {
		int a[] = {1,2,3,4,5,6,7};
		int k=3;
		rotationArray(a, k);
	}
	
	/*
	 * Offset i=rotation k, loop rot_ary[0..k] = a[k..n], n=arry.len;
	 * then loop rot_ary[k..j] = a[0..j], j=arry.len;
	 * 
	 * O(n) time and space.
	 * can be solved in O(1) space.
	 */
	public void rotationArray(int a[], int k) {
		
		int rot_ary[] = new int[a.length];
		int j=0;
		
		for(int i=k; i<a.length;i++) {
			rot_ary[j++]= a[i]; 
		}
		for(int i=0; j< a.length;j++) {
			rot_ary[j]=a[i++];
		}
		System.out.println("rotate array = " + Arrays.toString(rot_ary));
	}
	
	/*
	 * Given an array of random numbers, Push all the zero's of a given array to the end of the array. 
	 * For example, if the given arrays is {1, 9, 8, 4, 0, 0, 2, 7, 0, 6, 0}, 
	 * it should be changed to {1, 9, 8, 4, 2, 7, 6, 0, 0, 0, 0}. 
	 * The order of all other elements should be same. 
	 * Expected time complexity is O(n) and extra space is O(1).
	 */
	
	public void zeroEOFArrayTest() {
		int a[] =  {1, 9, 8, 4, 0, 0, 2, 7, 0, 6, 0};
		zeroEOFArray(a);
	}
	/*
	 * O(n), 2n= n.
	 */
	public void zeroEOFArray(int a[]) {
		int tmp = -1;
		List<Integer> list = new ArrayList<>();
		for(int i=0;i<a.length;i++) {
			if (0 == a[i]) {
				for (int j=i; j < a.length; j++) {
					if (0 != a[j]) {
						tmp = a[j];
						a[j] = a[i];
						a[i] = tmp;
						break;
					}
				}
			}
		}
		//space O(n)
		for(int x : a) {
			if(x!=0) {
				list.add(x);
			}
			else list.add(list.size(), x);
		}
		System.out.println("ll = " + list);
		System.out.println("array = " + Arrays.toString(a) );
	}
	
	/*
	 * Find the second largest number in a given array.
	 * Return 0 if the given array has no second largest number. 
	 */
	public void find2ndLargestTest() {
		
		int a[]= {1,7,21,4,5,9,10};
		
		System.out.println("second largest = " + find2ndLargest(a));
	}
	
	/*
	 * Answer: if max found, second = largest, update largest
	 * Else if second < a[i] update second.
	 * 
	 * Trick if array size 2, need else if statement.
	 */
	public int find2ndLargest(int a[]) {
		
		int lrg = Integer.MIN_VALUE;
		int secd = Integer.MIN_VALUE;
		
		for(int i=0;i<a.length;i++) {
		
			if(lrg < a[i]) {
				secd = lrg;
				lrg = a[i];
			}
			// handle array size of 2
			else if(secd < a[i]) {
				secd = a[i];
			}
		}
		
		return secd;
	}
	
	/*
	 * Given two strings, determine if they are isomorphic. 

		Two words are called isomorphic if the letters in one word can be remapped to get the second word.
	 	Remapping a letter means replacing all occurrences of it with another letter 
	 	while the ordering of the letters remains unchanged. 
	 	No two letters may map to the same letter, but a letter may map to itself.
	 	
	 	Given 'foo', 'app', returns true
		We can map 'f' -> 'a' and 'o' -> 'p'
		
		Given 'bar', 'foo', returns false
		We can�t map both 'a' and 'r' to 'o'
		
		Given 'turtle', 'tletur', returns true
		We can map 't' -> 't', 'u' -> 'l', 'r' -> 'e', 'l' -> 'u', 'e' -'r'
		
		Given 'ab', 'ca', returns true
		We can map 'a' -> 'c', 'b' -> 'a'
		
		Known: 	unique key can't map to 2 same values.
	  			key can map itself. 
	 */
	public void isomorphicTest() {
		//[row][col]
		String s[][]= {
				{"foo", "app"},
				{"bar", "foo"},
				{"turtle", "tletur"},
				{"ab", "ca"}
				};
		
		String s1="";
		String s2="";
 
		for(int i=0; i<s.length; i++){
		
			s1=s[i][0];
			s2=s[i][1];
			System.out.println(Arrays.deepToString(s[i]) + " ismorphic = " +isomorphic(s1, s2) );
		}
	}
	
	/*
	 * Map s1 and s2 with respected values
	 * check map s1 and s2 
	 * if dup and s1 char[i] != s2 char[i] return false
	 * Solution: s1 and s2 are 1 to 1 mapping
	 */
	public Boolean isomorphic(String s1, String s2) {
		
		Map<Character, Character> hp_s1 = new HashMap<Character, Character>();
		Map<Character, Character> hp_s2 = new HashMap<Character, Character>();
		
		Map<Character, Integer> m1 = new HashMap<>();
	    Map<Character, Integer> m2 = new HashMap<>();
	        
	        
		char char1[] = s1.toCharArray();
		char char2[] = s2.toCharArray();
		for(int i=0; i<char1.length; i++) {
//			if(!hp_s1.containsKey(char1[i]) ) {
//				hp_s1.put(char1[i], char2[i]);
//			}
//			// dup key with different values. Not 1 to 1
//			else if(char2[i] != hp_s1.get(char1[i])) {
//					return false;
//			}
//			
//			if(!hp_s2.containsKey(char2[i])) {
//				hp_s2.put(char2[i], char1[i]);
//			}
//			// dup s2 key. return false if s2 map value != s1 char[i]
//			else if(char1[i] != hp_s2.get(char2[i])) {
//					return false;
//			}
			
			//WOWO same concept 1 to 1 mapping
			// x,y is NULL if hashing first time,
			// else return previous hash value or value that replaced.
			Integer x = m1.put(s1.charAt(i), i);
			Integer y = m2.put(s2.charAt(i), i);
			System.out.println(x + "  " + y);
	       if ( x != y )
	                return false;
 
		}
 		return true;
	}
	
	/*
	 * Find the longest sequence of same characters in a string
	 * Output : s="accbbbdd", {b:3}
	 * 
	 * Algo: loop thru string, 
	 * if contain,  hash char, 
	 * else hashmap clear and update hash
	 * update max and seq char 
	 * 
	 * Algo: loop thru string
	 * if prev == curr char, count++
	 * else count =0
	 * update max and seq. reset count=1
	 * prev = curr
	 * 
	 */
	public void findLongestSeq() {
	
		String str = "aabbbccddd";
		Map<Character, Integer> hm = new HashMap<Character, Integer>();
		
		hm.put(str.charAt(0), 1);
		int max = 1;
		int count = 0;
		char seq = str.charAt(0);
		for(int i=1; i<str.length(); i++) {
			if(hm.containsKey(str.charAt(i))) {
				//update hash count
				hm.put(str.charAt(i), hm.get(str.charAt(i))+1);
			}
			else {
				hm.clear();
				hm.put(str.charAt(i), 1);
			}
			count = hm.values().stream().findFirst().get();
			if(count > max) {
				seq = hm.keySet().stream().findFirst().get();
				max = count;
			}
		}
		System.out.println("hm char = " + seq + " max " + max);
	
		char prev = '1';
		max = 0;
		count = 1;
		for(char c : str.toCharArray()) {
			if(prev == c) {
				count++;
			}
			else 
				count = 1;
			if(count > max ) {
				max = count;
				count = 1;
				seq = c;
			}
			prev = c;
			System.out.println(c);
		}
		
		System.out.println("char = " + seq + " max " + max);
		
	}
	
	/*
	 * 0. depth first search
	 * 1. print level == string len
	 * 2. loop thru string, index = level
	 * 3. swap(level , index i)
	 * 4. recursive(s, level+1, string len)
	 * 5. swap back. 
	 */
	public void permutation(char[] s, int l, int r) {
		if(l==r)
			System.out.println(s);
		
		for(int i=l; i < s.length; i++) {
		
			//swap
			char tmp = s[l];
			s[l] = s[i];
			s[i] = tmp;
			permutation(s,l+1, r);
			//swap back
			tmp = s[l];
			s[l] = s[i];
			s[i] = tmp;
		}
		
	}
	public void doPermutation() {
		char s[] = {'a','b','c'};
		permutation(s, 0, s.length);
//		System.out.println(s);
	}
	
	
	public void sort012() {
		 int a[] = {0, 1, 1, 0, 1, 2, 1, 2, 0, 0, 0, 1};
		 int arr_size = a.length;
		 
		int lo = 0;
		int hi = arr_size - 1;
		int mid = 0, temp = 0;
		while (mid <= hi) {
			switch (a[mid]) {
			case 0: {
				temp = a[lo];
				a[lo] = a[mid];
				a[mid] = temp;
				lo++;
				mid++;
				break;
			}
			case 1:
				mid++;
				break;
			case 2: {
				temp = a[mid];
				a[mid] = a[hi];
				a[hi] = temp;
				hi--;
				break;
			}
			}
		}
		System.out.println(Arrays.toString(a));
	}
	// Given two lowercase strings, S1 and S2, sort S1 in same order as S2. 
	// If a character in S1 doesn't exist in S2, put them at the end. 
	// If S1 is "program" and S2 is "grapo", then return "grrapom".
	public void sortS1S2Test() {
		String s1 = "program";
		String s2 = "grapo";
		
		System.out.println(sortS1S2(s1,s2));
		
	}
	// hash s1 and store value base on repeats
	// loop s2 and check if containsKey add to result n time or value
	// remove the keys after added to result.
	// loop thru the hash and add the remainin to result.
	public String sortS1S2(String s1, String s2) {
		String result ="";
		Map<Character, Integer> hm = new HashMap<>();
		
		for(char c : s1.toCharArray()) {
			if(hm.containsKey(c))
				hm.put(c, hm.get(c)+1);
			else hm.put(c, 1);
		}
		
		for(char c : s2.toCharArray()) {
			if(hm.containsKey(c) ) {
				for(int i = 0; i < hm.get(c); i++) {
					result += c;
				}
			}
			hm.remove(c);
		}
		
		for(Entry<Character, Integer> entry : hm.entrySet()) {
			for(int j = 0; j < entry.getValue(); j++) {
				result += entry.getKey();
			}
		}
		
		return result;
	}
	/*
	 * Given a text txt[0..n-1] and a pattern pat[0..m-1], 
	 * write a function search(char pat[], char txt[]) that prints all occurrences of pat[] in txt[]. 
	 * You may assume that n > m.
	 * answer: loop thru string s, 
	 *	  if match, count++
	 *	  else reset count=0
	 *	  if count == pat size, 
	 *		print index = i+1-path.lenght(), 
	 *		reset count
	 */
	public void findPattern() {
		String pat ="abc";
		String s = "abcasdfabc";
		int count=0;
		
		for(int i = 0; i < s.length(); i++) {
			if(s.charAt(i)== pat.charAt(count) ) {
				count++;
			}
			else {
				count = 0;
			}
			if(count == pat.length()) {
				System.out.println("index match at " + (i+1-pat.length()));
				count = 0;
			}
		}
		//or
		int index = 0;
		while(index < s.length()) {
			index = s.indexOf(pat, index);
			System.out.println("index match at = " + index);
			index += pat.length();
		}
		
	}
	// poisonus plant
	// loop thru array, 
	// store values in deceding order in a list<linkedlist<Integer>>
	// each new linkedlist is a set of deceding order values
	// 
	// loop thru list from 1,..,n. remove first element of linkedlist, day++.
	// merge linkedlist if in deceding order.
	// stop when all linkedlist merge into list[0] linkedlist.
	
	public int poisonPlant() {
//		int p[] = {6, 5, 8, 7, 4, 7, 3, 1, 1, 10};
//		int p[] = {6, 5, 8, 4, 7, 10, 9}; //output 2
		int p[] = {3, 2, 5, 4}; // outpus 2
//		int p[] = {4, 3, 7, 5, 6, 4, 2}; //output 3
//		int p[] = {1, 3, 5, 2, 7, 6, 4, 2, 1}; // output 4
		
		int days=0;
		List<LinkedList<Integer>> list = new ArrayList<>();
		list.add(new LinkedList<Integer>());
		list.get(list.size()-1).add(p[0]);
		 
		for(int i = 1; i < p.length; i++) {
			//check for deceding order
			if(p[i] <=list.get(list.size()-1).getLast()) {
				list.get(list.size()-1).add(p[i]);
			}
			//make new linkedlist and add to list array
			else {
				list.add( new LinkedList<Integer>());
				list.get(list.size()-1).add(p[i]);
			}
		}
		
		for(int i=list.size()-1; i >= 1; i--) {
  
			int j = 1;
			while(j < list.size() ) {
				list.get(j).removeFirst();
				if(list.get(j).isEmpty()) {
					list.remove(j);
					j--;
				}
				j++;
			}
			i = list.size()-1;
			
			// merge linkedlist
			if(i >=1) {
				int x = list.get(i).peekFirst();
				int y =  list.get(i-1).peekLast();
				if(x <= y) {
					list.get(i-1).addAll(list.get(i));
					list.remove(i);
				}
				
			}
			days++;
		}
		System.out.println(days);
		return days;

	}
	
	/*
	 * It can be solved by using stack. Let's take a look at the example below: 1,10,9,2,8,7,6,5,4,3. 
	 * After 2 days, it will become 1,2,6,5,4,3. After 4 more days, it will become 1 and no more dying. 
	 * One thing we can notice for a plant at idx i, if the left one at i - k is smaller than i, 
	 * it can eliminate all the plants the i can eliminate. 
	 * Also, a plant can finally eliminate all the plants has more pesticide(greater value) on its right hand side. 
	 * So if we know how many days a plant at idx i, we can know how many days the left plants need to eliminate the plants. 
	 * Below is a concise Java solution, which better explains the idea:
	 */
	
	public class Pair {
		int val, count;

		public Pair(int val, int count) {
			this.val = val;
			this.count = count;
		}
	}
	public void poisonousPlantsTest() {
//		int p[] = {6, 5, 8, 7, 4, 7, 3, 1, 1, 10}; // output 2
//		int p[] = {6, 5, 8, 4, 7, 10, 9}; //output 2
//		int p[] = {3, 2, 5, 4}; // outpus 2
//		int p[] = {4, 3, 7, 5, 6, 4, 2}; //output 3
//		int p[] = {1, 3, 5, 2, 7, 6, 4, 2, 1}; // output 4
		int p[][] = {	
						 
						{6, 5, 8, 7, 4, 7, 3, 1, 1, 10}
//						{6, 5, 8, 4, 7, 10, 9},
//						{1, 3, 5, 2, 7, 6, 4, 2, 1},
//						{1, 9, 8, 7, 6, 5, 4, 3, 2},
//						{3, 2, 5, 4}
					};
		
//		for(int a[] : p ) {
//			System.out.println(" array = " + Arrays.toString(a) + " days = " + poisonousPlants(a));
//		}
		
		// output 10
		int q[] = {403, 1048, 15780, 14489, 15889, 18627, 13629, 13706, 16849, 13202, 10192, 17323, 4904, 6951, 16954, 5568, 4185, 7929, 8860, 14945, 3764, 4972, 13476, 14330, 1174, 18952, 10087, 10863, 9543, 12802, 1607, 9354, 13127, 920};
		System.out.println(" array = " + Arrays.toString(q) + " days = " + poisonousPlants(q));
		
	}
	// Time O(2n) = O(n)
	// Space O(n)
	/*
	 * Note: 
	 */
	public int poisonousPlants(int[] p) {
		Stack<Pair> stack = new Stack<>();
		int cnt = 0;
		for (int i = p.length - 1; i >= 0; i--) {
			int temp = 0;
			while (!stack.empty() && p[i] < stack.peek().val) {
				temp++;
				Pair pair = stack.pop();
				temp = Math.max(temp, pair.count);
			}
			cnt = Math.max(cnt, temp);
			stack.push(new Pair(p[i], temp));
		}

		stack.forEach(st -> System.out.println(st.val + " " + st.count));
		return cnt;
	}
	  
	 public int kEmptySlots(int[] flowers, int k) {
	        int len = flowers.length, res= len+1; 
	         
	        // An array to store which day a flower at position i bloom
	        // 1-based
	        int[] days = new int[len]; 
	         
	        for (int i = 0; i < len; i++) {
	            int pos = flowers[i];  // 1-based position
	            days[pos - 1] = i + 1;  // Set the day the flower bloom
	        }
	         
	        // Walk through the days and find two flowers k positions apart 
	        // that have no earlier flowers in between
	        int left = 0; 
	        int right = k+1; 
	         
	        for (int i = 0; right < len; i++) {
	   
	            if (days[i] < days[left] || days[i] <= days[right]) {
	                if (i == right) {
	                    // Found a solution
	                    res = Math.min(res,Math.max(days[left], days[right]));
	                }
	                // Flower at position i cannot in between, 
	                // need to shift left to the current position
	                left = i; 
	                right = i + k + 1; 

	            }

	        }
	         
	        return (res > len ? -1 : res);
	 }
	 
	 public int kEmptySlotss(int[] flowers, int k) {
	        if (flowers == null || flowers.length < 1) {
	              return -1;
	          }
	           
	          // core logic
	          TreeSet<Integer> bloomingTreeSet = new TreeSet<>();
	          int day = 0;
	          for(int flower: flowers) {
	              day++;
	              bloomingTreeSet.add(flower);
	              Integer higher = bloomingTreeSet.higher(flower);
	              Integer lower = bloomingTreeSet.lower(flower);
	              if (higher != null && higher - flower - 1 == k || 
	                  lower != null && flower - lower - 1 == k ) {
	                  return day;
	              }
	          }
	          
	          return -1;
	}
	 
	public void kEmptySlotsTest() {
			
		int a[] = {1,2,3};
		int k = 1;
		
		System.out.println(kEmptySlotss(a,k));
	}
	
 	@Test
	public void test() {
//		isomorphicTest();
//		find2ndLargestTest();
//		zeroEOFArrayTest();
//		rotationArrayTest();
//		permutationTest();
//		permuteTest();
//		pangramTest();
//		printSpiralArrayTest();
//		isPermutationTest();
//		nonRepeaterTest();
//		reserseStrTest();
//		findLongestSeq();
//		doPermutation();
//		sort012();
//		sortS1S2Test();
//		findPattern();
// 		poisonousPlantsTest();
 		kEmptySlotsTest();
	}
	
}

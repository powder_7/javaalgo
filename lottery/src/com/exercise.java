package com;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Random;
import java.util.Stack;

import org.hamcrest.CoreMatchers;
import org.junit.rules.ErrorCollector;
import org.w3c.dom.Element;

class Node {

	Object x;
	int weight;
	ArrayList<Node> al;

	Node(int x) {
		this.x = x;
		weight = 0;
		al = new ArrayList<Node>();
	}

	public void setWeight(int w) {
		weight = w;
	}
}

class Nodes {
	int x;
	Nodes n;
}

class Tree {
	int x;
	Tree l;
	Tree r;
}

public class exercise {

	private ArrayList<ArrayList<Integer>> my_al = new ArrayList<ArrayList<Integer>>();
	private Node root = null;
	private Nodes roots = null;
	
	public void setArrayList() {
		ArrayList<Integer> tmp;
		tmp = new ArrayList<Integer>();
		tmp.add(9);
		tmp.add(7);
		tmp.add(0);
		my_al.add(tmp);

		tmp = new ArrayList<Integer>();
		tmp.add(9);
		tmp.add(15);
		my_al.add(tmp);

		tmp = new ArrayList<Integer>();
		tmp.add(7);
		tmp.add(4);
		tmp.add(2);
		my_al.add(tmp);

		tmp = new ArrayList<Integer>();
		tmp.add(7);
		tmp.add(8);
		my_al.add(tmp);

		tmp = new ArrayList<Integer>();
		tmp.add(8);
		tmp.add(20);
		tmp.add(10);
		my_al.add(tmp);

		tmp = new ArrayList<Integer>();
		tmp.add(15);
		tmp.add(12);
		tmp.add(2);
		my_al.add(tmp);

		tmp = new ArrayList<Integer>();
		tmp.add(15);
		tmp.add(16);
		my_al.add(tmp);

		// tmp = new ArrayList<Integer>();
		// tmp.add(15);
		// tmp.add(96);
		// my_al.add(tmp);

		tmp = new ArrayList<Integer>();
		tmp.add(4);
		tmp.add(1);
		tmp.add(13);
		my_al.add(tmp);

		tmp = new ArrayList<Integer>();
		tmp.add(4);
		tmp.add(2);
		my_al.add(tmp);
	}

	public int getRoot() {

		setArrayList();
		int root = -1;
		ArrayList<Integer> parent = new ArrayList<Integer>();
		ArrayList<Integer> child = new ArrayList<Integer>();
		for (int i = 0; i < my_al.size(); i++) {
			parent.add(my_al.get(i).get(0));
			child.add(my_al.get(i).get(1));
		}

		for (int i = 0; i < parent.size(); i++) {
			if (!child.contains(parent.get(i))) {
				root = parent.get(i);
				break;
			}
		}

		System.out.println(parent);
		System.out.println(child);

		return root;
	}

	public void printTree(Node t) {

		if (t != null) {
			System.out.println(t.x);
			if(t.al != null)
				for (int i = 0; i < t.al.size(); i++) {
					printTree((Node) t.al.get(i));
				}
		}
	}

	// print tree by level
	public void printBFSTree(Node t) {

		Queue<Node> qe = new LinkedList<Node>();
		qe.add(t);
		int curlevel = 1;
		int nextlevel = 0;

		while (!qe.isEmpty()) {
			Node tmp = qe.remove();

			if (curlevel == 0) {
				System.out.println();
				curlevel = nextlevel;
				nextlevel = 0;
			}

			for (int i = 0; i < tmp.al.size(); i++) {
				qe.add(tmp.al.get(i));
				nextlevel++;
			}

			curlevel--;
			System.out.print(tmp.x + " ");
		}
	}

	public void printDFSTree(Node t) {

		if (t != null) {
			Stack<Node> n_stk = new Stack<Node>();
			n_stk.push(t);

			while (!n_stk.empty()) {

				Node n = n_stk.pop();

				System.out.println(n.x);

				for (int i = 0; i < n.al.size(); i++) {
					n_stk.push(n.al.get(i));
				}

			}

		}
	}

	public int printSubTreeWeight(Node t) {
		int t_weight = 0;
		if (t != null) {

			for (int i = 0; i < t.al.size(); i++) {
				t_weight += printSubTreeWeight(t.al.get(i))
						+ t.al.get(i).weight;
			}

			int tt_weight = t_weight + t.weight;
			System.out.println("node = " + t.x + "\t total weight = "
					+ tt_weight + "\t node weight =  " + t.weight);

			return t_weight;
		}
		return t_weight;
	}

	public Node buildTree(int root) {

		Node r = new Node(root);
		// get children
		ArrayList<Integer> children = new ArrayList<Integer>();
		for (int i = 0; i < my_al.size(); i++) {
			if (my_al.get(i).get(0) == root) {
				// child
				children.add(my_al.get(i).get(1));
				// weight
				if (my_al.get(i).size() > 2) {
					r.setWeight(my_al.get(i).get(2));
					System.out.println("weight = " + my_al.get(i).get(2));
				}

			}
		}
		System.out.println("root [ " + root + " ]" + children);
		if (children.size() > 0) {
			for (int i = 0; i < children.size(); i++) {
				r.al.add(buildTree(children.get(i)));
			}
		}

		return r;

	}

	
	private void addNode(Node rt, int x) {
	
		Node r = rt;
		Node tmp = new Node(x);
				
		if(r==null) root=tmp;
		else if(r.al.size() == 0 ) {
			if((Integer)r.x > x) {
				tmp.al.add(r);
				root = tmp;
			}
			else {
				root.al.add(tmp);
			}
		}
		else {
			System.out.println("debug0");
			Node prev = r;
			Node cur  = r.al.get(0);
			while(cur.al.size() > 1) {
				if((Integer)cur.x > x) {
					tmp.al.add(cur);
					prev.al.add(tmp);
				}
				if(cur.al.size() > 1) {
					prev = cur;
					cur = cur.al.get(0);
				}
				System.out.println("debug");
			}
			root = r;
		}
	}
	
	private void addNode(Nodes rt, int x) {
		Nodes r = rt;
		Nodes tmp = new Nodes();
		tmp.x = x;
		tmp.n = null;
			
		if(r==null) {
			r = new Nodes();
			r = tmp;
			System.out.println("asdfdsa");
		}
		else {
			Nodes prev = null;
			Nodes cur = r;
			while(cur!=null) {
				if(cur.x > x) break;
				prev = cur;
				cur = cur.n;
			}
			// insert val ahead of cur
			tmp.n = cur;
			
			if(prev == null) r=tmp; //only head element
			// deal with mid and end of element. 
			// cur is null so x is biggest insert x at end
			else prev.n = tmp;      
				
		}
		rt = r;
//		System.out.println(rt);
//		printTree(rt);
	}
	
	public void printNode(Nodes r) {
		while(r!=null) {
			System.out.println(" x " + r.x);
			r = r.n;
		}
	}
	
	public void test_addNode() {
	
//		this.addNode(root, 2);
//		this.addNode(root, 3);
//		this.addNode(root, 10);
//		this.addNode(roots, 6);
//		this.addNode(roots, 20);
//		this.printTree(root);
		
		Nodes my_root = new Nodes();
		my_root.x = 1;
		this.addNode(my_root, 2);
		this.addNode(my_root, 3);
		this.addNode(my_root, 10);
		this.printNode(my_root);
		
	}
	
	public Tree BST_Add(Tree r, int x) {
 
		if(r==null) {
			Tree tmp = new Tree();
			tmp.x = x;
			r = tmp;
			System.out.println("debug nul ");
		}
		else if (r.x > x) {
			System.out.println("debug left " + x);
			r.l = BST_Add(r.l, x);
		}
		else if (r.x < x) {
			System.out.println("debug right " + x);
			r.r = BST_Add(r.r, x);
		}

		return r;
	}
	
	public void printTree(Tree r) {
		if(r!=null) {
			System.out.println(r.x);
			printTree(r.l);
			printTree(r.r);
		}
	}
	
	public void printBFSTree(Tree r) {

		if (r == null)
			return;
		Queue<Tree> q = new LinkedList<Tree>();
		q.add(r);
		int x = 0;
		int cur = 0, next = 0;
		Tree tmp = null;
		while (!q.isEmpty()) {

			tmp = q.poll();
			x = tmp.x;

			if (tmp.l != null) {
				q.add(tmp.l);
				next++;
			}
			if (tmp.r != null) {
				q.add(tmp.r);
				next++;
			}

			System.out.print(x);
			if (cur == 0) {
				System.out.println(" ");
				cur = next;
				next = 0;
			}
			cur--;
		}
	}
	public void test_BST_Tree() {

		Tree r = null;
		r = BST_Add(r, 2);
		r = BST_Add(r, 1);
		r = BST_Add(r, 5);
		r = BST_Add(r, 4);
		r = BST_Add(r, 6);
//		printTree(r);
		printBFSTree(r);
	}

	// add all the numbers in array to make 2 min integers.
	// {1,2,3,4,5,6}
	// {1,3,5} = 9
	// {2,4,6} = 12

	public void minSum() {

		int x[] = { 1, 3, 5, 2, 4, 6 };
		Arrays.sort(x);

		System.out.println("x = " + Arrays.toString(x));
		String y = "", z = "";
		int even = 0, odd = 0;
		for (int i = 0; i <= x.length / 2; i++) {
			// even
			even = 2*i;
			odd = even+1;
			if (even < x.length)
				y += Integer.toString(x[even]);
			// odd
			if (odd < x.length)
				z += Integer.toString(x[odd]);

		}

		System.out.println("y = " + y + " z = " + z);

		int sum = Integer.parseInt(y) + Integer.parseInt(z);

		System.out.println("total sum = " + sum);
	}

	// circler link list
	// need to come and look over
	public void initLinkList() {

		Node root = new Node(9);
		Node tmp_r = root;
		for (int i = 0; i < 4; i++) {
			Node tmp = new Node(i);
			tmp_r.al.add(tmp);
			tmp_r = tmp_r.al.get(0);

		}

		tmp_r.al.add(root);
		int j = 0;
		while (root != null) {
			int x = (Integer) root.x;
			if (root.al.size() > 0)
				root = root.al.get(0);
			else
				break;
			System.out.println("link val = " + x);
			if (j > 3)
				break;
			j++;
		}
	}

	public void isSubString() {
		String s = "asabcb";
		String s_sub = "abc";

		String tmp_str = s + s;
		System.out.println("sub string = " + tmp_str.contains(s_sub));

		int sub_idx = 0;
		for (int i = 0; i < s.length(); i++) {

			if (s.charAt(i) == s_sub.charAt(sub_idx)) {
				sub_idx++;
			} else {
				sub_idx = 0;
			}
			if (sub_idx == s_sub.length()) {
				break;
			}
		}
		if (sub_idx == s_sub.length())
			System.out.println("substr found idx = " + sub_idx);
		else
			System.out.println("substr not found");

	}

	// this isn't correct
	public void isStrPattern() {

		String s = "asss";
		String s_str = "ass";
		int i;
		int str_idx = 0;
		boolean found = false;
		for (i = 1; i < s.length(); i++) {

			if (s.charAt(i - 1) != s.charAt(i)) {
				System.out.println(" char = " + s.charAt(i - 1) + " i = "
						+ (i - 1));
				if (s.charAt(i - 1) == s_str.charAt(str_idx)) {
					str_idx++;
				} else
					str_idx = 0;

			}
			if (str_idx == s_str.length()) {
				found = true;
				break;
			}
		}
		// special case for last index in array
		if (!found) {
			if (s.charAt(i - 1) == s_str.charAt(str_idx))
				str_idx++;
		}

		if (str_idx == s_str.length()) {
			System.out.println("pattern found str_idx = " + str_idx);
		} else
			System.out.println("pattern not found str_idx = " + str_idx);

	}

//	// check if int and oct value are pal
//	public void isIntgerPal() {
//
//		for (int n = 100; n < 300;) {
//			n++;
//			String s = Integer.toString(n);
//			String oct = Integer.toOctalString(n);
//
//			if (this.isPali(s, s.length()) && this.isPali(oct, oct.length())) {
//				System.out.println("int = " + s + " oct = " + oct + " is pal");
//			}
//		}
//	}

	public void intToOct() {

		int i = 74;
		String s = Integer.toOctalString(i);
		String out = "";
		int j = 0;

		while (i > 0) {

			out = Integer.toString(i % 8) + out;
			i /= 8;
		}
		// out = i + out;
		System.out
				.println(" oct  = " + out + " remainder " + i + " oct s " + s);
	}

	/*
	 * Given a amount and several denominations of coins, find all possible ways
	 * that amount can be formed? eg amount = 5, denominations = 1,2,3. Ans- 5
	 * ways 1) 1,1,1,1,1 2) 1,1,1,2 (combinations aren't counted eg 1,2,1,1 etc)
	 * 3) 1,1,3 4) 1,2,2 5) 2,3
	 */
	public void initGetCoinCombinations() {
		int[] coins = new int[] { 1, 2, 3 };
		int amt = 5;
		System.out.println(" Combinations :" + getCoinCombinations(0, coins, 5));
//		System.out.println(" combo coints : " + this.comboCoin(5, coins, 0));
	}

	public int getCoinCombinations(final int start, final int[] coins,
			int amount) {
		int result = 0;
		if (amount == 0) {
			return 1;
		}
		for (int i = start; i < coins.length; i++) {
			if (amount >= coins[i]) {
//				System.out.println(" amount = " + amount + " coins = "
//						+ coins[i]);
				result += getCoinCombinations(i, coins, amount - coins[i]);

			}
		}
		return result;
	}
	
	// amt = 5, coins={1,2,3}
	private int comboCoin(int amt, int[]coins, int idx) {
		
		int result = 0;
		if(0 == amt) result = 1;
		else if( 0 > amt || 0 == coins.length) return 0;
		for(int i=idx; i<coins.length; i++) {
			if(amt >= coins[i]) {
				System.out.println(" amount = " + amt + " coins = " + coins[i] + " i = " + i);
				result += comboCoin(amt-coins[i], coins, i );
			}
		}
		
		return result;
	}

	// need to use heap/priority queue aka sort queue
	public void kthMin() {

		int a[] = { 1, 2, 3, 9 };
		int b[] = { 1, 4, 7, 11 };
		int k = 3;
		PriorityQueue<Integer> pq = new PriorityQueue<Integer>();
		int count = 0;
		int min_val = 0;
		pq.clear();
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < b.length; j++) {
				// store the first column in priority queue
				int x = a[i] + b[j];
				pq.add(x);
				// if(pq.size() == k) break;
			}
			if (pq.size() > k * k)
				break;
		}

		this.matrix(a, b);
		Iterator it = pq.iterator();

		System.out.println(" pq size = " + pq.size());
		while (it.hasNext()) {
			System.out.print(" " + it.next());
		}

		while (k > 0) {
			min_val = pq.remove();
			k--;
		}
		System.out.println("\nk = " + k + " k min = " + min_val + " pq size = "
				+ pq.size());

	}

	// saddleback algo
	// start top right
	// z > c[x][y] , x--, else y++
	public void saddleback() {

		int a[] = { 1, 2, 3, 9 };
		int b[] = { 1, 4, 7, 11 };
		int z = 3;

		int c[][] = this.matrix(a, b);

		// start at top right matrix
		int x = a.length - 1;
		int y = 0;
		for (int i = 0; i < c.length; i++) {
			for (int j = 0; j < c[0].length; j++)
				System.out.print("\t" + c[j][i]);
			System.out.println("");
		}
		while (x >= 0 && y < b.length) {

			if (z == c[x][y]) {
				System.out.println(" found [ " + c[x][y] + " ] " + " at " + x
						+ ", " + y);
				break;
			} else if (z > c[x][y]) {
				y++;
			} else {
				x--;
			}
			if (z == c[x][y]) {
				System.out.println(" found [ " + c[x][y] + " ] " + " at " + x
						+ ", " + y);
				break;
			}
		}
		if (x <= 0 || y >= b.length) {
			System.out.println(" not found ");
		}
	}

	public int[][] matrix(int a[], int b[]) {
		// / int b[] = { 3, 8, 10 };
		// int a[] = { 2, 5, 7 };

		int mat_ary[][] = new int[a.length][b.length];

		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < b.length; j++) {
				int x = a[i] + b[j];
				mat_ary[i][j] = x;
				// System.out.print("\t" + x);
			}
			// System.out.println("");
		}
		return mat_ary;
	}

	public void do_qsort() {
		int a[] = { 1, 3, 2, 5, 1,4 };

//		this.qsort(a, 0, a.length - 1);
		this.qs(a, 0, a.length-1);
		
		System.out.println(" qsort = " + Arrays.toString(a));
	}

	private void qsort(int a[], int left, int right) {

		if(left<right) {
			int mid = partition(a, left, right);
			// System.out.println("mid = " + mid);
//			if (0 < mid)
				qsort(a, left, mid - 1);
//			if (mid < right)
				qsort(a, mid, right);
		}
	}

	private int partition(int a[], int left, int right) {

		int mid = (left+right)/2;
		int pivot = a[mid];
		while(left <= right) {
			while(a[left]  < pivot) left++;
			while(a[right] > pivot) right--;
			//swap
			if(left <= right) {
				int tmp = a[left];
				a[left++] = a[right];
				a[right--] = tmp;
//				left++;
//				right--;
			}
		}
		return left;
	}

	public void do_mergeSort() {

		int a[] = { 3, 2, 1 };
		mergeSort(a, 0, a.length - 1);
		System.out.println("mergeSort = " + Arrays.toString(a));

	}

	private void mergeSort(int a[], int left, int right) {

		if (left < right) {
			int mid = ((right + left) / 2);
			mergeSort(a, left, mid);
			mergeSort(a, mid + 1, right);
			mergeArray(a, left, mid, right);
		}

	}

	private void mergeArray(int a[], int left, int mid, int right) {

		int tmp[] = new int[right - left + 1];
		int i = left;
		int j = mid + 1;
		int k = 0;
		// loop thru array to sort
		while (i <= mid && j <= right) {
			if (a[i] < a[j]) {
				tmp[k] = a[i];
				i++;
			} else {
				tmp[k] = a[j];
				j++;
			}
			k++;
		}

		// copy rest of left array
		while (i <= mid) {
			tmp[k] = a[i];
			i++;
			k++;
		}
		// copy rest of left array
		while (j <= right) {
			tmp[k] = a[j];
			j++;
			k++;
		}
		// copy back to orginal array
		for (k = 0; k < tmp.length; k++)
			a[left + k] = tmp[k];

	}

	/*
	 * Write a program to return list of words (from the given list of words)
	 * that can be formed from the list of given characters. This is like
	 * Scribble game. Say for example the given characters are ('a' , 'c' , 't'
	 * } and list the words are {'cat', 'act', 'ac', 'stop' , 'cac'}. The
	 * program should return only the words that can be formed from given
	 * letters. Here the output should be {'cat', 'act', 'ac'}.
	 */
	public void scribbleWord() {

		String key = "atcc";
		ArrayList<String> str_al = new ArrayList<String>();
		str_al.add("cat");
		str_al.add("act");
		str_al.add("ac");
		str_al.add("stop");
		str_al.add("ca");

		char[] key_char_ary = key.toCharArray();
		int key_ary[] = new int[256];
		int tmp_key_ary[] = new int[256];
		int j = 0;

		// init key
		for (j = 0; j < key_char_ary.length; j++) {
			key_ary[key_char_ary[j]]++;
			tmp_key_ary[key_char_ary[j]]++;
		}

		// loop thru list
		for (int i = 0; i < str_al.size(); i++) {
			char tmp[] = str_al.get(i).toCharArray();
			// check each word in list
			for (j = 0; j < tmp.length; j++) {
				if (tmp_key_ary[tmp[j]] > 0)
					tmp_key_ary[tmp[j]]--;
				else
					break;
			}
			if (j == tmp.length)
				System.out.println(Arrays.toString(tmp));

			// init key
			for (j = 0; j < key_ary.length; j++) {
				if (key_ary[j] != 0)
					tmp_key_ary[j] = key_ary[j];
			}

			// for(j=0; j<key_ary.length;j++)
			// if(tmp_key_ary[j]!=0)
			// System.out.println("char = " + j + " val = " + tmp_key_ary[j]);

		}

	}

	/*
	 * google-interview-questions Given a sorted array of integers, write a
	 * function that will return the number with the biggest number of
	 * repetitions. (Asked to refine the solution to be more efficient)
	 */
	public void findMostRep() {
		
		int a[] ={1, 1, 2, 3, 3, 3, 3 };
		
		System.out.println("findMostRep = " + Arrays.toString(a));
	
		int mid = a.length/2;
		int i,j, right, left;
		i =  mid;
		j =  mid+1;
		left = 0;
		right = a.length-1;
		int max_rep_count = 1;
		
		System.out.println(" mid = " + mid);

		// len is odd
		if(a.length % 2 != 0) {
			if(a[i-1] == a[i]) {
				max_rep_count++;
			}
			i--;
		}else {
			i = mid-1;
			j = mid;
		}

		// check mid
		while(i >= 0 && j <= right) {
			if(a[i] == a[j]) {
				max_rep_count++;
				System.out.println("i = " + i + " val = " + a[i]);
				System.out.println("j = " + j + " val = " + a[j]);
				System.out.println("max rep = " + max_rep_count);
				i--;
				j++;
					
			}else break;
		}
		int left_count = 1;
		int left_max_count = 1;

		int right_count = 1;
		int right_max_count = 1;

		while(i > left && j < right) {
//			System.out.println("a[i] = " + a[i] + " a[i-1] = " + a[i-1] + " i = "  + i);
			if(a[i] == a[i-1]) {
				left_count++;
				if(left_max_count < left_count) {
					left_max_count = left_count;
				}
				
			} else {
				left_count = 1;
			}
			i--;
			
	
			if(a[j] == a[j+1]) {
				right_count++;
				if(right_max_count < right_count) {
					right_max_count = right_count;
				}
				
			} else {
				right_count = 1;
			}
			j++;
	
			
		}
		
		System.out.println("left max count = " + left_max_count);
		System.out.println("right max count = " + right_max_count);
	
	}

	public void do_karmarkarKarpPartition() {
		int a[] = {2,2,2,2,2};
		
		System.out.println("Karp Parition = " + this.karmarkarKarpPartition(a));
	}
	// not correct
	private int karmarkarKarpPartition( int[] baseArr ){       
	    // create max heap  
	    PriorityQueue<Integer> heap = new PriorityQueue<Integer>(baseArr.length, Collections.reverseOrder());  
	    for( int value : baseArr ){            
	        heap.add( value );      
	    }
	    
	    for(Integer e : heap) {
	    	System.out.println("heap val = " + e.toString());
	    }
	    
	    while( heap.size() > 1 ){
	        int val1 = heap.poll(); 
	        int val2 = heap.poll(); 
	        heap.add( val1 - val2 );
	    }
	    return heap.poll();
	}
	/*
	 * what is time complexity of concatenating two int in java example :-
	*  int a=18965; int b=78521369741;
	*  after concatenation i want ans in primitive integer data types like,
	*  int c=1896578521369741;
	*  i want to know what is the fastest way to do this and what will be the time complexity ?
	 */
	public void do_concatIntegers() {
		long a = 1234;
		long b = 5678;
		this.concatIntegers(a, b);
	}
	
	private void concatIntegers(long a, long b) {
		long tmp = a;
		while(tmp>0) {
			tmp /= 10;
			a *= 10;
		}
		long c = a + b;
		System.out.println (" a = " + String.valueOf(a) + " b = " + String.valueOf(b) + " concat = " + String.valueOf(c));
	}
	// fibonacci num
	public void do_fibLoop() {
		int n = 2;
		for(int i = 0; i < 17; i++)
		System.out.println("fib num " + i + " = " + String.valueOf(this.fibLoop(i)));
	}
	
	private long fibLoop(int n){
		
		if (n<=0){return 0;}
		long prev = 0;
		long cur = 1;
		long next = 0;
		for (int i=2; i<=n; i++){
			next = cur + prev;
			prev = cur;
			cur = next;
		}
		
		return cur;
	}
	/*
	 * Consider in Java arraylist we have mix of int, double, float, string, etc. 
	 * How will you find if a given index of arraylist have string. 
	 * No need to worry about generics and type safe.
	 */
	public void do_objType() {
	
		ArrayList<Object> al = new ArrayList<Object>();
		Integer a = 1;
		Float b = (float) 2.1;
		String c = "test";
		
		al.add(a);
		al.add(b);
		al.add(c);
		
		for(int i=0; i< al.size(); i++) {
			if(al.get(i) instanceof String ) System.out.println("String ");
			else if(al.get(i) instanceof Integer) System.out.println("Integer");
			else if(al.get(i) instanceof Float) System.out.println("Float");
		}
	}
	/*
	 * Given a number N, now find the smallest number K such that product 
	 * of digits of K is equal to N. If there is no such K then return -1.
	 * Suppose N = 100, then K = 455 N=26, K = -1
	 */
	public void do_AmzQ1() {
		int n = 13;
		PriorityQueue<Integer> digits = new PriorityQueue<Integer>();
		for(int factor = 9; factor > 1; factor--) {
			while(n % factor == 0) {
				n /= factor;
				digits.add(factor);
			}
		}
		if(n != 1) System.out.println("no sol -1");
		else {
			
			while(!digits.isEmpty()) {
				System.out.println(digits.poll());
			}
		}
	}
	/* Given an unordered array of positive integers, 
	 * create an algorithm that makes sure no group of integers of 
	 * size bigger than M have the same integers.
	 * Input: 2,1,1,1,3,4,4,4,5 M = 2
	 * Output: 2,1,1,3,1,4,4,5,4
	 */
	public void do_groupNum1() {
//		int a[] = {2,1,1,1,3,4,4,4,5}; //21134454
//		int a[] = {1,1,1,1,2,2,2}; // 1121122
		int a[] = {4,1,1,1,4,1,1,1};
		int m = 2;
		int count = 0;
		int j;
		int pre = 0;
		int cur = 1;

		while(cur < a.length) {

			if(a[pre] == a[cur]) count++;
			else count=0;
			if(m==count) {
				for(j=cur+1; j<a.length; j++) {
					if(a[cur] != a[j]) {
						//swap
						int tmp = a[cur];
						a[cur] = a[j];
						a[j] = tmp;
						count = 0;
						break;
					}
				}

				if(j>=a.length) {
					System.out.println("no solution");
					break;
				}

			}
			System.out.print(" " + a[pre]);

			pre++;
			cur++;
			
		}
		
		if(m > count) System.out.print(" " + a[a.length-1]);
	
	}
	/*
	 * Count the number of positive integers less than N that does not contains digit 4.
	 */
	public void do_getCountForNum() {
		int x = 50;
		this.getCountforNum(x);
	}
	
	private void getCountforNum(int num) {
	
		String s;
		int count=0;
		for(int i=1;i<=num;i++) {
			s = String.valueOf(i);
			if(!s.contains("4")) {
				count++;
			}
		}
		System.out.println("count = " + count);
	}
	
	/*
	 * Write a program for finding a minimum element in rotated sorted array(either ascending or descending ) 
	 * and array contains duplicates.
	 */
	public void do_dupRotAry() {
//         int[] arr = { 4, 4, 5, 5, 5, 6, 1, 1, 2, 2, 3, 3, 3 };
//        int[] arr = { 3, 2, 1, 6, 5, 4,4 };
//		int []arr = {4, 5, 8, 9, 11, -1, 0, 3, 2};
		int arr[] =  { 5, 1, 2, 3, 4, 5}; //fail, cc code.
        int l = 0;
        int r = arr.length - 1;
        while (arr[l] > arr[r]) {
            int m = l + (r - l) / 2;
            if (arr[m] > arr[r]) {
                l = m + 1;
            } else {
                r = m;
            }
        }
        int minAcs = arr[l];
        l = 0;
        r = arr.length - 1;
        while (arr[r] > arr[l]) {
            int m = (l + (r - l) / 2) + 1;
            if (arr[m] > arr[l]) {
                r = m - 1;
            } else {
                l = m;
            }
        }
        System.out.println(" ary = " + Arrays.toString(arr) + " dup value = " + Math.min(minAcs, arr[r]));
	}
	
	
	public void do_findMinRotDupAry() {
		int a[] = {5, 6, 8, 9, 11, -1, 0, 2, 3};
//		int a[] = { 3, 1, 2, 2, 2};
//		int a[] = { 3, 2, 0, 6, 5, 4, 4};
//		int a[] = { -1, 0, 1, 2, 3,4, 4};
		int x = a[0];
		for(int i=1; i<a.length; i++) {
			x = Math.min(x,a[i]);
		}
		System.out.println(" a[] " + Arrays.toString(a) + " min dup cir ary = " + x);
	}
	// do radix and selection sort
	private void radixSort(int arr[], int maxDigits){
		int exp = 1;//10^0;
		for(int i =0; i < maxDigits; i++){
			ArrayList<Integer> bucketList[] = new ArrayList[10];
			for(int k=0; k < 10; k++){
				bucketList[k] = new ArrayList<Integer>();
			}
			for(int j =0; j < arr.length; j++){
				int number = (arr[j]/exp)%10;
				bucketList[number].add(arr[j]);
			}
			exp *= 10;
			int index =0;
			for(int k=0; k < 10; k++){
				for(int num: bucketList[k]){
					arr[index] = num;
					index++;
				}
			}
		}

	}
	
	public void do_radixSort() {
		int a[] = {12,1,4,2,3,5,10,8,11,9,12,12};

		//		this.radixSort(a, 2);
		this.radxSort(a, 3);
		System.out.println("Radix Sort : " + Arrays.toString(a));
	}
	/*
	 * Get the each radix digit and store into array[rdx] queue
	 * copy array[rdx] back into original array a
	 * repeat from left to right significant values in array a.
	 * O(n) runtime.
	 */
	private void radxSort(int a[], int radix_size) {
	
		Queue<Integer> q[] = new LinkedList[10];
		int j=0;
		int div = 1; // 10^0..10^radix_size
		int rdx=0;
		for(int i=0; i<q.length; i++) {
			q[i] = new LinkedList<Integer>();
		}
		
		while(j < radix_size) {
			for(int i=0; i<a.length; i++) {
				// get radix
				rdx = a[i] / div;
				rdx = rdx % 10;
				q[rdx].add(a[i]); // store into arrays of queues
			}
			
			int k=0;
			for(Queue<Integer> x:q) {
				while(!x.isEmpty()) {
					a[k++] = x.poll();
				}
			}
			div *= 10;
			j++;
		}
	}
	/*
	 * Given an array of (unsorted) integers, arrange them such that a < b > c < d > e... etc.
	 */
	public void do_swap_alternative() {
//		int a[] = { 5, 3, 2, 4};
		int a[] = { 1,3,3,4};
		this.swap_alt(a);
	}
	// swap i and i+1, i=2,..i+=2..,len-1
	private void swap_alt(int a[]) {
		
		Arrays.sort(a);
		int tmp=0;
		for(int i=2; i<a.length; i=i+2) {
			tmp = a[i];
			a[i] = a[i-1];
			a[i-1] = tmp;
		}
		
		System.out.println(" a = " + Arrays.toString(a));
	}
	/*
	 * Give a N*N matrix, print it out diagonally.
		Follow up, if it is a M*N matrix, how to print it out.
		Example:
		1 2 3
		4 5 6
		7 8 9
		print:
		1
		2 4
		3 5 7
		6 8
		9

	 */
	public void do_prt_diag() {
		
		int a[][] = { {1,2,3}, {4,5,6}, {7,8,9} };
//		int a[][] = { {1,2,3,4,22}, {5,6,7,8,33}, {9,10,11,12,44} };
		
		int j = 0;
		int k = 0;
		printDiagonally(a);
	}
	// start row then column
	// start at a[y][x], where y=0..n-1 and x=0...m-1 go down and left till out of bound.
	// start at a[y][x], where y=1..n-1 and x=m-1..0 go down and left till out of bound.
	private void printDiagonally(int[][] a) {
		int n = a.length; // col
		int m = a[0].length; // row
		int x = 0;
		int y = 0;
		
		// do row
		for (int i = 0; i<m; i++) {
			x=i;
			while(x >= 0 && y <= n-1) {
				System.out.print(a[y][x] + " ");
				x--;
				y++;
			}
			y=0;  //reset column
			System.out.println();
		}
		//do column
		x=m-1;
		for(int i=1; i<n; i++) {
			y=i;
			while(x>=0 && y<=n-1) {
				System.out.print(a[y][x] + " ");
				y++;
				x--;
			}
			x=m-1; //rest row
			System.out.println();
		}
	}
	//heap sort
	private void heapify(int a[], int idx, int len) {
		
		int p       = idx;
		int left    = 2*idx + 1;
		int right   = 2*idx + 2;
		int largest = p;
		
		if(left <= len && a[p] < a[left]) largest = left;
		else largest = p;
		
		if(right <= len && a[largest] < a[right]) largest = right;
		
		if(largest != p) {
			int tmp = a[p];
			a[p] = a[largest];
			a[largest] = tmp;
//			System.out.println("array heapify : " + Arrays.toString(a));
			this.heapify(a, largest, len);
//			System.out.println("debug heapify");
		}
		
	}
	// siftdown heapify
	private void siftDown(int a[], int start, int end) {
		 
		int r = start;

		// at least 1 child
		while(2*r+1  <= end) {
			int child = 2*r+1; // left child
			if(child+1 <= end && a[child] < a[child+1]) child += 1; // has right child and bigger
			//swap
			if(a[r] < a[child]) {
				int x = a[r];
				a[r] = a[child];
				a[child]=x;
				r = child; //update root to child
			}else break;
		}
	}
	
	// build heap from unsort array
	// bottom up, thus root(idx =0) is largest
	// start = count-2/2, last parent of node
	private void buildHeap(int a[]) {

		int start = (a.length-2)/2;
		while(start >=0) {
			this.siftDown(a, start, a.length-1);
			start--;
		}
	}
	
	// heapsort
	public void do_heapSort(int a[]) {
 
		this.buildHeap(a);
		for(int i=a.length-1; i>=0; i--) {
			int x = a[i];
			a[i] = a[0];
			a[0] = x;
			this.siftDown(a, 0, i-1);
		}
	}
	
	private int qs_pivot(int a[], int left, int right) {


		return left;
	}
	
	private void qs(int a[], int left, int right) {
		
		if(left < right) {
 
		}

	}
	
	private void mergeSS(int a[], int left, int mid, int right) {
		
		int tmp[] = new int[right-left+1];// tmp ary len
		int l = left;
//		int r = right;
		int m = mid+1;
		int idx = 0;
		while(l <= mid &&  m <= right ) {
			if(a[l] <= a[m]) tmp[idx++] = a[l++];
			else tmp[idx++] = a[m++];
		}
		// copy remain left to tmp
		while(l <= mid) tmp[idx++] = a[l++];
		// copy remain right to tmp
		while(m <= right) tmp[idx++] = a[m++];
	 
		// copy back to array a
		for(int i=0; i<idx; i++) {
			a[left] = tmp[i];
			left++;
		}
	}
	
	public void do_mergeSS(int a[], int left, int right) {
 
		if(left < right) {
			int mid = (right+left)/2;
			this.do_mergeSS(a, left, mid);
			this.do_mergeSS(a, mid+1, right);
			this.mergeSS(a, left, mid, right); // med+1***why?
		}
	}
	
	
	// print bfs or level treee
	public void printLevelTree(Node n) {
				
	}
	
	private  int coinCB(int a[], int amt, int idx) {
		
		int total = 0;
		return total;
	}
	
	public void initCoinCB() {
	
		int a[] = {1,2};
		int amt = 5;
		int total = this.coinCB(a, amt, 0);
		System.out.println(" total coin combo : " + total);
	}
	
	public boolean isPal(char[] a, int b, int c) {


		if(c<b) return true;
		if(a[b]!= a[c]) return false;		
		return isPal(a, b+1, c-1);
	}
	
	public boolean subString(String s1, String sub) {

		return false;
	}
	
	private boolean subStr(char []str, char []letter) {
		
		Arrays.sort(str);
		Arrays.sort(letter);
		
		int idx = 0;
		for(char c: letter) {
			if(str[idx] == c) idx++;
			if(idx == str.length) return true;
		}
		return false;
	}
	
	private boolean subStr(char str[], char letter[], int idx, int i ) {

		return false;
	}
	
	public void do_scribble() {
	
//		String s1[] = {"cat", "dog", "tag", "test"};
		String s1[] = {"cat", "act", "ac", "stop" , "cac"};
//		String letter ="abcedfgtsot";
		String letter = "act";
	
//		for(String s : s1) {
//			if(this.subStr(s.toCharArray(), letter.toCharArray()))
//				System.out.println("scribble : " + s);
//		}
		
		for(String s : s1) {
			if(this.subStr(s.toCharArray(), letter.toCharArray(), 0, 0))
				System.out.println("scribble : " + s);
		}
	}
	
	private void maxRepeat(int a[]) {
		
		int cur = 0, next = cur+1, max = 0, count=1;
		int max_rep = 0;
		
		Arrays.sort(a);
		while(next < a.length) {
			
			// special case last element is the same as prev
//			if(next == a.length-1 && a[cur] == a[next]) count++;
			
			if(a[cur]==a[next]) count++;
			if(  max < count || next == a.length-1) {
				System.out.println(a[next] + " count : " + count + " ary : " + Arrays.toString(a));
				max_rep = a[cur];
				max = count;
				count=1;
			}
			cur++;
			next=cur+1;
			
		}
		
		System.out.println(" max : " + max + " max rep value : " + max_rep);
		
	}
	
	private void maxRepRecursive(int a[], int val, int cur, int max, int cnt) {
	
		int next = cur+1;

		if(cur==0) Arrays.sort(a);
		
		if(next < a.length) {
			
			if(a[cur] == a[next]) cnt++;
			else if(max < cnt) {
				max = cnt;
				cnt =1;
				val = a[cur];
			}
		} 
		else if(next == a.length &&
				  a[cur] == a[cur-1] &&
				  max < cnt) {
			max = cnt;
			cnt = 1;
			val = a[cur];
		}
		if(next == a.length) {
			System.out.println(Arrays.toString(a));
			System.out.println(" max : " + max + " val : " + val);
			return;
		}
		this.maxRepRecursive(a, val, cur+1, max, cnt);
	}
	
	private void perm(int a[], int k, int n) {
		if(k==n) {
//			for(int j=0;j<=n;j++) System.out.print(a[j]);
			System.out.print(Arrays.toString(a));
			System.out.println("");
		}else {
			for(int i=k; i<=n; i++) {
				int x =a[k]; a[k]=a[i]; a[i]= x; // swap itself on first time.
				perm(a,k+1, n); 
			    x =a[k]; a[k] =a[i]; a[i]= x; // hit base case and swap back 
			}
		}
	}

	public int fib_recursive(int n ){
	
		if(0==n) return 0;
		if(1==n) return 1;
		return fib_recursive(n-1) + fib_recursive(n-2);
	}
	
	public void do_fib_recursive() {
	
		int x = fib_recursive(7);
		System.out.println(" fib recursive = " + x );
	}
	
	public void do_daily_test() {
		
//		int a[] = {1,3,2,19,5,4,10,11,11,19,19,1,1,1,1};
//		char a[] = {1,2,3,2,1};
		int b[] = new int[20];
		
		Random rand = new Random();
		
		for(int i=0;i<20;i++) {
			b[i] = rand.nextInt(100);
		}
		int c[] = Arrays.copyOf(b, b.length);
		
//		test_addNode();
//		test_BST_Tree();
		
//		this.qs(b, 0, b.length-1);
//		this.do_mergeSS(b, 0, b.length-1);
//		this.do_heapSort(b);

//		System.out.println("isPal = " + this.isPal(a, 0, a.length-1));
		
//		Arrays.sort(c);
//		System.out.println(Arrays.equals(b, c));
// 
//		System.out.println(" b ary " + Arrays.toString(b));
//		System.out.println(" c ary " + Arrays.toString(c));
		
//		this.initCoinCB();
		
//		this.do_scribble();
//		this.maxRepeat(a);
//		this.maxRepRecursive(a, -1, 0, 0, 1);
//		int a[] = {1,2,3};
//		this.perm(a, 0, a.length-1 );
		
	}

 	public void run_it() {
 		
		int r = getRoot();
		Node build_tree = buildTree(r);
		 printTree(build_tree);
		System.out.println("\nprintBFSTree");
		printBFSTree(build_tree);
		System.out.println("\nprintLevelTree");
		printLevelTree(build_tree);
		// System.out.println("\nprintDFSTree");
		// printDFSTree(build_tree);
		System.out.println("\nprintSubTreeWeight");
		printSubTreeWeight(build_tree);
//		String s = "ascbdbca";
//		System.out.println("\npali =  " + this.isPali(s, s.length()));
		minSum();
		initLinkList();
		isSubString();
		// isStrPattern();
//		this.isIntgerPal();
//		this.intToOct();
		this.initGetCoinCombinations();
		this.kthMin();
//		this.saddleback();
//		 this.do_qsort();
//		 this.do_mergeSort();
//		this.scribbleWord();
		this.findMostRep();
//		this.do_karmarkarKarpPartition(); // don't work
//		this.do_numParition();
//		this.do_concatIntegers();
//		this.do_fibLoop();
//		this.do_objType();
//		this.do_AmzQ1();
//		this.do_groupNum();
//		this.do_groupNum1();
//		this.do_getCountForNum();
//		this.do_dupRotAry();
//		this.do_findMinRotDupAry();
//		this.do_radixSort();
//		this.do_swap_alternative();
//		this.do_prt_diag();
		
//		this.do_daily_test();
		this.do_fib_recursive();
	}

}

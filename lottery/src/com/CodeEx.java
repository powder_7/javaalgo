package com;

import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.SortedSet;
import java.util.Stack;
import java.util.TreeSet;
import java.util.logging.Logger;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.ws.http.HTTPException;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.http.HttpParameters;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.params.SyncBasicHttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONObject;


public class CodeEx {

	CodeEx() {

	}

	// given a sequence of strings, like A, B, C, D, A,
	// in which A = "abcde", B = "hello world", C = "NBA", D = "CNN", A =
	// "abcde"
	// to remove duplicates of strings. The output is like A, B, C, D.
	// String input[][] = {{"A", "abcde"}, {"B", "hello world"}, {"C", "NBA"},
	// {"D", "CNN"}, {"A", "abcde"} };
	public void removeDup(String[][] input) {

		Hashtable<String,String> ht = new Hashtable<String,String>();
		HashMap<String,String> hm = new HashMap<String,String>();
		for (String[] s : input) {
			ht.put(s[0], s[1]);
			hm.put(s[0], s[1]);
			System.out.println("key:" + s[0] + " val :" + s[1]);
		}

		System.out.println(ht);
		System.out.println(hm);
	}

	public void sortAry(String[] input) {

		System.out.println(Arrays.toString(input));
		// Arrays.sort(input);
		TreeSet<String> ts = new TreeSet<String>();

		for (String s : input) {
			// System.out.println(s);
			ts.add(s);
		}

		System.out.println(ts);
	}

	private int[] originalArray;
	private int[] sortedArray;

	public void mergeSort(int[] array) {
		this.originalArray = array;
		this.sortedArray = new int[array.length];
		divide(0, array.length - 1);
	}

	private void divide(int low, int high) {
		// Check if low is smaller then high, if not then the array is sorted
		if (low < high) {
			// Get the index of the element which is in the middle
			int middle = (low + high) / 2;
			// Sort the left side of the array
			divide(low, middle);
			// Sort the right side of the array
			divide(middle + 1, high);
			// Combine them both
			combine(low, middle, high);
		}
	}

	private void combine(int low, int middle, int high) {

		for (int i = low; i <= high; i++) {
			sortedArray[i] = originalArray[i];
		}
		int i = low;
		int j = middle + 1;
		int k = low;
		// Copy the smallest values from either the left or the right side back
		// to the original array
		while (i <= middle && j <= high) {
			if (sortedArray[i] <= sortedArray[j]) {
				originalArray[k] = sortedArray[i];
				i++;
			} else {
				originalArray[k] = sortedArray[j];
				j++;
			}
			k++;
		}
		// Copy the rest of the left side of the array into the target array
		while (i <= middle) {
			originalArray[k] = sortedArray[i];
			k++;
			i++;
		}
	}

	public void quicksort(int[] a, int left, int right) {

		int idx = partition(a, left, right);

		// left sub array
		if (left < idx - 1)
			quicksort(a, left, idx - 1);
		// right sub array
		if (idx < right)
			quicksort(a, idx, right);
	}

	private int partition(int[] a, int left, int right) {

		int pivot = a[(left + right) / 2];
		// int pivot = a[right];
		int lt = left;
		int rt = right;
		int tmp = 0;

		while (lt <= rt) {
			while (a[lt] < pivot)
				lt++;
			while (a[rt] > pivot)
				rt--;
			// swap idx val
			if (lt <= rt) {
				tmp = a[lt];
				a[lt] = a[rt];
				a[rt] = tmp;
				lt++;
				rt--;
			}

		}
		return lt;
	}

	public void mergeSort(int[] a, int low, int high) {
		if (low < high) {
			int mid = (low + high) / 2;
			// left sub array
			mergeSort(a, low, mid);
			// right sub array
			mergeSort(a, mid + 1, high);
			// merge 2 sub arrays
			merge(a, low, mid, high);
		}
	}
	private void merge(int[] a, int low, int mid, int high) {

		int left = low;
		int right = mid + 1;
		int k = 0;
		int[] temp = new int[high - low + 1];

		// sort and merge 2 sub arrays
		while (left <= mid && right <= high) {
			// left sub array val is smaller
			if (a[left] <= a[right]) {
				temp[k] = a[left];
				left++;
				// right sub array val is smaller
			} else {
				temp[k] = a[right];
				right++;
			}
			k++;
		}
		// copy rest the left sub array
		while (left <= mid) {
			temp[k] = a[left];
			left++;
			k++;
		}
		// copy rest the right sub array
		while (right <= high) {
			temp[k] = a[right];
			right++;
			k++;
		}
		// copy merge array back to orignal passed array
		for (k = 0; k < temp.length; k++) {
			a[low + k] = temp[k];
		}
	}

	// Finding the max repeated element in an array
	public void findMaxRepeatElt(int[] a) {

		HashMap<Integer, Integer> hm = new HashMap<Integer, Integer>();

		int[] tmp = new int[a.length];
		int count = 0;
		for (int i = 0; i < a.length; i++) {

			if (hm.containsKey(a[i])) {
				count = (Integer) hm.get(a[i]);
				count++;
				hm.put(a[i], count);
			} else {
				hm.put(a[i], 1);
			}
		}

		int num_of_rep = 0;
		int max_rep_val = 0;
		for (Entry<Integer, Integer> entery_set : hm.entrySet()) {
			if (entery_set.getValue() > num_of_rep) {
				// total repeat
				num_of_rep = entery_set.getValue();
				// value of repeat or index
				max_rep_val = entery_set.getKey();
			}

		}

		System.out.println(hm);
		System.out.println("max rep val = " + max_rep_val);

	}

	public void powerset() {
		String st[] = { "x", "y" };
		LinkedHashSet hashSet = new LinkedHashSet();
		int len = st.length;
		int elements = (int) Math.pow(2, len);
		for (int i = 0; i < elements; i++) {
			String str = Integer.toBinaryString(i);
			int value = str.length();
			String pset = str;
			for (int k = value; k < len; k++) {
				pset = "0" + pset;
			}
			LinkedHashSet set = new LinkedHashSet();
			for (int j = 0; j < pset.length(); j++) {
				if (pset.charAt(j) == '1')
					set.add(st[j]);
			}
			hashSet.add(set);
		}
		System.out.println(hashSet.toString().replace("[", "{")
				.replace("]", "}"));

	}

	public void RangeAdd(int[] a) {
		// int [] z = {9,11};
		int[][] set = { { 1, 4 }, { 6, 7 }, { 10, 12 } };
		HashSet hs = new HashSet();
		hs.add(set[0]);
		hs.add(set[1]);
		hs.add(set[2]);

		int[] temp = { -1, -1 };
		int min = 0;
		int max = 0;
		Iterator it = hs.iterator();
		while (it.hasNext()) {
			int[] b = (int[]) it.next();
			// check if min of add within range of set or
			// set is within range added
			if ((b[0] <= a[1] && b[0] >= a[0])
					|| (a[0] <= b[1] && a[0] >= b[0])) {
				it.remove();
				min = Math.min(b[0], a[0]);
				max = Math.max(b[1], a[1]);
				System.out.println("min :" + min + " max : " + max);
				temp[0] = min;
				temp[1] = max;
				hs.add(temp);
				break;
			}

		}
		if (temp[0] == -1)
			hs.add(a);
		it = hs.iterator();
		while (it.hasNext()) {
			int[] b = (int[]) it.next();
			System.out.print("{" + b[0] + ", " + b[1] + " }");
		}
	}

	static class Node {
		Node left;
		Node right;
		int x;

		public Node(int a) {
			this.x = a;
		}
	}

	public void testTree() {

		// Node root = new Node(5);
		// insertTree(root, 1);
		// insertTree(root, 8);
		// insertTree(root, 6);
		// insertTree(root, 3);
		// insertTree(root, 9);
		// insertTree(root, 2);

		Node root = new Node(8);
		insertTree(root, 33);
		insertTree(root, 111);
		insertTree(root, 6);
		insertTree(root, 15);
		insertTree(root, 14);
		insertTree(root, 17);
		insertTree(root, 110);
		// insertTree(root, 140);
		// insertTree(root, 13);

		Node n = findLCA(root, 14, 110);

		System.out.println("second largest val : " + findTreeSecLargest(root));
		System.out.println("sec largest!!! : " + secLarg[1]);
		System.out.println("tree height : " + this.findTreeHeight(root));
		System.out.println("LCA : " + n.x);
		
        BFS(root);
	}

	int xtmp = 0;
	Stack<Node> stk = new Stack<Node>();
	HashSet hs = new HashSet();
	Stack<Node> tmp_stk = new Stack<Node>();

	public int findTreeHeight(Node n) {
		if (n == null)
			return 0;
		int left = findTreeHeight(n.left);
		int right = findTreeHeight(n.right);

		return 1 + Math.max(left, right);
	}

	public Stack findTreeVal(Node node, int x) {

		stk.push(node);

		if (node.x == x) {
			for (Node s : stk) {
				System.out.println("x : " + s.x);
				hs.add(s);
				tmp_stk.add(s);
			}

			return stk;
		}

		// if(node.x == y) {
		//
		// for(Node s: stk) {
		// System.out.println("y : " +s.x);
		// if(hm.containsKey(s.x))
		// System.out.println
		// }
		// return;
		//
		// }

		if (node.left != null) {
			findTreeVal(node.left, x);
		}
		if (node.right != null) {
			findTreeVal(node.right, x);
		}

		// stk.pop();
		return stk;
	}

	int[] secLarg = { 0, 0 };

	public int findTreeSecLargest(Node node) {

		// int count = 0;
		if (node != null) {

			findTreeSecLargest(node.right);
			System.out.print(" [" + node.x + "] ");
			secLarg[0] = Math.max(secLarg[0], node.x);
			if (secLarg[0] > node.x) {
				secLarg[1] = Math.max(secLarg[1], node.x);
				// System.out.print(" sl[1] : " + secLarg[1] );
			}
			findTreeSecLargest(node.left);

		}

		return secLarg[1];
	}

	public void insertTree(Node node, int x) {

		if (x < node.x) {
			if (node.left != null) {
				// keep going left of tree
				insertTree(node.left, x);
			} else {
				node.left = new Node(x);
			}
		} else if (x > node.x) {
			if (node.right != null) {
				// keep goin right of tree
				insertTree(node.right, x);
			} else {
				node.right = new Node(x);
			}
		}

	}

	public void printTree(Node node) {

		if (node != null) {

			printTree(node.right);
			System.out.print(" [" + node.x + "] ");
			printTree(node.left);

		}
	}

	// max subarray
	public int findMaxConsecutive(int[] arr) {
		int temp = 0;
		int maxmax = 0;
		int start_idx = 0, end_idx = 0;
		for (int i = 0; i < arr.length; i++) {
			// reset temp if it's less than 0
			if (temp < 0) {
				temp = arr[i];
				start_idx = i;
			} else {
				temp += arr[i];
			}
			if (temp >= maxmax) {
				maxmax = temp;
				end_idx = i;
			}
		}
		System.out
				.println("start idx : " + start_idx + " end idx : " + end_idx);
		return maxmax;
	}

	public int maxLose(int[] a) {
		int tmp = 0, max = 0;
		for (int i = 0; i < a.length; i++) {
			tmp = Math.max(tmp, a[i]); // find max num in array
			max = Math.max(max, tmp - a[i]); // different of max num in array
												// and store it.
		}

		return max;
	}

	public int maxGain(int[] a) {
		int tmp = a[0], max = 0;

		for (int i = 0; i < a.length; i++) {
			tmp = Math.min(tmp, a[i]);
			max = Math.max(max, a[i] - tmp);
		}

		return max;
	}

	//if larger than go right
	//if smaller go left
	//else they diverge return root
	public Node findLCA(Node root, int x, int y) {

		if (root == null) return null;

		if (x > root.x && y > root.x)
			return findLCA(root.right, x, y); // return else root in stack
		if (x < root.x && y < root.x)
			return findLCA(root.left, x, y); // return else root in stack
		else
			return root;

		// if (root == null)
		// return null;
		// while (root != null) {
		// if (x > root.x && y > root.x)
		// root = root.right;
		// if (x < root.x && y < root.x)
		// root = root.left;
		// else
		// return root;
		// }
		// return null;

	}
	
	public void BFS(Node root) {
		 
		if(root == null)
			return;
	
		Node tmp;
		Queue q = new LinkedList<Integer>();
		
		q.add(root);

		while(!q.isEmpty()) {
			
			tmp = (Node) q.remove();
			System.out.println(tmp.x);
			if(tmp.left!=null)
				q.add(tmp.left);
			if(tmp.right!=null)
				q.add(tmp.right);
		}
	}
	
	
	public String encode(String value) 
	{
        String encoded = null;
        try {
            encoded = URLEncoder.encode(value, "UTF-8");
        } catch (UnsupportedEncodingException ignore) {
        }
        StringBuilder buf = new StringBuilder(encoded.length());
        char focus;
        for (int i = 0; i < encoded.length(); i++) {
            focus = encoded.charAt(i);
            if (focus == '*') {
                buf.append("%2A");
            } else if (focus == '+') {
                buf.append("%20");
            } else if (focus == '%' && (i + 1) < encoded.length()
                    && encoded.charAt(i + 1) == '7' && encoded.charAt(i + 2) == 'E') {
                buf.append('~');
                i += 2;
            } else {
                buf.append(focus);
            }
        }
        return buf.toString();
    }
	
	private static String computeSignature(String baseString, String keyString) throws GeneralSecurityException, UnsupportedEncodingException 
	{
	    SecretKey secretKey = null;

	    byte[] keyBytes = keyString.getBytes();
	    secretKey = new SecretKeySpec(keyBytes, "HmacSHA1");

	    Mac mac = Mac.getInstance("HmacSHA1");
	    mac.init(secretKey);

	    byte[] text = baseString.getBytes();

	    return new String(Base64.encodeBase64(mac.doFinal(text))).trim();
	}
    
    private OAuthConsumer mConsumer = null;
    private String mToken;
    private String mSecret;
    private HttpClient mClient;
    public static final String TWITTER_CONSUMER_KEY = "W55R5fE81cp5CXfgDkuX6w";
    public static final String TWITTER_CONSUMER_SECRET = "XgHf9UaNybRLoZID8lKiDWLuJUBgXgPBAy7kIFWfmI";
    
    public static final String USER_TOKEN = "562384524-iutT497qIlBI5CSkba3q3UcenzTSI1SwVZrDi1bD";
    public static final String USER_SECRET = "pbHTOs9lLzSKMSff9zy2Uu5qgkt4Dl4lfguLRyil8";
	
    private static final String BASE_URL = "https://api.twitter.com/1.1";

    protected static final String EXTENSION = ".json";
    protected static final String STATUSES_FRIENDS_TIMELINE_URL = BASE_URL
        + "/statuses/home_timeline" + EXTENSION;
    protected static final String STATUSES_HOME_TIMELINE_URL = BASE_URL
        + "/statuses/home_timeline" + EXTENSION;
    protected static final String STATUSES_MENTIONS_TIMELINE_URL = BASE_URL + "/statuses/mentions_timeline"
            + EXTENSION;
    protected static final String STATUSES_UPDATE_URL = BASE_URL + "/statuses/update" + EXTENSION;
    protected static final String STATUSES_DESTROY_URL = BASE_URL + "/statuses/destroy/";
    protected static final String DIRECT_MESSAGES_URL = BASE_URL + "/direct_messages" + EXTENSION;

    protected static final String ACCOUNT_VERIFY_CREDENTIALS_URL = BASE_URL
            + "/account/verify_credentials" + EXTENSION;

    protected static final String ACCOUNT_RATE_LIMIT_STATUS_URL = BASE_URL
            + "/application/rate_limit_status" + EXTENSION;
    protected static final String FAVORITES_CREATE_BASE_URL = BASE_URL + "/favorites/create" + EXTENSION;
    protected static final String FAVORITES_DESTROY_BASE_URL = BASE_URL + "/favorites/destroy" + EXTENSION;
    
	public void setConnection() throws ConnectException {

		
        HttpParams parameters = new BasicHttpParams();
        HttpProtocolParams.setVersion(parameters, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(parameters, HTTP.DEFAULT_CONTENT_CHARSET);
        HttpProtocolParams.setUseExpectContinue(parameters, false);
        HttpConnectionParams.setTcpNoDelay(parameters, true);
        HttpConnectionParams.setSocketBufferSize(parameters, 8192);
//        parameters.setLongParameter("id", Long.valueOf(my_id));
//        parameters.setParameter("id_str", my_id);
        
      SchemeRegistry schReg = new SchemeRegistry();
//      schReg.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
      schReg.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
      ClientConnectionManager tsccm = new ThreadSafeClientConnManager(parameters, schReg);
      mClient = new DefaultHttpClient(tsccm, parameters);
//      mClient = new DefaultHttpClient();

      mConsumer = new CommonsHttpOAuthConsumer(TWITTER_CONSUMER_KEY,  TWITTER_CONSUMER_SECRET);
      mConsumer.setTokenWithSecret(USER_TOKEN, USER_SECRET);
      
      
        boolean ok = false;
    	HttpGet get;
        try {
        	
        	String verify_url = "https://api.twitter.com/1.1/account/verify_credentials.json";
//        	String retweet = "https://api.twitter.com/1.1/statuses/retweets/"+id.toString()+".json";
//        	String search_url ="https://api.twitter.com/1.1/search/tweets.json&q=asdf";
//        	String user_url = "https://userstream.twitter.com/1.1/user.json";
        	String mentions_url = "https://api.twitter.com/1.1/statuses/mentions_timeline.json";
//        	String utimeline_url = "https://api.twitter.com/1.1/statuses/user_timeline.json";
//        	String ratelim_url = "https://api.twitter.com/1.1/application/rate_limit_status.json";
//        	String upPro_url = "https://api.twitter.com/1.1/account/update_profile.json";
//        	String home_tline = "https://api.twitter.com/1.1/statuses/home_timeline.json";
        	String post_fav_url = "https://api.twitter.com/1.1/favorites/destroy.json";
//        	String fav_list_url = "https://api.twitter.com/1.1/favorites/list.json";
        	
//        	get = new HttpGet(fav_list_url);
//        	mConsumer.sign(get);
//            String response = mClient.execute(get, new BasicResponseHandler());
            
    		
            
            HttpPost post = new HttpPost(post_fav_url);
            List<NameValuePair> formParams = new ArrayList<NameValuePair>(1);
            formParams.add(new BasicNameValuePair("id", "352215735063416833"));
             
            UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(formParams, HTTP.UTF_8);
            post.setEntity(formEntity);
            
            mConsumer.sign(post);
            
            String response = mClient.execute(post, new BasicResponseHandler());

            if(response.charAt(0)=='[') {
            	JSONArray jso = new JSONArray (response.trim());
            	 
            	 System.out.println("json array = " + jso.toString(2));
            } else {
            	JSONObject jso = new JSONObject(response.trim());
//            	int remaining = jso.getJSONObject("/application/rate_limit_status").getInt("remaining");
//            	int remaining = jso.getJSONObject("application").getInt("remaining");
            	
//            	int remaining = jso.getJSONObject("resources").getJSONObject("application").getJSONObject("/application/rate_limit_status").getInt("remaining");
//            	int remaining = jso.getJSONObject("resources").getJSONObject("statuses").getJSONObject("/statuses/home_timeline").getInt("remaining");
//            	int remaining = jso.getJSONObject("resources").getJSONObject("statuses").getJSONObject("/statuses/mentions_timeline").getInt("remaining");
//            	System.out.println("remaining : " + remaining);
            	System.out.println("json obj = " + jso.toString(2));
            	
            }
            
 

        } catch (Exception e) {
            e.printStackTrace();
            throw new ConnectException(e.getLocalizedMessage());
        } 
 
	}
}
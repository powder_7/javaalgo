package com.exe;

import java.util.Arrays;

public class Exe {

	public Exe(){
		
	}
	
	// don't not work
	// c={5,10, 20, 25} amt = 40
	// this fucntion return {25, 10, 5}
	// but answer is {20,20}
//	public int coin(int a[], int idx, int count, int amt ) {
//		
//		// loop thru all coin
//		for(int i=idx; i <a.length; ) {
//			// loop thru the same coin
//			// change coin when amt is zero
//			while( amt-a[i] >= 0) {
//				System.out.println(" amt = " + amt + " count = " + count + " a[i] = " + a[i]);
//				count++;
//				amt = amt -a[i];
//				if(amt==0) {
//					break;
//				}
//			}
//			i++;
//		}
//		return count;
//	}

	// j is counter
	private  int change(int[] c, int amount, int idx) { 

//		System.out.println(" idx = " + idx );
		if (amount == 0)  return 0; 
		
		// no coin to solve amount
		if (idx == c.length) {
//			System.out.println("max  idx " + idx);
			return Integer.MAX_VALUE-1; 
		}
		
 		if (amount < c[idx])  {
//			System.out.println("amt = " + amount + " coin = " + c[idx] + " idx = " + idx );
			return change(c, amount,idx+1);
		}
		else { 
			int c1 = change(c, amount,idx+1); 
			int c2 = 1 + change(c, amount-c[idx],idx); // c0 is used at least once
			System.out.println( " c1 = " +c1 + " c2 = " + c2);
			return Math.min(c1,c2);
		} 
	}
	
	public int change(int[] coins, int amount){ 
		if(amount == 0) return 0;
		int min = amount; // max coins can equal to amount
		      
		for(int i = 0; i < coins.length; i++) {
		   if(coins[i] <= amount) {
			   System.out.println("amt = " + (amount-coins[i]) + " coin = " + coins[i] + " i " + i + " min = " + min  );
			   int noOfCoins = change(coins, amount-coins[i]) + 1;
//			   System.out.println(" num coin " + noOfCoins);
			   min = Math.min(min, noOfCoins);
			   
		   }
		}
		return min;
	}
	
 
	// Returns the count of ways we can sum  S[0...m-1] coins to get sum n
	// m = arry len, n = amt
	public int count( int S[], int ary_len, int amt ) {
	    // If amt is 0 then there is 1 solution (do not include any coin)
	    if (amt == 0)
	        return 1;
	     
	    // If amt is less than 0 then no solution exists
	    if (amt < 0)
	        return 0;
	 
	    // If there are no coins and amt is greater than 0, then no solution exist
	    if (ary_len <=0 && amt >= 1)
	        return 0;
	 
	    // count is sum of solutions (i) including S[ary_len-1] (ii) excluding S[ary_len-1]
	    return count( S, ary_len - 1, amt ) + count( S, ary_len, amt-S[ary_len-1] );
	}
	
	public int changeDP(int[] coins, int amount){ 
		
		int[][] tab = new int[amount+1][coins.length+1]; 
		
		for(int i = 0; i <= amount; i++) 
			tab[i][coins.length] = Integer.MAX_VALUE-1; 
		for(int j = 0; j < coins.length; j++) 
			tab[0][j] = 0; 
		
		for(int i = 1; i <= amount; i++) { 
			for(int j = coins.length-1; j >= 0; j--) { 
				if (i < coins[j]) 
					tab[i][j] = tab[i][j+1]; 
				else { 
					int c1 = tab[i][j+1]; 
					int c2 = 1 + tab[i-coins[j]][j]; 
//					if (c1 < c2) tab[i][j] = c1; 
//					else tab[i][j] = c2; 
					tab[i][j] = Math.min(c1, c2);
				} 
			} 
		}
//		System.out.println(Arrays.deepToString(tab));
		
		for (int[] arr : tab) {
            System.out.println(Arrays.toString(arr));
        }
		return(tab[amount][0]); 
	} 
	
	// j is counter
	private  int change1(int[] c, int amount, int ary_len) { 

		System.out.println("amt = " + amount + " ary_len = " + ary_len );
		if (amount == 0) return 0; 
		
		// no coin to solve amount
		// no solution -- negative sum of money)
		if (amount < 0) {
//			System.out.println("max  idx " + idx);
			return Integer.MAX_VALUE-1; 
		}
		
		if (ary_len <=0 && amount >= 1) {
			return Integer.MAX_VALUE-1;
		}
 		
		int c1 = change(c, amount, ary_len-1); 
		int c2 = 1 + change(c, amount-c[ary_len], ary_len); 
		System.out.println( " c1 = " +c1 + " c2 = " + c2);
		return Math.min(c1,c2);
	}
	
	public void coinTest() {
//		int a[] = {25, 20, 10, 5, 1}; 
//		int a[] = {1, 5, 10, 20, 25};
//		int amt = 40;
		
//		int a[] = {1,2, 3};
		int a[] = {3, 2, 1};
		int amt = 7;
		
		// don't work
//		System.out.println("Amount = "+ amt + " coin need = " + change1(a, amt, a.length-1));
//		System.out.println("Amount = "+ amt + " coin need = " + change(a, amt, 0));
		System.out.println("Amount = "+ amt + " coin need = " + changeDP(a, amt));
		System.out.println("Amount = "+ amt + " coin need = " + change(a, amt));
//		System.out.println("Amount = "+ amt + " coin need = " + count(a, a.length, amt));
	}
	
	public void test() {
		
		coinTest();
		
	}
}

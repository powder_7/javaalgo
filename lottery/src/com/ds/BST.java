package com.ds;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
 
public class BST {

	public class Node {
		public int x;
		char c;
		public Node l;
		public Node r;
 		    
		public Node(int a) {
			x=a;
			l=null;
			r=null;
		}
		Node(char a) {
			c=a;
			l=null;
			r=null;
		}
	}
	
	private Node root;
	public BST() {
		root=null;
	}
	
	public void insert(int a) {
		
		root=insert(root, a);
	}
	
	public Node insert(Node n, int a) {
		
		if(n == null) {
			return new Node(a);
		}
		if(a <= n.x) {
			n.l = insert(n.l, a);
		}else 
			n.r = insert(n.r, a);
		
		return n;
	}
	
	public void printDFS() {
		printDFS(root);
	}
	
	public void printDFS(Node r) {
		
		if(r==null) return;
		printDFS(r.l);
//		System.out.print(" " + r.x);
		System.out.print(" " + r.c);
		printDFS(r.r);
	}
	
	public void printBFS() {
		printBFS(root);
//		printBFSReverse(root);
	}
	
	// increment next level 
	// when a node is add to queue
	public void printBFSReverse(Node r) {
		
		if(r==null) return;
		Queue<Node> q = new LinkedList<Node>();
		Stack<Node> st = new Stack<Node>();
		q.add(r);
		st.add(r);
		st.add(null);
		Node tmp;
		int curr_level = 1;
		int next_level  = 0;
		while(!q.isEmpty()) {
			tmp = q.poll();
			System.out.print(" " + tmp.x);
			if(tmp.l!=null) {
				q.add(tmp.l);
				st.add(tmp.l);
				next_level++;
			}
			if(tmp.r!=null) {
				q.add(tmp.r);
				st.add(tmp.r);
				next_level++;
			}
			curr_level--;
			if(curr_level ==0) {
				st.add(null);
				System.out.println("");
				curr_level = next_level;
				next_level = 0;
			}
			
		}
		
		while(!st.isEmpty()) {
			Node n = st.pop();
			if(n==null)
				System.out.println("");
			else 
				System.out.print(" " + n.x);
		}
		
	}
	
	public void printBFS(Node r) {
		
		if(r==null) return;
		Queue<Node> q = new LinkedList<Node>(); 
		q.add(r);
		Node tmp;
		int curr_level = 1;
		int next_level  = 0;
		while(!q.isEmpty()) {
			tmp = q.poll();
			System.out.print(" " + tmp.x);
//			System.out.print(" " + tmp.c);
			if(tmp.l!=null) {
				q.add(tmp.l);
				next_level++;
			}
			if(tmp.r!=null) {
				q.add(tmp.r);
				next_level++;
			}
			curr_level--;
			if(curr_level ==0) {
				System.out.println("");
				curr_level = next_level;
				next_level = 0;
			}
			
		}
		
	}
	
	// each node is one 
	// goto last node and count back
	// take max(left, right)
	public int heightTree(Node n) {
	
		if(n==null) return 0;
		int l=heightTree(n.l);
		int r=heightTree(n.r);
		
		return 1 + Math.max(l,r);
		
	}
	// 3 compares max(l,r, current_val)
	public int largestNodeVal(Node n) {
		
		if(n==null) return 0;
		int val = n.x;
		int l=largestNodeVal(n.l);
		int r=largestNodeVal(n.r);
		val = Math.max(l, val);
		val = Math.max(r, val);
		
		return val;
	}
	
	public Node buildTree(int []t) {
		for(int a : t) {
			insert(a);
		}
		
		return root;
	}
	
	public Node buildTree1(int []t) {
	
		Queue<Node> q = new LinkedList<Node>();
		Node r = root;
		q.add(root);
		for(int a : t) {
		
			while(!q.isEmpty()){
				Node tmp = q.poll();
				if(tmp==null) {
					root = new Node(a);
					
					q.add(root);
					break;
				}
				if(a <= tmp.x){
					// add to left node
					// break and put root in queue
					// to start add from top
					if(tmp.l==null) {
						tmp.l = new Node(a);
						q.add(root);
						break;
					}
					q.add(tmp.l);
				}else {
					if(tmp.r==null) {
						tmp.r = new Node(a);
						q.add(root);
						break;
					}
					q.add(tmp.r);
				}
			}
			
		}
//		root = r;
		return root;
	}
	
	public boolean hasPathSum(Node r, int sum) {
 
		if(r==null) return false;
		System.out.println(r.x);
		sum = sum - r.x;
		if(sum==root.x && root.l == null && root.r==null) return true;
		// if true, then other function isn't call
		// trick, go left then right
		return hasPathSum(r.l, sum) || hasPathSum(r.r, sum); 
		
	}
	
	public int maxPath(Node n, int sum, int max ) {
		if(n == null) {
			max = Math.max(sum, max);
			return max;
		}
		sum += n.x;
		max = maxPath(n.l, sum, max);
		max = maxPath(n.r, sum, max);
		
		return max;
	}
	
	public void testMaxPath() {
		System.out.println("max path = " + maxPath(root, 0, 0) );
	}
	
	//  a(c,d) become a(c,d)
	public void mirrorTreeRecursive(Node r) {
	
		if(r==null) return;
		
		mirrorTreeRecursive(r.l);
		mirrorTreeRecursive(r.r);
		Node tmp = r.l;
		r.l      = r.r;
		r.r      = tmp;
	}
	
	/*
	 * BFS and swap nodes.
	 */
	public void mirrorTree(Node r) {
		
		Queue<Node> q = new LinkedList<Node>();
		q.add(r);
		while(!q.isEmpty()){
			Node tmp = q.poll();
			if(tmp!=null) {
				Node n = tmp.l;
				tmp.l  = tmp.r;
				tmp.r  = n;
			}
			if(tmp.l != null) q.add(tmp.l);
			if(tmp.r != null) q.add(tmp.r);
		}
		
	}
	
	public void testMirrorTree() {
		System.out.println("mirror tree recurisve");
		mirrorTreeRecursive(root);
		printBFS();
		System.out.println("mirror tree back loop");
		mirrorTree(root);
		printBFS();
	}
	
	public void printLeaf(Node n) {
	
		if(n==null) return;
		printLeaf(n.l);
		printLeaf(n.r);
		
		if(n.l==null && n.r==null)
			System.out.println(n.x);
	}
	
	// print node at given level
	public void printLevelRecursive(Node n, int level ) {
		if(n==null) return;
		// go thru all level
		// reach the level and print
		if(level==0) System.out.print(" " + n.x);
		
		printLevelRecursive(n.l, level-1);
		printLevelRecursive(n.r, level-1);
		

	}
	
	//BFS,need height and loop thru level
	//Time O(n^2), space O(1)
	public void testPrintLevelRecursive() {
		//max(l,r) nodes
		int height = heightTree(root);
		
		for(int i = 0; i < height; i++){
			printLevelRecursive(root, i);
			System.out.println("");
		}
		
	}
	
	public void testPrintLeaf() {
		printLeaf(root);
	}
	
	public void testHasPathSum() {
		
		int sum = 11;
		System.out.println("has path "+ sum + " = " + hasPathSum(root, sum));
	}
	
	public void quickTest() {
		System.out.println("\nlargest val " + largestNodeVal(root));
		
	}
	// flat out bst into array using preorder(n,l,r)
	// add "$" for each node without childrean.
	public int  printInorder(Node n, int idx, String s[]) {
		if(n==null){
			return idx;
		}
			
		idx = printInorder(n.l, idx, s);
		System.out.print (" " + n.x);
		s[idx] = String.valueOf(n.x);
		if(n.l == null){
			s[idx] = s[idx] +  "$";
		}
		if(n.r == null) {
			s[idx] = s[idx] +  "$";
		}
		idx = printInorder(n.r, idx+1, s);
		return idx;
	}
	
	// this don't work bc the hack is wrong
	public void printPreorder(Node n, Queue<String> q) {
		if(n==null){
			return;
		}
		System.out.print(" " + n.x);
		q.add(String.valueOf(n.x));
		if(n.l == null){
			//hack add $ to string
			q.add(n.x+"$");
		}
		if(n.r == null) {
			q.add(n.x+"$");
		}
		printPreorder(n.l, q);
		printPreorder(n.r, q);
		return;
	}
	
	public void subTree() {
		
		BST bst_s = new BST();
		BST bst_t = new BST();
		
		int s[] = {10,4,16};
//		int t[] = {26,10,4,6,3,16};
		int t[] = {26,10,4,16,17,50};
		Node root_s = bst_s.buildTree(s);
		Node root_t = bst_t.buildTree(t);
		
		String a[] = new String[s.length];
		String b[] = new String[t.length];
 
		
		bst_s.printBFS(root_s);
		System.out.println(" ");
		bst_t.printBFS(root_t);
		
		System.out.println("\nInorder S = ");
		bst_s.printInorder(root_s, 0, a);

		System.out.println("\nInorder T = ");
		bst_t.printInorder(root_t, 0, b);
		
		System.out.println("\nInorder s " + Arrays.toString(a));
		System.out.println("\nInorder t " + Arrays.toString(b));

		int count = 0;
		if(a.length > b.length) {
			for(String ss : a) {
				if(Arrays.asList(b).contains(ss)) count++;
			}	
			if(count==b.length)
				System.out.println("Is subtree");
			else
				System.out.println("Is NOT subtree");
			
		}
		else {
			for(String ss : b) {
				if(Arrays.asList(a).contains(ss)) count++;
			}	
			if(count==a.length)
				System.out.println("Is subtree");
			else
				System.out.println("Is NOT subtree");
		}
		
		
//		Queue<String> q_s = new LinkedList<String>();
//		Queue<String> q_t = new LinkedList<String>();
//		
//		System.out.println("\nProrder S = ");
//		bst_s.printPreorder(root_s,  q_s);
//		System.out.println("\nProrder T = ");
//		bst_t.printPreorder(root_t,  q_t);
//		List<String> qs_list = new ArrayList<String>(q_s);
//		List<String> qt_list = new ArrayList<String>(q_t);
//		System.out.println("\nProrder s " +qs_list.toString());
//		System.out.println("\nProrder t " +  qt_list.toString());
//		
//		int count=0;
//		if(qs_list.size() < qt_list.size()) {
//			for(String ss : qs_list) {
//				if(qt_list.contains(ss)) {
//					count++;
//				}
//			}
//			if(count == qs_list.size()) {
//				System.out.println("Is subTree");
//			}
//			else {
//				System.out.println("Is NOT subTree");
//				
//			}
//		}

	}
	
	void testDeleteAll(int a[]) {
		for(int i:a) {
			System.out.println(" del = " + i);
			root = deleteNode(root, i); // tick assign back to root
			printDFS();
			
		}
	}
	
	void deleteNode(int a) {
		deleteNode(root, a);
	}
	
	// dfs and remove current node 
	// 3 case
	// case 1. node has no children
	// case 2. node has left or right child
	// case 3. right left most is null or NOT
	//         if left most NOT null, then curr = most left node
	//         if left most is null, right left = curr left, curr = curr right
	Node deleteNode(Node n, int a) {
		
		if(n==null)	return null;
		
		if(a < n.x) 
			n.l = deleteNode(n.l, a); // trick return node to parent
		else if(a > n.x)
			n.r = deleteNode(n.r,a);
		else	
		if(n.x == a ) {
			// case 1. node has not children
			if(n.l == null && n.r==null) n = null;
			// case 2 node has left or right child
			else if(n.l == null  && n.r!= null) n = n.r;
			else if(n.l != null && n.r == null) n = n.l;
			// node has left and right child
			else if(n.l != null && n.r != null) {
				// right left is null
				//  5
				//    7
				//   6 8
				
				// remove 7
				//    5
				//     8
				//   6
				// case 3 right left is null
				if(n.r.l == null) {
					n.r.l= n.l;  // right left child is new branch 
					n = n.r;     //replace parent with right child
				}
				// node = right most left
				// n = n.r.left, right the last left mode
				// if right left most is null then 
				// n = n.r.r
				// case 4 right left is NOT null
				else {

					Node prev=n, curr = n.r;
					// loop thru left node till null
					while(curr.l != null) {
						prev = curr;
						curr = curr.l;
					}
					// assign node to left most node
					// we can assign value or node
//					n.x = curr.x;
					curr.l = n.l;
					curr.r = n.r;
					n = curr;
					// remove node
					prev.l = null;
				}
			}
		}

		return n;
	}

	public void findNode(int a) {
		a = 3;
//		Stack<Node> st = new Stack<Node>();
//		root = findNode(root, a, st);
		root = findNode(root, a);
		printDFS();
	}
	
	//return node, else null
	public Node findNode(Node n, int a) {
		
		if(n==null) return null;
		System.out.println(n.x);
		if(n.x == a) {
			System.out.println(" found = " + a);
			return n;
		}else {
			// return parent if uncomment
	//		if(n.l != null && n.l.x == a) return n.l;
	//		if(n.r != null && n.r.x == a) return n.r;
//			if(a < n.x)	return findNode(n.l, a);
//			else 		return findNode(n.r, a);
		
//          loop thru all the nodes
			Node l = findNode(n.l, a);
			Node r = findNode(n.r, a);
			if(l!=null) return l;
			else return r;//findNode(n.r, a);  // if null then go right
		}
		 
	}
	
	public void testTotalLeave() {
		System.out.println("Total num of leave = " + totalLeave(root));
	}
	
	public int totalLeave(Node n ) {
		
		if(n==null) return 0;
		return 1+ totalLeave(n.l)+ totalLeave(n.r);
	}
	
	//return node, else null
	public Node findNode(Node n, int a, Stack<Node> st) {
		
		if(n==null || n.x == a) return n;
		Node tmp;
		st.add(n);
		while(!st.empty()) {
			tmp = st.pop();
			System.out.println("a = " + tmp.x);
			
			if(tmp.x == a) return tmp;
			if(tmp.l != null) st.add(tmp.l);
			if(tmp.r != null) st.add(tmp.r);
		}
		
		return null;
	}

	public void test_lca() {
//		root = LCA_BST(root, 1, 4);
 
		root = LCA(root, root.l.l, root.l.r);
//		root = LCA(root, root.l, root.r);
		printBFS(root);
	}
	
	// least common ancestor
	public Node LCA_BST(Node n, int a, int b) {
	
		if(n==null) return null;
		if(a > n.x && b > n.x)
			return LCA_BST(n.r, a, b);
		if(a < n.x && b < n.x)
			return LCA_BST(n.l, a, b);
		else 
			return n;
	}
	
	public Node LCA(Node n, Node a, Node b) {
		if(n==null) return null;
		if(n==a || n==b)
			return n;
//		System.out.println(n.x);
		Node l = LCA(n.l, a, b);
		Node r = LCA(n.r, a, b);
		
		//on opposite side
		if(l!=null && r!=null)
			return n;
		//on same side
		if(l!=null) return l;
		else return r;
	}
	
	public void deepCopy(Node r, Node n) {
		if(r==null) return;
		Stack<Node> st_r  = new Stack<Node>(); 
		Stack<Node> st_n = new Stack<Node>();
		st_r.add(r);
		n = new Node(r.x);
		st_n.add(n);
//		Node dc = n;
		Node tmp=null;
		Node tmp_n=null;
		while(!st_r.isEmpty()) {
			tmp = st_r.pop();
			tmp_n= st_n.pop();
			if(tmp.l!=null){
				st_r.add(tmp.l);
				tmp_n.l = new Node(tmp.l.x);
				st_n.add(tmp_n.l);
			}
			if(tmp.r!=null) {
				st_r.add(tmp.r);
				tmp_n.r = new Node(tmp.r.x);
				st_n.add(tmp_n.r );
			}
		}
		printBFS(n);
	
	}
	
	//push ?
	//pop :
	// a?b?c:d:e
	/*
	 *       a
	 *     b   e
	 *   c  d
	 * 
	 */
	// a?b:c?d:e
	public void ternary2BT(String s) {
		
		Stack<Node> st = new Stack<Node>();
		Node rt = new Node(s.charAt(0));
		Node n = rt;
		for(int i=1; i<s.length();i+=2) {
//			System.out.println(s.charAt(i+1));
			if('?'==s.charAt(i)) {
				 n.l = new Node(s.charAt(i+1));
				 st.add(n);
				 n = n.l;
			}
			else if(':'==s.charAt(i)) {
				n = st.pop();
				while(n.r !=null && !st.isEmpty())
					n = st.pop();
				n.r = new Node(s.charAt(i+1));
				st.add(n);
				n = n.r;
				
			}
			
		}
		for(Node x : st) {
			System.out.println(" size = " + st.size() + " " + x.c) ;
 		}
		
		printDFS(rt);
 
	}
	
	/*
	       5
	     /   \
	    3     7
	   / \   / \
	  1   4 6   9
	   \       / \
	    2     8   10
	 
	*/ 

	 
	
	public void test() {
		Node bst_root =null;
		BST bst = new BST();
		int a[] = {5,7,3,1,2,4,9, 8,6, 10};
//		int a[] = {5, 7};
		
		bst_root = bst.buildTree(a);
//		bst_root = bst.buildTree1(a);
		
//		bst.printDFS();
		bst.printBFS();
//		System.out.println("\nheight = " + bst.heightTree(bst_root));
//		bst.testHasPathSum();
//		bst.testMirrorTree();
//		bst.testPrintLeaf();
//		bst.testPrintLevelRecursive();
//		bst.quickTest();
//		subTree();
//		bst.testDeleteAll(a);
//		bst.deleteNode(5);
//		bst.printDFS();
//		bst.test_lca();
//		bst.printBFS();
//		Node dc= null;
//		bst.deepCopy(bst_root, dc);
//		bst.printDFS();		
//		bst.findNode(7);
//		bst.testMaxPath();
//		bst.testTotalLeave();
//		bst.printBFS();
		
		bst.ternary2BT("a?b?c:d:e");
//		bst.ternary2BT("a?b:c?d:e"); 
	}
}

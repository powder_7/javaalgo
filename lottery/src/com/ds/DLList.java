package com.ds;

public class DLList {

	public class Node {
		int x;
		Node next;
		Node prev;
		
		Node(int a) {
			x=a;
			next=null;
			prev=null;
		}
	}
	
	private int size=0;
	private Node head, tail;
	public DLList() {
//		size = 0;
		head=null;
		tail=null;
	}
	
	public int size() {
		return size;
	}
	
	public boolean isEmpty() {
		return (size==0);
	}
	
	
	//1 case, head=tail=empty
	//return a
	public int addFront(int a) {
	
		Node r = new Node(a);
		if(isEmpty()) {
			tail = r;
		}else {
			r.next = head;
			head.prev = r;
		}
		head = r;
		size++;
		
		return head.x;
	}
	
	public int addLast(int a) {
		Node r = new Node(a);
		if(isEmpty()) {
			head=r;
		}else {
			r.prev = tail;
			tail.next = r;
		}
		tail=r;
		size++;
		return tail.x;
	}
	
	// 4 cases, 1 node, head, teal, mid
	// return , else -1
	public int deleteNode(int a) {
		Node r = head;
		if(r==null) return -1;
		int tmp_size = size;
		while(r!=null) {
			// one node
			if(head.x==a && tail.x == a) {
				head=null;
				tail=null;
				size--;
				break;
			}
			else if(head.x==a) {
				head = head.next;
				head.prev = null;
				size--;
				break;
			}
			else if(tail.x ==a) {
				tail = tail.prev;
				tail.next = null;
				size--;
				break;
			}
			else if(r.x==a) {
				r.prev.next = r.next;
				r.next.prev = r.prev;
				size--;
				break;
			}
			
			r=r.next;
		}
		// not found
		if(tmp_size==size)
			return -1;
		
		return a;
	}
	
	public void printList() {
		
		Node r = head;
		while(r!=null) {
			System.out.print(" " + r.x);
			r = r.next;
		}
	}
	
	public void printTailList() {
	
		Node r = tail;
		while(r!=null) {
			System.out.print( " " + r.x);
			r = r.prev;
		}
	}
	
	public void setSize(int a) {
		size = a;
	}
	
	public int getSize() {
		return size;
	}
	
	public void reverseDLL() {
		Node curr = head;
		Node prev = null;
		
		while(curr != null) {
 
	        prev = curr;
	        curr = curr.next;
	        prev.next = prev.prev;
	        prev.prev = curr;
		}
		head = prev;
		
	}
	public void test() {
		DLList d_ll = new DLList();
		d_ll.addFront(1);
		d_ll.addFront(2);
		d_ll.addFront(3);
		d_ll.addLast(4);
		d_ll.printList();
//		System.out.println(" delete  = " + d_ll.deleteNode(1) );
		
//		d_ll.deleteNode(2);
//		d_ll.deleteNode(4);
		
		d_ll.reverseDLL();
		d_ll.printList();
		
//		d_ll.printTailList();
		System.out.println(" size = " + d_ll.getSize());
	}
}

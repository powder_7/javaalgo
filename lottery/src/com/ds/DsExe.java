package com.ds;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

class Node {
	int x;
	Node n;
	Node l;
	Node r;
	Node next;
	Node prev;
	List<Node> c_list;
	Node(int a) {
		x=a;
		n=null;
		l=null;
		r=null;
		next=null;
		prev=null;
		c_list = new ArrayList<Node>();
	}
	
	 
}
class pc{
	int p;
	int c;
	pc(int x, int y) {
		p=x;
		c=y;
	}
	public void add(int c2) {
		// TODO Auto-generated method stub
		
	}
}
class MyTree {
	List<pc> pc_list = new ArrayList<pc>();
	pc my_pc;
 	Node my_t;
 	
	public void init() {
		my_pc = new pc(9,8);
		pc_list.add(my_pc);
		
		my_pc = new pc(9,7);
		pc_list.add(my_pc);
		
		my_pc = new pc(8,6);
		pc_list.add(my_pc);
		
		my_pc = new pc(8,5);
		pc_list.add(my_pc);
		
		my_pc = new pc(7,4);
		pc_list.add(my_pc);
		
		my_pc = new pc(7,3);
		pc_list.add(my_pc);
		
	}
	public int getRoot() {
 
		int root=0;
		List<Integer> p = new ArrayList<Integer>();
		List<Integer> c = new ArrayList<Integer>();
		for(pc x : pc_list) {
			p.add(x.p);
			c.add(x.c);
		}
		
		for(int r : p) {
			if(!c.contains(r)){
				root= r;
				break;
			}
		}
		return root;
	}
	
	public void buildT(int x) {
		my_t = buildTree(x);
	}
	
 	public Node buildTree(int x) {
		
		Node n = new Node(x);
		List<Integer> c_list = new ArrayList<Integer>();
		// get all child
		for(pc c : pc_list) {
			if(c.p==x) c_list.add(c.c);
		}
		
		//create node for child
		for(int child : c_list) {
			n.c_list.add(buildTree(child));
		}
		return n;
	}
	
	public void printPC() {
		for(pc x : pc_list) {
			System.out.println(x.p + " " + x.c);
		}
	}
	
	public void printBFS() {
		printBFS(my_t);
	}
	
	public void printBFS(Node n) {
		
		if(n==null) return;
		Queue<Node> q = new LinkedList<Node>();
		q.add(n);
		int lvl =0;
		int next=0;
		while(!q.isEmpty()) {
			Node m = q.poll();
			for(Node l : m.c_list) {
				q.add(l);
				next++;
			}
			System.out.print(""+m.x);
			if(lvl ==0) {
				lvl=next;
				next=0;
				System.out.println(" ");
			}
			lvl--;
		}
	}

	public void printDFS() {
		printDFS(my_t);
	}
	public void printDFS(Node n) {
		if(n==null) return;
		System.out.println(n.x);
		for(Node c: n.c_list) {
			printDFS(c);
		}
	}
	
	public void pathAndTotal() {
		Stack<Integer> st = new Stack<Integer>();
		pathAndTotal(my_t, st);
	}
	public void pathAndTotal(Node n, Stack<Integer> st) {
		if(n==null) return;
		st.add(n.x);
		if(n.c_list.isEmpty()) {
			int total = 0;
			for(int i : st) {
				total +=i;
				System.out.print(" " + i);
			}
			System.out.println(" total = " + total);
		}
		for(Node child : n.c_list) {
			pathAndTotal(child, st);
		}
		if(!st.isEmpty()) st.pop();// remember this line
	}
	
	public void test() {
		MyTree t = new MyTree();
		t.init();
//		t.printPC();
//		System.out.println("root = " + t.getRoot());
		t.buildT(t.getRoot());
		t.printBFS();
		t.pathAndTotal();
//		t.printDFS();
	}
}

// addFirst, addLast, remove
class LL {
	
	private Node root=null;
	LL() {}
	
	public void addFront(int x) {
		
		if(root == null ) root = new Node(x);
		else {
			Node n = new Node(x);
			n.n = root;
			root = n;
		}
	}
	
	public void deleteLoop(int x) {
		if(root.x == x) root = root.n;
		else {
			Node p = root;
			Node c = p.n;
			while(c!=null) {
				if(c.x ==x) {
					p.n = c.n;
					break;
				}
				p = p.n;
				c = c.n;
			}
		}
	}
	
	public void delete(int x) {
		root=delete(root, x);
	}
	
	public Node delete(Node n, int x) {
		
		if(n==null) return null;
		if(n.x==x) return n.n;
		else n.n = delete(n.n, x);
		return n;
	}
	
	//recursive add last
	public void addLast(int x) {
		root = addLast(root, x);
	}
	public Node addLast(Node n, int x) {
		if(n==null) return new Node(x);
		else n.n = addLast(n.n, x);
		return n;
	}
	
	public void print() {
		while(root!=null) {
			System.out.println(root.x);
			root = root.n;
		}
	}
	
	public void test() {
		LL ll = new LL();
		
		for(int i=0; i<5;i++) {
//			ll.addFront(i);
			ll.addLast(i);
		}
		ll.delete(3);
		ll.deleteLoop(2);
		ll.deleteLoop(0);
		ll.deleteLoop(1);
//		ll.deleteLoop(4);
		ll.print();
	}
	
}

//add, remove, print
class MyBST {
	
	Node root =null;
	
	MyBST() {}
	
	public void find(int x) {
		Node n = find(root, x);
		printBFS(n);
	}
	public Node find(Node n, int x) {
		
		if(n==null) return null;
		
		if(n.x == x) return n;
		else {
			if(n.x < x) return find(n.r, x);
			else return find(n.l, x);
		}
	}
	
	
	public void maxVal() {
		System.out.println("max val = " +  maxVal(root, 0,0));
	}
	public int maxVal(Node n, int sum, int max) {
		if(n==null) {
			return Math.max(sum, max);
		}
		sum = n.x;
		max = maxVal(n.l, sum, max);
		max = maxVal(n.r, sum ,max);
		return max;
	}
	public void maxPathVal() {
		System.out.println("max  path val = " +  maxPathVal(root, 0,0));
	}
	public int maxPathVal(Node n, int sum, int max) {
		
		if(n==null) {
			return Math.max(sum, max);
		}
		sum +=n.x;
		max = maxPathVal(n.l, sum, max);
		max = maxPathVal(n.r, sum ,max);
		return max;
	}
	
	public void printPathNVal() {
		Stack<Node> st = new Stack<Node>();
		printPathNVal(root, st);
	}

	// use stack, check left right is NULL
	public void printPathNVal(Node n, Stack<Node> st) {
		if (n == null) return;
		st.add(n);
		if (n.l == null && n.r == null) {
			int val = 0;
			for (Node i : st) {
				int a = i.x;
				val += a;
				System.out.print(" " + i.x);
			}
			System.out.println(" total path val  = " + val);
		} else {
			printPathNVal(n.l, st);
			printPathNVal(n.r, st);
		}
		if (!st.isEmpty()) st.pop();

	}
	public void hasPath() {
		int x = 7;
		System.out.println("has path "  + x + " =  " + hasPath(root, x) );
	}
	public boolean hasPath(Node n, int x) {
		if(n==null) return (x==0);
		return hasPath(n.l, x-n.x) || hasPath(n.r, x-n.x);
	}
	
	public void heightBottomUp() {
		System.out.println("height = " + heightBottomUp(root));
	}
	public int heightBottomUp(Node n) {
		if(n==null) return 0;
		return 1 + Math.max(heightBottomUp(n.l), heightBottomUp(n.r));
	}
	
	public void remove(int x) {
		root = remove(root, x);
	}
	
	public Node remove(Node n, int x) {
		if(n==null) return null; 
		//check left and right node
		if(n.x==x) {
			//left is NULL
			if(n.l==null && n.r!=null)
				return n.r;
			//right is NULL
			else if(n.l!=null && n.r==null)
				return n.l;
			//left and right have node
			else {
				//check right left is NULL
				// assign right left to current node
				if(n.r.l==null) {
					n.r.l = n.l;
					return n.r;
				}
				// righ left most is NULL
				else {
					Node p = n.r.l;
					Node c = p.l;
					while(c!=null) {
						if(c.l == null) break;
						p=p.l;
						c=c.l;
					}
					c.l = n.l;
					c.r = n.r;
					n = c;
					p.l = null; // remove right left most node;
				}
			}
		}
		else {
			if(n.x < x) n.r = remove(n.r, x);
			else n.l = remove(n.l, x);
		}
		return n;
	}
	
	public void add(int x) {
		root = add(root, x);
	}
	public Node add(Node n, int x) {
		if(n==null) return new Node(x);
		if(n.x < x) n.r = add(n.r, x);
		else n.l = add(n.l, x);
		return n;
	}
	
	public void printBFT() {
		
		if(root==null) return;
		Queue<Node> q = new LinkedList<Node>();
		q.add(root);
		int lvl = 0;
		int next = 0;
		while(!q.isEmpty()) {
			Node n = q.poll();
			if(n.l!=null) {
				q.add(n.l);
				next++;
			}
			if(n.r!=null) {
				q.add(n.r);
				next++;
			}
			System.out.print(" " + n.x);
			if(lvl == 0) {
				lvl = next;
				next = 0;
				System.out.println("");
			}
			lvl--;
		}
	}
	
	public void print() {
		print(root);
	}
	
	// print inorder
	public void print(Node n) {
		if(n!=null) { 
			print(n.l);
			System.out.println(n.x);
			print(n.r);
		}
	}
	
	public void printBFS(Node n) {
		if(n==null) return;
		Queue<Node> q = new LinkedList<Node>();
		q.add(n);
		int lvl=0;
		int next=0;
		while(!q.isEmpty()) {
			Node m = q.poll();
			if(m.l !=null) {
				q.add(m.l);
				next++;
			}
			if(m.r !=null) {
				q.add(m.r);
				next++;
			}
			System.out.print("  " + m.x);
			if(lvl == 0) {
				lvl = next;
				next = 0;
				System.out.println("");
			}
			lvl--;
		}
	}
	public void test() {
		MyBST bst = new MyBST();
		int a[] = {1,4,2,8, 7,6,50};
//		int a[] = {4,2,5};
		
		for( int i :a) {
			bst.add(i);
		}
		bst.find(8);
		bst.maxPathVal();
		bst.maxVal();
		bst.printPathNVal();
		bst.hasPath();
        bst.heightBottomUp();
		bst.printBFT();
		bst.remove(4);
		bst.printBFT();
	}
}

//add, remove, print
class MyDLList {
	Node head;
	Node tail;
	
	MyDLList() {
		head=null;
		tail=null;
	}
	
	public Node addFront(int x) {
		
		if(null==head && null==tail) {
			Node n = new Node(x);
			head = n;
			tail = n;
			return head;
		}else {
			Node n = new Node(x);
			n.next = head;
			head.prev = n;
			head = n;
			return n;
		}
	}
	
	public Node addBack(int x) {
		
		if(null==head && null==tail) {
			Node n = new Node(x);
			head = n;
			tail = n;
			return head;
		}else {
			Node n = new Node(x);
			n.prev = tail;
			tail.next = n;
			tail = n;
			return n;
		}
	}
	
	public void remove(int x) {
		if(null==head || null == tail) return;
		Node n = tail;
		while(null!=n){
			if(head.x==x && tail.x==x) {
				head=null;
				tail=null;
				break;
			}
			else if(head.x ==x) {
				head = head.next;
				head.prev = null;
				break;
			}
			else if(tail.x==x) {
				tail = tail.prev;
				tail.next = null;
				break;
			}
			else if(n.x==x) {
				n.prev.next = n.next;
				n.next.prev = n.prev;
				System.out.println("found " +x);
			}
			n=n.prev;
		}
	}
	
	public void printFront() {
		System.out.println("printFront ");
		Node n = head;
		while(null!=n){
			System.out.println(n.x);
			n=n.next;
		}
	}
	public void printBack() {
		System.out.println("printBack ");
		Node n = tail;
		while(null!=n){
			System.out.println(n.x);
			n=n.prev;
		}
	}
	public void test() {
		MyDLList ddlist = new MyDLList();
		for(int i=0;i<5;i++) {
//			ddlist.addFront(i);
			ddlist.addBack(i);
		}
		for(int j=0;j<5;j++)
			ddlist.remove(j);
		ddlist.printFront();
		ddlist.printBack();
	}
}

class MyTree2 {
	List<pc> pc_list = new ArrayList<pc>();
	pc my_pc;
 	Node my_t;
 	MyTree2() {
 		init();
 	}
 	void init() {
 		my_pc = new pc(9,8);
 		pc_list.add(my_pc);
 		
		my_pc = new pc(9,7);
		pc_list.add(my_pc);
		
		my_pc = new pc(8,6);
		pc_list.add(my_pc);
		
		my_pc = new pc(8,5);
		pc_list.add(my_pc);
		
		my_pc = new pc(7,4);
		pc_list.add(my_pc);
		
		my_pc = new pc(7,3);
		pc_list.add(my_pc);
 	}
 	
 	public int getRoot() {
 		
 		List<Integer> p = new ArrayList<Integer>();
 		List<Integer> c = new ArrayList<Integer>();
 	 	
 		//setup parent and child
 		for(pc s : pc_list) {
 			p.add(s.p);
 			c.add(s.c);
 		}
 		
 		// root can't be in child
 		for(int x : p) {
 			if(!c.contains(x)) 
 				return x;
 		}
 		
 		// no root
 		return -1;
 	}
 	
 	public void buidTree(int x) {
 		my_t = buildTree(x);
 	}
	
 	public Node buildTree(int x) {
 		
 		Node n = new Node(x);
 		List<Integer> child = new ArrayList<Integer>();
 		for(pc p : pc_list) {
 			// find parent x and all child
 			if(p.p == x) child.add(p.c);
 		}
 		
 		for(int a : child) {
 			n.c_list.add(buildTree(a));
 		}
 		
 		return n;
 	}
 	public void printDFS() {
 		printDFS(my_t);
 		System.out.println("");
 	}
 	
 	public void printDFS(Node n) {
 		if(n == null) return;
 		System.out.println(n.x);
 		for(Node x : n.c_list) {
 			printDFS(x);
 		}
 	}
 	
 	public void printBFS() {
 		
 		Queue<Node> q = new LinkedList<Node>();
 		q.add(my_t);
 		int cur_lvl=0;
 		int next_lvl = 0;
 		while(!q.isEmpty()) {
 			Node p = q.poll();
 			for(Node c : p.c_list) {
 				q.add(c);
 				next_lvl++;
 			}
 			System.out.print(" " + p.x);
 			if(cur_lvl== 0) {
 				cur_lvl = next_lvl;
 				next_lvl = 0;
 				System.out.println("");
 			}
 			cur_lvl--;
 		}
 	}
 	
 	public void maxPath() {

 		System.out.println(" max path = " + maxPath(my_t, 0, 0));
 		System.out.println(" print path ");
 		printPath(my_t, new Stack<Node>());
 	}
 	
 	public int maxPath(Node n, int sum, int max_num) {
 		
 		sum += n.x;
 		
 		if(n.c_list.isEmpty()) return Math.max(sum,max_num);
 		
 		for(Node i : n.c_list) {
 			max_num = maxPath(i, sum, max_num);
 		}
 		
 		return max_num;
 	}
 	
 	public void printPath(Node n, Stack<Node> st) {
 		
 		if(n==null) return;
 		st.push(n);
 		if(n.c_list.isEmpty()) {
 			int tmp=0;
 			for(Node i : st) {
 				tmp += i.x;
 				System.out.print(" " + i.x);
 			}
 			System.out.print(" "  + tmp);
 			System.out.println("");
 		}
 		
 		for(Node x : n.c_list) {
 			printPath(x, st);
 		}
 		st.pop();
 	}
 	
 	public void findNodeParent() {
 		int x = 5;
 		Node n = findNodeParent(my_t, x);
 		printDFS(n);
 	}

 	public Node findNodeParent(Node n, int x) {
 	
 		if(n==null) return null;
 		Queue<Node> q = new LinkedList<Node>();
 		q.add(n);
 		
 		while(!q.isEmpty()) {
 			Node p = q.poll();
 			for(Node c: p.c_list) {
 				if(c.x == x) return p;
 				else q.add(c);
 			}
 		}
 		//not found
 		return null;
 	}
 	
 	public void getTreeHeight() {
 		System.out.println("tree height  = " + getTreeHeight(my_t, 0, 0));
 	}
 	
 	public int getTreeHeight(Node n, int h, int sum) {
 		
 		if(n==null) return 0;
 		if(n.c_list.isEmpty()) {
 			return Math.max(h, sum);
 		}
 		for(Node i : n.c_list) {
 			sum = getTreeHeight(i, h+1, sum);
 		}
 		return sum;
 	}
 	
 	public void deleteNode() {
 		int x = 8;
 		deleteNode(my_t, x);
 		deleteNode(my_t, 19);
 	}
 	
 	public void deleteNode(Node n, int x) {
 	
 		if(null==n) return;
 		
 		Node tmp = n;
 		if(tmp.x==x && tmp.c_list.isEmpty()) n=null;
 		else if(tmp.x==x && !tmp.c_list.isEmpty()) {
 			Node p = n;
 			Node child = n.c_list.get(0);
 			while(!child.c_list.isEmpty()) {
 				p = child;
 				child = child.c_list.get(0);
 			}
 			n.x = child.x;
 			p.c_list.remove(0);
 		}else {
 			Stack<Node> st = new Stack<Node>();
 			st.add(n);
 			while(!st.isEmpty()) {
 				Node p = st.pop();
 				for(Node child : p.c_list) {
 					if(child.x == x) {
 						for(Node grand_child : child.c_list) {
 							p.c_list.add(grand_child);
 						}
 						p.c_list.remove(child);
 						break;
 						
 					}
 				}
 			}
 		}
 		
 		printBFS();
 	}
 	
 	public void test() {
 		MyTree2 t = new MyTree2();
 		System.out.println(" root = " + t.getRoot());
 		t.buidTree(t.getRoot());
// 		t.printDFS();
 		t.printBFS();
// 		t.maxPath();
// 		t.findNodeParent();
// 		t.getTreeHeight();
 		t.deleteNode();
 		
 		
 	}
}

class LinkedList2 {
	Node head=null;
	LinkedList2() {
		
	}

	public void addFront(int x) {
		this.head = addFront(this.head, x);
	}
	
	public Node addFront(Node head,int x) {
		if(null==head) return new Node(x);
		Node tmp = new Node(x);
		tmp.next =head;
		head = tmp;
		return head;
	}
	
	public void addLastLoop(int x) {
//		head = addLast(head, x);
		Node p = head;
		if(head==null) head = new Node(x);
		else {
			while(p.next!=null){
				p=p.next;
			}
			p.next = new Node(x);
		}
	}
	
	public Node addLast(Node n, int x) {
		if(null==n) return new Node(x);
		n.next = addLast(n.next, x);
		return n;
	}
	
	public void removeFirst() {
		if(null == head) return;
		head = head.next;
	}
	
	public void remove(int x) {
		head = remove(head, x);
	}
	
	public void removeLast() {
		
		head = removeLast(head);
		
//		if(null==head || null==head.next) return;
//		Node p = head;
//		Node c = p.next;
//		while(c!=null) {
//			//need this trick
//			if(null==c.next)break;
//			p = p.next;
//			c = c.next;
//		}
//		p.next=null;
	}
	
	public Node removeLast(Node n) {
		if(null == n) return n;
		if(n.next==null) return n.next;
		else n.next = removeLast(n.next);
		return n;
	}
	
	public Node remove(Node n, int x) {
		if(null==n) return null;
		if(n.x == x) return n.next;
		else n.next = remove(n.next, x);
		return n;
	}
	
	public void printList() {
		while(head!=null) {
			System.out.println(head.x);
			head = head.next;
		}
	}
	
	public void test() {
		LinkedList2 ll = new LinkedList2();
		ll.addFront(1);
		ll.addFront(2);
		ll.addLastLoop(9);
//		ll.removeFirst();
		ll.removeLast();
		ll.remove(2);
		ll.printList();
	}
}


public class DsExe {
	
	public void runTest(){
		LL ll = new LL();
		MyBST bst = new MyBST();
		MyTree t = new MyTree();
		MyDLList dlist = new MyDLList();
		MyTree2 t2 = new MyTree2();
		LinkedList2 ll2 = new LinkedList2();
//		ll.test();
//		bst.test();
//		t.test();
//		dlist.test();
//		t2.test();
		ll2.test();
	}
}



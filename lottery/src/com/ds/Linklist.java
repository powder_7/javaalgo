package com.ds;

public class Linklist {
	
	class Node {
		int x;
		Node next;
		
		Node() {
			x=0;
			next=null;
		}
		Node(int a) {
			x=a;
			next = null;
		}
	}
	
	private Node head=null;
	private int size=0;
	
	void Listlist() {
		head = null;
	}
	public Node getHead() {
		return head;
	}
	public int size() {
		Node tmp = head;
		int count = 0;
		while(tmp != null) { 
			count++;
			tmp = tmp.next;
		}
		return count;
	}
	
	private void print() {
		Node tmp = head;
		while(tmp != null) {
			System.out.print(" " + tmp.x);
			tmp = tmp.next;
		}
	}
	
	// node point head, head point to new node
	// return x if success
	public int addFirst(int x) {
		Node node = new Node();
		node.x = x;
		node.next = head;
		head = node;
		size++;
		return head.x;
	}
	
	// return x if success
	public int addLast(int x) {
//		Node tmp = head;
//		Node node = new Node();
//		node.x = x;
//		
//		// only 1 node
//		if(head == null) head = node; 
//		else  {
//			while(tmp.next != null){
//				tmp = tmp.next;
//			}
//			tmp.next = node;
//			return node.x;
//		}
//		return -1;
		head = addLast(head, x);
		return head!=null ? x : -1 ;
	}
	
	public Node addLast(Node n, int x) {
		
		if(n==null) return new Node(x);
		n.next = addLast(n.next,x);
		return n;
	}
	// return x if success
	public int remove(int x) {
		
		if(head != null) {
			Node cur = head;
			Node prev = null;
			while(cur != null) {
				if(cur.x == x) {
					// remove first node
					if(prev == null) {
						head = head.next;
						cur = head;
					} else {
						prev.next = cur.next;
						cur = null;
					}
					return x;
				}
				prev = cur;
				cur = cur.next;
			}
		} 
		return -1;
	}
	public void testRemoveRecursive(int a) {
		head = remove(head, a);
	}
	
	public Node remove(Node n, int a) {

		if(n==null) return null;
		if(n.x == a)  return n.next;
		else n.next = remove(n.next, a);
		return n;
	}
	
	//return first if success else -1
	public int removeFirst() {
		
		if(head == null)
			return -1;
		else {
			int x = head.x;
			head = head.next;
			return x;
		}
	}
	
	// need to remove prev.next that point to curr node
	public int removeLast() {
	
		Node curr = head;
		Node prev= null;
		int x=-1;
		while(curr.next != null) {
			prev = curr;
			curr = curr.next;
		}
		if(prev == null) {
			x = head.x;
			head = null;
		}
		else {
			// get last node value
			// remove last node
			x = prev.next.x;
			prev.next = null;
		}
		return x;
	}
	
	public void removeLastRecursive() {
		head = removeLastRecursive(head);
	}
	
//	public void removeLast(Node n) {
//		if(n==null) return;
//		//head
//		if(n.next==null) {
//			head=null;
//			return;
//		}
//		else if(n.next.next == null) {
//			n.next = null;
//		}
//		removeLast(n.next);
//	}
	
	public Node removeLastRecursive(Node n) {
		if(n==null) return null;
		if(n.next == null) n = null;
		else n.next = removeLastRecursive(n.next);
		return n;
	}
	
	public int maxVal(Node n) {
		if(n==null) return 0;
		int x = n.x;
		x = Math.max(maxVal(n.next), x);
		return x;
	}
	
	/**
	 * 	Trick store curr.next, 
	 * 	update curr.next to prev and 
	 * 	prev to curr and 
	 * 	curr to next
	 */
	public void reverseLL() {
		if(head==null) return;
		Node curr = head;
		Node next = null;
		Node prev = null;
		while(curr!=null) {
			next = curr.next;
			curr.next = prev;
			prev = curr;
			curr=next;
		}
		head =  prev;
	}
	
	public void test() {
		Linklist ll = new Linklist();
		ll.addFirst(2);
		ll.addFirst(1);
		ll.addFirst(3);
		System.out.println("link list before test");
		ll.print();
		ll.reverseLL();
		System.out.println("\nreverse linkedlist");
		ll.print();
		
//		ll.addLast(1);
//		ll.addLast(2);
//		ll.addLast(4);
		
//		ll.remove(2);
//		ll.remove(1);
		

//		ll.testRemoveRecursive(2);
//		System.out.println("remove first = " + ll.removeFirst());
//		System.out.println("remove last = " + ll.removeLast());
//		System.out.println("remove last = " );
		ll.removeLastRecursive();
		ll.removeLastRecursive();
//		System.out.println(" max val = " + ll.maxVal(ll.getHead()));
		System.out.println("\nlink list after test");
		ll.print();
		System.out.println(" size = " + ll.size() );
	}
}

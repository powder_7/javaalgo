package com.ds;

import org.junit.Test;

public class DS_Test {
	public class Node {
		int x;
		Node next;
		Node(int a) {
			x=a;
		}
	}
	public class LinkedList{
		private Node mHead;
		private int mIndex;
		
		LinkedList() {
			mHead = null;
			mIndex = 0;
		}
		
		//add end of list
		private void add(int x) {
			if(null == mHead) {
				mHead = new Node(x);
			}else {
				Node n = mHead;
				while(null!=n.next) {
					n = n.next;
				}
				n.next =  new Node(x);
				mIndex++;
			}
		}
		
		//add front
		private void addFront(int x) {
			if(null==mHead) mHead = new Node(x);
			else {
				Node tmp = new Node(x);
				tmp.next = mHead;
				mHead = tmp;
				mIndex++;
			}
		}
		
		//add at index
		private boolean addIndex(int x, int idx) {
			if(0 <= idx && idx <= mIndex) {
				if(0==idx) {
					Node n = new Node(x);
					n.next = mHead;
					mHead = n;
					mIndex++;
					return true;
				}else {
					
					Node curr = mHead;
					Node prev = null;
					int count_idx=1;
					while(null!=curr) {
						prev = curr;
						curr = curr.next;
						if(count_idx==idx) {
							Node n = new Node(x);
							Node tmp = null;
							tmp = prev.next;
							prev.next = n;
							n.next = tmp;
							mIndex++;
							return true;
						}
						count_idx++;
					}
				}
				
			}
			return false;
		}
		
		//remove node at index
		private boolean remove(int idx) {
			if(0 <= idx && idx <= mIndex && null != mHead) {
				//remove head
				if(0==idx) {
					mHead = mHead.next;
					mIndex--;
					return true;
				}
				Node curr = mHead;
				Node prev = null;
				int count_idx=1;
				while(null!=curr) {
					prev = curr;
					curr = curr.next;
					if(count_idx == idx) {
						//remove node
						prev.next = curr.next;
						mIndex--;
						return true;
					}
					count_idx++;
				}
			}
			
			return false;
		}
		
		//remove last node
		//look ahead of current node
		private boolean removeLast() {
		
			if(null==mHead) return false;
			
			Node curr = mHead;
			Node prev = null;
			while(null!=curr.next) {
				prev = curr;
				curr = curr.next;
			}
			//head is last and set null
			if(null==prev) {
				mHead = null;
			}else  {
				prev.next = curr.next;
			}
			mIndex--;
			return true;
		}
		
		private void print() {
			Node tmp = mHead;
			while(null!=tmp) {
				System.out.println(tmp.x + " ");
				tmp = tmp.next;
			}
		}
		
		private void test() {
			LinkedList ll = new LinkedList();
			ll.add(1);
			ll.add(3);
			ll.add(2);
			ll.addFront(9);
			ll.addFront(19);
			
			ll.addIndex(2, 2);
			ll.addIndex(0, 0);
			
			ll.remove(0);
			ll.remove(5);
//			ll.removeLast();
//			ll.removeLast();
//			ll.removeLast();
//			ll.removeLast();
//			ll.removeLast();
			ll.print();
			
		}
	}
	
	@Test
	public void test() {
		LinkedList myLL = new LinkedList();
		myLL.test();
	}
}

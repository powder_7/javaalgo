package com.ds;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;


public class Tree {

	public class Node{

		int x;
		List<Node> node;
		
		Node() {
			x=0;
			node=new ArrayList<Node>();
		}
		
	}
	public class pcSet {
		int p;
		int c;
		
		pcSet(int x, int y) {
			p = x;
			c = y;
		}
		
		pcSet() {
			
		}
	}
	


	private Node m_root;
	private List<pcSet> m_pc_list;
	private Queue<Node> m_queue = new LinkedList<Node>();
	private Stack<Node> m_stack = new Stack<Node>();
	private List<Node>  m_list  = new ArrayList<Node>();
	
	// tree input set
	//      1
	// 2    3    4
	//5 6  7 8  9
	public int buildpcSet() {
		if(m_pc_list == null)
			return -1;
		
		pcSet pc_set;
		 
		pc_set = new pcSet(1,2);
		m_pc_list.add(pc_set);
		
		pc_set = new pcSet(1,3);
		m_pc_list.add(pc_set);

		pc_set = new pcSet(1,4);
		m_pc_list.add(pc_set);

		pc_set = new pcSet(2,5);
		m_pc_list.add(pc_set);

		pc_set = new pcSet(2,6);
		m_pc_list.add(pc_set);

		pc_set = new pcSet(3,7);
		m_pc_list.add(pc_set);

		pc_set = new pcSet(3,8);
		m_pc_list.add(pc_set);

		pc_set = new pcSet(4,9);
		m_pc_list.add(pc_set);
		
		pc_set = new pcSet(9,0);
		m_pc_list.add(pc_set);
		
//		pc_set = new pcSet(5,0);
//		m_pc_list.add(pc_set);
//		
//		pc_set = new pcSet(0,-1);
//		m_pc_list.add(pc_set);
		
		return 1; 
	}
	public Tree() {
		m_pc_list = new ArrayList<pcSet>();
		buildpcSet();
	}
	
	public int findRoot() {
		List<Integer> parent = new ArrayList<Integer>();
		List<Integer> children = new ArrayList<Integer>();
		int r = -1;;
		// set parent and children list
		for(int i = 0; i<m_pc_list.size(); i++) {
			parent.add(m_pc_list.get(i).p);
			children.add(m_pc_list.get(i).c);
		}
		// root can't be parent or child
		// thus root can't be in child list
		for(int i = 0; i<parent.size(); i++) {
			r = parent.get(i);
			if(!children.contains(r))
				return r;
		}
		return r;
	}
	
	// use recursion to buildTree bc 
	// work and not language specific
	public Node buildTree(int x) {
	
		Node r = new Node();
		r.x = x;
		List<Integer> children  = new ArrayList<Integer>();

		for(int i=0; i<m_pc_list.size(); i++) {
			// found parent to add children
			if(m_pc_list.get(i).p == x) {
				// add child
				children.add(m_pc_list.get(i).c);
			}
		}
		
		// add children to parent recursively
		for(int i=0; i<children.size(); i++) {
			r.node.add(buildTree(children.get(i)));
		}
		return r;
	}
	
	// BFS buildTree use queue
	// DFS buiddTree use stack
	// note. buidTree is like printTree
	public Node buildTree1(int x) {
		
		Node tmp = null;
		Node r = new Node();
		r.x = x;
		//key is ptr to root
		Node r_ptr = r;
				
		m_queue = new LinkedList<Node>();
		m_queue.add(r);
 
		while(!m_queue.isEmpty()) {
			//parent node
			r_ptr = m_queue.poll();
			//loop all the children
			for(int i=0; i<m_pc_list.size(); i++) {
				if(m_pc_list.get(i).p == r_ptr.x) {
					tmp = new Node();
					tmp.x = m_pc_list.get(i).c;
					// store child in queue
					m_queue.add(tmp);
					// add child to parent
					r_ptr.node.add(tmp);
				}
			}
		}
		return r;
	}
	
	public void printTreeDFS(Node r) {
		
		if(r==null) return;
		
		System.out.println(r.x);
		for(int i=0; i<r.node.size(); i++) {
			printTreeDFS(r.node.get(i));
		}
	}
	
	// queue, dequeue, update current
	// and next level to print
	// return tree height, else -1
	public int printTreeBFS(Node r) {
		
		if(r==null) return -1;
		
		m_queue = new LinkedList<Node>();
		m_queue.add(r);
		
		int curr_lvl = 0;
		int next_lvl = 0;
		int height = 0;
		while(!m_queue.isEmpty()) {
			r = m_queue.poll();
			// loop the children
			// add to queue
			for(int i=0; i<r.node.size(); i++) {
				m_queue.add(r.node.get(i));
				next_lvl++;
			}
			System.out.print(" " +r.x);
			if(curr_lvl == 0) {
				curr_lvl = next_lvl;
				next_lvl = 0;
				System.out.println("");
				height++;
			}
			curr_lvl--;
		}
		return height;
	}
	
	// assume x is unique only 1 exist
	// return null if not found
	public Node findNodeParent(Node r, int x) {
		
		if(r == null) return null;
		
		if(r.x == x) return r;
		
		Node parent = new Node();
		m_queue = new LinkedList<Node>();
		m_queue.add(r);
		while(!m_queue.isEmpty()) {
			parent = m_queue.poll();
			// loop children
			for(int i=0; i<parent.node.size(); i++) {
				Node children = parent.node.get(i);
				if(children.x == x) {
//					System.out.println("found x = " + x + " i = " + i);
					return parent;
				}
				//add children
				m_queue.add(parent.node.get(i));
			}
		}
		// not found
		return null;
	}
	
	// recursive find x and return node
	public Node findNodeXRecursive(Node r, int x) {

		if(r==null) return r;
		if(r.x == x) {
			System.out.println("r.x = " + r.x);
			return r;
		}
		for(int i=0; i<r.node.size();i++) {
			if(r.node.get(i).x == x) {
				return r.node.get(i);
			}
			findNodeXRecursive(r.node.get(i), x);
		}
		// didn't find
		return null;
	}
	
	// count from top
	//not the good way of doing it
	public int getTreeHeight(Node r, int height, int max_height) {
		
		if(r==null) return 0;
		max_height = Math.max(height,max_height);
		for(int i=0; i< r.node.size(); i++) {
			// add 1 going to child node
			max_height = getTreeHeight(r.node.get(i), height+1, max_height);
		}
		//last return is here
		return max_height;
	}
	
	// count node from bottom
	// goto end of left and count backward
	// compare it with height when function return.
	public int getTreeHeight(Node r ) {
	
		if(r==null)return 0;
		int t_height=0;
		// loop thru all nodes
		for(int i=0; i<r.node.size(); i++) {
			// goto end of node count backward.
			// assign to height, height is use to compare node(i>1) 
			// when function return 
			t_height=Math.max(t_height, getTreeHeight(r.node.get(i) ) );
		}
		return t_height+1;
	}
	
	// find path in tree to sum x
	public boolean hasPathSum(Node r, int sum,  Stack<Node> st) {

		boolean  found = false;
		if(r==null) return false;
		//push node print path
		st.add(r);
		sum = sum - r.x;
		//no more node and sum is zero
		if(sum==0 && r.node.isEmpty()) {
			System.out.print("found path = "  );
//			while(!st.isEmpty()) {
//				System.out.print( " " + st.remove(0).x);
//			}
			for( Node tmp : st) {
				System.out.print( " " + tmp.x);
			}
			System.out.println("");
			found = true;
		}
		//recursive loop thru children
		for(int i = 0; i< r.node.size(); i++) {
			// need this trick to get result back from recursion 
			if(found) break;
			found = hasPathSum(r.node.get(i), sum, st);
		}
		//remove node bc is not the path
		if(!st.isEmpty())st.pop();
		return found;
	}
	//DFS print path and sum
	public void printPathAndSum(Node r, int sum, Stack<Node> st) {
		
		if(r==null) return;
		
		st.push(r);
		sum = sum + r.x;
		
		// no more node print sum
		if(r.node.isEmpty()) {
			for(int i=0; i<st.size(); i++) {
				System.out.print("  " + st.get(i).x);
			}
			System.out.println(" Path sum = " + sum);
		}
		//loop thru children
		for(int i=0; i<r.node.size(); i++) {
			printPathAndSum(r.node.get(i), sum, st);
		}
		// end of node, remove and goto
		// different branch
		if(!st.isEmpty()) st.pop();
	}
	// need 2 parm, one to count and one to hold max
	// sum count up to current node
	// return max or min of path depend on input s={max,min}
	public int maxminPathSum(Node r, int sum, int total_sum, String s) {
		
		if(r==null) return -1;
		sum = sum +r.x;
		
		// end of path
		if(r.node.isEmpty()) {
			if(s == "min") 
				total_sum = Math.min(sum, total_sum);
			else 
				total_sum = Math.max(sum, total_sum);

		}
		//loop thru children
		for(int i=0;i<r.node.size();i++) {
			// call function and evaluate first
			// then assign to variable, 
			// then loop and use it as input
			total_sum = maxminPathSum(r.node.get(i), sum, total_sum, s);
		}
 		return total_sum;
	}
	
	public void testFindNodeTest() {
		
		int findx = 8;
		Node tmp = null;
//		System.out.println("find parent and x =  " + findx);
//		tmp = findNodeParent(m_root, findx);
//		System.out.println("Print found parent and their children");
//		printTreeBFS(tmp);
		 
		System.out.println("recursive find Node of x  " + findx );
		tmp = findNodeXRecursive(m_root, findx);
		System.out.println("Print result");
		printTreeBFS(tmp);
		
	}
	
	public void findPathAndSumTest() {
		
		int findx = 8;
		Stack<Node> st = new Stack<Node>();
		System.out.println("hasPathSum of "+  findx + "  = " + hasPathSum(m_root, findx, st));
//		
		System.out.println("Path and sum = ");
		printPathAndSum(m_root, 0, st);
	}
	
	public void maxminPathSumTest() {
		System.out.println("min path sum = " + maxminPathSum(m_root, 0, 1000, "min"));
		System.out.println("max path sum = " + maxminPathSum(m_root, 0, 0, "max"));
		
	}
	
	// parent add the children of delete node
	// return parent of delete node
	public Node deleteNode(Node r, int x, Stack<Node> st, Queue<Node> q) {
		
		if(r==null) return null;
		// key pointer to root
		Node p, node_ptr = r;
//		st.add(node_ptr);
		q.add(node_ptr);
		
		while(!q.isEmpty()) {
//			p = st.pop();
			p = q.poll();
			for(int i=0; i<p.node.size(); i++) {
				//found match
				Node child = p.node.get(i); 
				if( child.x== x) {
					// parent add match node's children
					for(int k=0; k < child.node.size(); k++) {
						p.node.add(child.node.get(k));
					}
					//remove child x from tree
					p.node.remove(i);
					break;
					
				}else {
//					st.push(child);
					q.add(child);
				}
			}
			
		}
		
		return node_ptr;
		
	}
	
	public void deleteNodeTest(Node r, int x) {
		
		Stack<Node> st = new Stack<Node>();
		Queue<Node> q = new LinkedList<Node>();
		
		Node p = deleteNode(r, x, st, q); 
		printTreeBFS(p);
	}
	
	public void buildTreeTest() {
		Tree my_root = new Tree();
		
		int r = my_root.findRoot();
		System.out.println("find root = " + r);
	
		System.out.println("build tree ");
		m_root = buildTree(r);
		System.out.println("printTreeBFS test ");
		System.out.println("BSF tree height = " + printTreeBFS(m_root) );
//		printTreeDFS(m_root);
		
//		System.out.println("build tree before test ");
//		m_root = buildTree1(r);
//		System.out.println("build tree after test ");
//		System.out.println("printBFS test ");
//		printTreeBFS(m_root);
		
	}
	
	public void test() {
		
		buildTreeTest();
		System.out.println("Tree height = " + getTreeHeight(m_root ));
		System.out.println("Tree height = " + getTreeHeight(m_root, 0,0));
//		findPathAndSumTest();
//		maxminPathSumTest();
//		deleteNodeTest(m_root, 3);
	}
}

package com;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.TreeMap;

public class lottery {
 	
//	private Node mNode;
	private TreeMap<Integer, Node> mTreeHmap = new TreeMap<Integer, Node>(Collections.reverseOrder());
	public class Node {
	
		int num=0;
		int count=0;
		TreeMap<Integer, Node> leaf_map = new TreeMap<Integer,Node>();
 		
	    public void print(String prefix, boolean isTail) {
	    	
	    	if(num<0)num *=-1;
	    	String s = prefix + (isTail ? "└── " : "├────") + num + "[" + count + "] "; 
	        System.out.println(s);
	       	        
	        for(Map.Entry<Integer,Node> entry : leaf_map.entrySet()) {
	        	entry.getValue().print(prefix + (isTail ? "    " : "*     "), false);
	        
	        }
	    }
	}
 
	public void testDoLottery() throws IOException {
		
 		List<Integer> list = new ArrayList<Integer>(Arrays.asList(1, 4, 5, 24, 30, -1));
 		List<Integer> list1 = new ArrayList<Integer>(Arrays.asList(1, 7, 44, 68, 73, -1));
 		List<Integer> list2 = new ArrayList<Integer>(Arrays.asList(1, 8, 54, 69, 72, -1));
 		List<Integer> list3 = new ArrayList<Integer>(Arrays.asList(2, 7, 20, 55, 70, 1));
 
//		doLottery(list);
//		doLottery(list1);
//		doLottery(list2);
		doLottery(list3);
 		printTree(mTreeHmap);
		mTreeHmap.get(-40).print("", true);
	}
	
	// note need to build tree with root and parent/child base on list.
 	public void doLottery(List<Integer> list) throws IOException {
 
 		// set/update maga or power num to tree 
 		int lot_num = list.get(list.size()-1);
 		//root
 		Node root = new Node();
 		if(mTreeHmap.containsKey(lot_num)) {
 			root = mTreeHmap.get(lot_num);
 			root.count++;
 			mTreeHmap.put(lot_num, root);
 		}
 		// add to tree
 		else {
 			root.num = lot_num;
 			root.count = 1;
 			root.leaf_map = new TreeMap<Integer, Node>();
 			mTreeHmap.put(lot_num, root);
 		}
 		
		int p = lot_num;
		int c = -1;
		if(mTreeHmap.containsKey(lot_num)) {
	 		// loop thru all nums except the mega/power num
	 		int i=0;
	 		root = mTreeHmap.get(lot_num);
 
	 		for(Integer x : list) {
	 			if(i==list.size()-1) break;
	 			c = x;
	 			updateTreeHmap(root,p, c, list);
	 			i++;
	 			p = c; 
	 		}
 		}

// 		printTree(mTreeHmap);
 		
	}
	
 	public boolean updateTreeHmap(Node branch, int parent, int child, List<Integer> list) throws IOException {
 		
 	    boolean found = false;
 		//parent exits, add child
 		if(branch!=null) {
 			Node n = new Node();
 			if(branch.num == parent) {
				if(branch.leaf_map.containsKey(child)) {
					//add count to child
	 				n = branch.leaf_map.get(child);
		 			n.count++;
		 			branch.leaf_map.put(child, n);
		 			found =  true;
				}
	 			//add child node
	 			else {
	 				n.num = child;
	 				n.count++;
	 				n.leaf_map = new TreeMap<Integer, Node>();
	 				branch.leaf_map.put(child, n);
	 				found =  true;
 	 			}
			}
			else {
 
//				// dept first search the children
				for(Map.Entry<Integer,Node> entry : branch.leaf_map.entrySet()) {

					if(list.contains(entry.getValue().num ))
						found = updateTreeHmap(entry.getValue(), parent, child, list);

					if(found) {
//						System.out.println("found " + entry.getValue().num + " p = "+ parent + " c = " + child);
						break;
					}
					
//					if(list.contains(entry.getValue().num ))
//						found = updateTreeHmap(entry.getValue(), parent, child, list);
					
				}
	 		}
		}
 		return found;
 	}
 	
 	public void printMaxPath() {
 		
 	}
	
 	//breath dept search
 	public void printTree(TreeMap<Integer, Node> tree) {
 		 
  		tree.forEach((k,v)-> {
 			Queue<Node> q = new LinkedList<Node>();
 			Node n = v;
 			q.add(n);
 			int count = 1;
 			int curr_level=1;
 			int next_level = 0;
 			System.out.println("");
 			while(!q.isEmpty()) {
 				Node tmp = q.poll();
 				
				if (count == 1) {
					System.out.println(" maga/power = " + tmp.num + "[" + tmp.count + "]");
					count--;
				}

				for (Map.Entry<Integer, Node> entry : tmp.leaf_map.entrySet()) {

					q.add(entry.getValue());
					next_level++;
				 System.out.print(" p[" + tmp.num + "]" + entry.getValue().num + "[" + entry.getValue().count +"]" + " size = " + tmp.leaf_map.size());

				}
 				curr_level--;
				if (curr_level == 0) {
//					System.out.print(" " + tmp.num + "[" + tmp.count + "]");
					System.out.println(" ");
					curr_level = next_level;
					next_level = 0;
 				}
 			}
 			q.clear();
 			n=null;
 
 		});
  		
  		System.out.println("tree size = " + tree.size());
 	
 	}
 
	public void readFile() throws IOException {
		
		Map<Integer, Integer> hp = new HashMap<Integer, Integer>();
		
		try {
			URL path = lottery.class.getResource("MEGAMILLIONS.txt");
//			URL path = lottery.class.getResource("POWERBALL.txt");
//			URL path = lottery.class.getResource("SUPERLOTTO.txt");
			File f = new File(path.getFile());
 
			FileReader fileReader = new FileReader(f);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
 			String line;
			
			int i=0;
			while ((line = bufferedReader.readLine()) != null) {
				//skip first 5 lines in files
 				if(i>4) {
					String delims = "[ ]+";
					String[] tokens = line.split(delims);

					List<Integer> winnerList = new ArrayList<Integer>();
					for (int j = 5; j<tokens.length; j++) {
					    if(j==tokens.length-1) {
					    	int x = Integer.valueOf(tokens[j])*-1;
					    	winnerList.add(x);
					    }
					    else 
					    	winnerList.add(Integer.valueOf(tokens[j]));
					    
					}
					
					Collections.sort(winnerList.subList(0, winnerList.size()-1));
					//hack maga number bc repeats
					int lottery = winnerList.get(winnerList.size()-1);
					 
					if(hp.containsKey(lottery)) {
						int value = hp.get(lottery);
						hp.put(lottery, ++value);
					
					}
					else {
						hp.put(lottery, 1);
					}
 
//					System.out.println("**********");
//					if((winnerList.get(0)==4 || winnerList.get(0)==1) && winnerList.get(5)==-1) {
//						System.out.println(winnerList.toString());// + " size = " + winnerList.size());
//						doLottery(winnerList); 
//					}
					doLottery(winnerList);
				}
				else 
					i++;
			}
			fileReader.close();
 
 		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		for(Entry<Integer, Node> entry: mTreeHmap.entrySet()) {
			entry.getValue().print("", true);
		}
		
//		mTreeHmap.forEach((k,v)->System.out.print(k*-1 + "[" + v.count + "] "));
//		System.out.println(" mTree size = " + mTreeHmap.size()); 
	}
	
 	
	public static void main(String args[]) throws IOException {
 
		
		lottery l = new lottery();
		
		l.readFile();
//		l.testDoLottery();
		
//		l.test();
		
 	}
}

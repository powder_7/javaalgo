package com.recursion;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;

public class Recursion {

	public void Recursion() {
		
	}
	
	public void reverse(int a[], int start, int end) {
		
		if(start >= end)
			return;
		
		int tmp;
		tmp = a[start];
		a[start] = a[end];
		a[end] = tmp;
		
		reverse(a, start+1, end-1);
	}
	
	int partition(int a[], int low, int high) {
	
		int pivot_val = a[(low+high)/2];
		while(low <= high) {
			while(a[low] < pivot_val) low++;
			while(a[high] > pivot_val) high--;
			
			if(low <= high) {
				int tmp = a[low];
				a[low++] = a[high];//assign then increment idx
				a[high--] = tmp;;
			}
		}
		return low;
	}
	
	
	void quickSort(int a[], int low, int high) {
		if(low < high) {
			int p = partition(a, low, high);
			quickSort(a, low, p-1);
			quickSort(a, p, high);
		}
	}
	
	public int fib_mem(int n, int a[]) {
		
		
		if(n<=1) return n;
		if(a[n] != -1) return a[n];
		else {
			a[n] = fib_mem(n-1, a) + fib_mem(n-2, a);
		}
		return a[n];
	}
	void testQuickSort() {
	
		
		int a[] = new int[10];
		Random rand = new Random();
		for(int i=0; i<a.length; i++) {
			a[i]=rand.nextInt(30)+1;
		}
		
		System.out.println("a = " + Arrays.toString(a));
		quickSort(a, 0, a.length-1);
		System.out.println("a qsort  = " + Arrays.toString(a));
		
		for(int i=0; i< a.length; i++) {
			a[i]=-1;
		}
		System.out.println("a = " + Arrays.toString(a));
		int x = 7;
		System.out.println("fib " + x + " = " + fib_mem(x, a));
		System.out.println("a = " + Arrays.toString(a));
	}
	
	public void test() {
		
		int a[] = {1,2,3,4,5};
		
		System.out.println("Reverse before test = " + Arrays.toString(a));
		reverse(a, 0, a.length-1);
//		System.out.println("Reverse after  test = " + );
		System.out.println("Reverse before test = " + Arrays.toString(a));
		
		testQuickSort();
		
	}
}

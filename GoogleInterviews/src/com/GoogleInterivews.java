package com;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.TreeSet;

import org.junit.Test;

class Node<Object> {

	Node<Object> next = null;
	Node<Object> prev = null;
	Object x;

}

class myIntervew {

	Node<Integer> r = null;

	Node<Integer> dbl_tail = null;
	Node<Integer> dbl_root = null;

	
	// retainAll intersection of set
	public void minArrayIntersect2() {
		// Create two collections:
		ArrayList lList = new ArrayList();
		ArrayList aList = new ArrayList();

		// Add some elements to lList:
		lList.add("Isabella");
		lList.add("Angelina");
//		lList.add("Angelina");
		lList.add("Pille");
		lList.add("Hazem");
		
		// Add some elements to aList:
		aList.add("Isabella");
		aList.add("Angelina");
		aList.add("Angelina");
		aList.add("Bianca");

		// Display the two collections before:
		System.out.println("The aList collection before: " + aList);
		System.out.println("The lList collection before: " + lList);

		// Delete the elements lList that are not in aList:
		lList.retainAll(aList);

		// Display the collection after:
		// Display the two collections before retaining all:
		System.out.println("The aList collection after: " + aList);
		System.out.println("The lList collection after: " + lList);
	}

	
	// use retainAll for intersection a, b set.
	public void minArrayListIntesect(){
		
		List<Integer> a = new ArrayList<Integer>( Arrays.asList(1, 2, 4, 4, 3)); 
		List<Integer> b = new ArrayList<Integer>( Arrays.asList(1, 2, 3, 4));

	    b.retainAll(a);
	    System.out.println("minArrayListIntesect : " + b);

	}

	public List<Integer> findMinIntersect (int[] a, int[] b){
	    List<Integer> result = new ArrayList<Integer>();
	    List<Integer> alist = new ArrayList<Integer>();
	   
	    //add array a to arraylist
	    for(int i=0; i < a.length; i++) {
	    	if(!alist.contains(a[i]))
	    		alist.add(a[i]);
	    }
	    
	    for(int j=0; j<b.length; j++) {
	    	//check not in result and intersect with alist
	    	if(!result.contains(b[j]) && alist.contains(b[j]) ) {
	    		result.add(b[j]);
	    	}
	    }

//solution 2: sort a, b array
//	    Arrays.sort(a);
//	    Arrays.sort(b);
	    		
//	    int i = 0;
//	    int j = 0;
//	    while(i < a.length && j < b.length){
//	        if(a[i]<b[j])
//	            ++i;
//	        else if (a[i] > b[j])
//	            ++j;
//	        else{
//	            if (!result.contains(a[i]))
//	                result.add(a[i]);
//	            ++i;
//	            ++j;
//	        }   
//	    }
	    System.out.println("findMinInterect " + result.toString());
	    return result;
	}
	
	public void printPar(int count) {
		char[] str = new char[count * 2]; // Create a char array to store the
											// parentheses
		printPar(count, count, str, 0); // call the printPar method, the
										// parameters are the left,
		// the right parentheses, the array to store the
		// parenthese, and the counter
	}

	public void printPar(int l, int r, char[] str, int count) {
		// Use recursion method to print the parentheses
		// if there are no parentheses available, print
		// them out
		if (l == 0 && r == 0) {
			System.out.println(str); // Print out the parentheses
		}

		else {
			if (l > 0) { // try a left paren, if there are some available
				str[count] = '(';
				printPar(l - 1, r, str, count + 1); // Recursion
			}
			// Add constraint that a parenthesis is open before inserting a
			// closing paranthesis
			// hence, add l < r - meaning one 'l' is printed before going for an
			// 'r'
			// try a right paren, if there are some
			// available
			if (l < r) {
				str[count] = ')';
				printPar(l, r - 1, str, count + 1); // Recursion
			}
		}
	}

	public void doStringToLong() {

		String s = "1234";
		try {
			System.out.println("1234 str to long : " + StringToLong(s));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private long StringToLong(String s) throws NumberFormatException {
		long n = 0;
		long factor = 1;
		int c;
		for (int i = s.length() - 1; i > 0; --i) {
			c = s.charAt(i);
			if (c < 48 || c > 57) {
				throw new NumberFormatException(s + " is not a valid number.");
			}
			n += (c - 48) * factor;
			factor *= 10;
		}

		c = s.charAt(0);
		if (c == '-') {
			return -n;
		} else {
			n += (c - 48) * factor;
		}
		return n;
	}

	private void Brackets(String output, int open, int close, int total) {
		if (output.length() == total * 2 && output.charAt(total * 2 - 1) != '(') {
			System.out.println(output);
		} else if (open > total || open < close) {
			return;
		} else {
			if ((open == close)) {
				Brackets(output + "(", open + 1, close, total);
			}
			if ((open > close)) {
				Brackets(output + "(", open + 1, close, total);
				Brackets(output + ")", open, close + 1, total);
			}
		}
	}

	public void Brackets(int total) {
		Brackets("", 0, 0, total);
	}

	public void quicksort(int[] a, int left, int right) {
		// left is the lower index, right is the upper index
		// of the region of array a that is to be sorted
		int lt = left, rt = right, h;

		// comparison element x
		int x = a[(left + right) / 2];

		// partition
		do {
			while (a[lt] < x)
				lt++;
			while (a[rt] > x)
				rt--;
			if (lt <= rt) {
				h = a[lt];
				a[lt] = a[rt];
				a[rt] = h;
				lt++;
				rt--;
			}
		} while (lt <= rt);

		// recursion
		if (left < rt)
			quicksort(a, left, rt);
		if (lt < right)
			quicksort(a, lt, right);
	}

	public int pivotedBinarySearch(int[] a, int n, int k) {
		int pivot = findPivot(a, 0, n - 1);

		// if (pivot != -1) {
		// if (a[pivot] == k) {
		// return pivot;
		// } else if (a[0] <= k) {
		// return binarySearch(a, 0, pivot-1, k);
		// } else {
		// return binarySearch(a, pivot + 1, n - 1, k);
		// }
		// }

		if (pivot != -1) {
			if (a[pivot] == k) {
				return pivot;
			} else if (a[0] <= k) {
				return Arrays.binarySearch(a, 0, pivot - 1, k);
			} else {
				return Arrays.binarySearch(a, pivot + 1, n - 1, k);
			}
		}

		return -1;
	}

	public int binarySearch(int[] a, int low, int high, int k) {
		if (high > low) {
			int mid = (low + high) / 2;

			if (a[mid] == k) {
				return mid;
			} else if (a[mid] > k) {
				return binarySearch(a, low, mid - 1, k);
			} else {
				return binarySearch(a, mid + 1, high, k);
			}
		}

		return -1;
	}

	public int findPivot(int[] a, int low, int high) {
		if (high > low) {
			int mid = (low + high) / 2;

			if (a[mid] > a[mid + 1]) {
				return mid;
			} else if (a[low] > a[mid]) {
				return findPivot(a, low, mid);
			} else {
				return findPivot(a, mid + 1, high);
			}
		}

		return -1;
	}

	public void compressString(String s) {

		String cmp_str = "";
		int i, counter = 1;
		for (i = 1; i < s.length(); i++) {
			if (s.toCharArray()[i - 1] == s.toCharArray()[i]) {
				counter++;
			} else {
				cmp_str += Character.toString(s.toCharArray()[i - 1])
						+ Integer.toString(counter);
				counter = 1;
			}
		}
		cmp_str += Character.toString(s.toCharArray()[i - 1])
				+ Integer.toString(counter);
		System.out.println("compress string : " + cmp_str);

	}

	public int findMaxConsecutiveSum(int[] arr) {
		int temp = 0;
		int maxmax = 0;
		int temp_start_idx =0, start_idx = 0, end_idx = 0;
		for (int i = 0; i < arr.length; i++) {
			// reset temp if it's less than 0
			if (temp < 0) {
				temp = arr[i];
				temp_start_idx = i;
			} else {
				temp += arr[i];
			}
			if (temp >= maxmax) {
				maxmax = temp;
				start_idx = temp_start_idx;
				end_idx = i;
			}
		}
		System.out.println("start idx : " + start_idx + " end idx : " + end_idx);
		return maxmax;
	}

	public int maxLose(int[] a) {
		int tmp = 0, max = 0;
		for (int i = 0; i < a.length; i++) {
			tmp = Math.max(tmp, a[i]); // find max num in array
			max = Math.max(max, tmp - a[i]); // different of max num in array
												// and store it.
		}

		return max;
	}

	public int maxGain(int[] a) {
		int tmp = a[0], max = 0;

		for (int i = 0; i < a.length; i++) {
			tmp = Math.min(tmp, a[i]);
			max = Math.max(max, a[i] - tmp);
		}

		return max;
	}

	// sort array all zero on left others on right
	// O(n) and in place w/ no extra space.
	public void sortZeroInPlace() {
		int[] a = { 1, 0, 1, 0, 1, 0 };
		int j = 0;
		for (int i = 0; i < a.length; i++) {
			if (a[i] == 0 && i != j) {
				a[i] = a[j];
				a[j] = 0;
				j++;
			}
		}
		System.out.println(" sortZeroInPlace : " + Arrays.toString(a));
	}

	// Given an array of numbers. Create another array that contains
	// the product of all the members in the array except the current element.
	// answer : get product of all element in array. divide the unwanted element
	// from the total product.
	public void productArray(int[] a) {
		int[] b = new int[a.length];
		int total_prod = 1;
		for (int i = 0; i < a.length; i++) {
			total_prod *= a[i];
		}
		// case divide zero
		for (int j = 0; j < a.length; j++) {
			if (a[j] == 0)
				b[j] = 0;
			else
				b[j] = total_prod / a[j];
		}
		System.out.println(Arrays.toString(b));
	}

	public boolean isSymetric(String s) {

		for (int i = 0; i < s.length() / 2; i++) {
			if (s.charAt(i) != s.charAt(s.length() - 1 - i))
				return false;
		}
		return true;
	}

	// shift array with rotation.
	// [a, b, c << 2 = 2 c, a, b]
	public void shiftStr() {

		char[] a = { 'a', 'b', 'c', 'd', 'e' };
		char tmp;
		int shift = 4;
		int i = a.length - 1;
		while (shift > 0) {

			tmp = a[i];
			a[i] = a[i - 1];
			a[i - 1] = tmp;
			i--;
			shift--;
		}
		System.out.println(a);
	}

	public int[] division(int x, int y) {
		int[] answer = new int[2];
		int quotient = 0;
		int remainder = 0;
		int dividend = x;
		// x / y
		while (x >= y) {
			x = x - y;
			quotient++;

		}
		remainder = x;
		answer[0] = quotient;
		answer[1] = remainder;
		System.out.println(String.valueOf(dividend) + " / " + String.valueOf(y)
				+ " : " + "quotient : " + answer[0] + " remainder : "
				+ answer[1]);
		return answer;

	}

	public void addNode(int x) {

		if (r == null) {
			r = new Node<Integer>();
			r.x = x;
			dbl_tail = r;
		} else {
			Node<Integer> n = new Node<Integer>();
			n.x = x;
			n.next = r;
			r = n;
		}
	}

	public void addDBLNode(int x) {

		if (dbl_root == null) {
			dbl_root = new Node<Integer>();
			dbl_root.x = x;
		} else if (dbl_tail == null) {
			Node<Integer> n = new Node<Integer>();
			n.x = x;
			n.next = dbl_root;
			dbl_tail = dbl_root;
			dbl_root.prev = n;
			dbl_root = n;
		} else {
			Node<Integer> n = new Node<Integer>();
			n.x = x;
			n.next = dbl_root;
			dbl_root.prev = n;
			dbl_root = n;
		}
	}

	public void printNode() {

		Node<Integer> root = this.r;
		while (root != null) {
			System.out.println(root.x);
			root = root.next;
		}
	}

	public void printDBLNode() {

		Node<Integer> root = this.dbl_root;
		while (root != null) {
			System.out.println(root.x);
			root = root.next;
		}
	}

	public void printDBLNodeRev() {

		Node<Integer> root = this.dbl_tail;
		// while (root.next != null) {
		// // System.out.println(root.x);
		// root = root.next;
		// }
		while (root != null) {
			System.out.println(root.x);
			root = root.prev;
		}
	}

	public void deleteNode(int x) {
		Node<Integer> root = r;

		while (root != null) {
			// delete head
			if (root.next != null && root.next.x == x)
				root.next = root.next.next;

			if (root.x == x) {
				// r.next = null;
				r = root.next;

			}
			if (root != null)
				root = root.next;
		}
	}

	public void deleteDBLNode(int x) {

		Node<Integer> root = this.dbl_root;
		Node<Integer> tail = this.dbl_tail;

		while (root != null) {

			if (root.x == x) {
				this.dbl_root = root.next;

				if (root.next != null) {
					root.next.prev = root.next.prev.prev;
				}
				if (tail != null && tail.x == x) {
					tail = tail.prev;
					this.dbl_tail = tail;
				}

			}
			if (root.next != null && root.next.x == x) {
				if (root.next.next != null) {
					root.next.prev = null;
					root.next.next.prev = root;
					root.next = root.next.next;

				}
				// tail
				else {
					tail = tail.prev;
					this.dbl_tail = tail;
					root.next.prev = null;
					root.next = root.next.next;

				}
			}

			if (root != null)
				root = root.next;
		}

	}

	void reverseLinkList() {

		Node<Integer> tmp, prev = null;
		Node<Integer> root = this.dbl_root;

		while (root != null) {
			tmp = root.next; // store root.next
			root.next = prev;// point root.next to previous, first loop is null
			prev = root; // previous is previous node
			root = tmp; // move root to next node
		}
		this.dbl_root = prev;
	}

	/**
	 * 
	 */
	public void testNode() {

		// addNode(1);
		// addNode(2);
		// addNode(3);
		// addNode(4);
		// System.out.println("print nod ");
		// printNode();
		//
		// deleteNode(1);
		// deleteNode(2);
		// System.out.println("delete nod ");
		// System.out.println("print nod ");
		// printDBLNode();

		addDBLNode(1);
		addDBLNode(2);
		addDBLNode(3);

		System.out.println("print nod ");
		printDBLNode();
		reverseLinkList();
		System.out.println("print reverseLinkList nod ");
		printDBLNode();

		// printDBLNodeRev();

		// /this.deleteDBLNode(1);
		// this.deleteDBLNode(2);
		// this.deleteDBLNode(3);

		// System.out.println("delete nod ");
		// System.out.println("print nod ");
		// printDBLNode();
		// System.out.println("print rev nod ");
		// printDBLNodeRev();

	}

	public int getMaxVow(String s) {
		int num_of_vow = 0;
		char[] str = s.toCharArray();
		char tmp = '1';
		for (int i = 0; i < str.length; i++) {
			tmp = Character.toLowerCase(str[i]);

			if (tmp == 'a' || tmp == 'e' || tmp == 'o' || tmp == 'i'
					|| tmp == 'u')
				num_of_vow++;
		}

		return num_of_vow;
	}

	public int getMaxVowHashSet(String s) {

		int max_num_vow = 0;

		if (s.length() > 0) {
			try {
				HashSet<Object> hs = new HashSet<Object>();
				hs.add('a');
				hs.add('e');
				hs.add('i');
				hs.add('o');
				hs.add('u');

				for (int i = 0; i < s.toCharArray().length; i++) {
					if (hs.contains(Character.toLowerCase(s.toCharArray()[i])))
						max_num_vow++;
				}

				// loop hash set
				Iterator<Object> it = hs.iterator();
				Object set;
				while (it.hasNext()) {
					set = it.next();
					System.out.print(" " + set + " ");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return max_num_vow;
	}

	public int getMaxVowTreeSet(String s) {

		int max_num_vow = 0;
		if (s.length() > 0) {
			try {
				TreeSet<Object> ts = new TreeSet<Object>();
				ts.add('a');
				ts.add('e');
				ts.add('i');
				ts.add('o');
				ts.add('u');

				for (int i = 0; i < s.toCharArray().length; i++) {
					if (ts.contains(Character.toLowerCase(s.toCharArray()[i])))
						max_num_vow++;
				}

				// loop tree set
				Iterator<Object> it = ts.iterator();
				Object set;
				while (it.hasNext()) {
					set = it.next();
					System.out.print(" " + set + " ");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return max_num_vow;
	}

	public int getMaxVowTreeMap(String s) {

		int max_num_vow = 0;
		if (s.length() > 0) {
			try {
				TreeMap<Object, Object> tm = new TreeMap<Object, Object>();
				tm.put('a', 0);
				tm.put('e', 0);
				tm.put('i', 0);
				tm.put('o', 0);
				tm.put('u', 0);

				for (int i = 0; i < s.toCharArray().length; i++) {
					if (tm.containsKey(Character.toLowerCase(s.toCharArray()[i])))
						max_num_vow++;
				}
				// loop tree map
				Iterator<Entry<Object, Object>> it = tm.entrySet().iterator();
				Entry<Object, Object> entry;
				while (it.hasNext()) {
					entry = it.next();
					System.out.print(entry.getKey() + " = " + entry.getValue());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return max_num_vow;
	}

	public int getMaxVowHashTable(String s) {
		int max_num_vow = 0;
		if (s.length() > 0) {
			try {
				char[] str = s.toCharArray();
				Hashtable<Object, Object> ht = new Hashtable<Object, Object>();
				// hash table put
				ht.put('a', 0);
				ht.put('e', 0);
				ht.put('i', 0);
				ht.put('o', 0);
				ht.put('u', 0);

				for (int i = 0; i < str.length; i++) {
					if (ht.containsKey(Character.toLowerCase(str[i])))
						max_num_vow++;
				}
				// loop and print
				Iterator<Entry<Object, Object>> it = ht.entrySet().iterator();
				Entry<Object, Object> entry;
				while (it.hasNext()) {
					entry = it.next();
					System.out.print(entry.getKey().toString() + " = "
							+ entry.getValue().toString());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return max_num_vow;
	}

	public int getMaxVowHashMap(String s) {

		int max_num_vow = 0;
		if (s.length() > 0) {
			try {
				HashMap<Object, Object> hp = new HashMap<Object, Object>();
				hp.put('a', 0);
				hp.put('e', 0);
				hp.put('i', 0);
				hp.put('o', 0);
				hp.put('u', 0);

				for (int i = 0; i < s.toCharArray().length; i++) {
					if (hp.containsKey(Character.toLowerCase(s.toCharArray()[i])))
						max_num_vow++;
				}
				// loop hash map
				Iterator<Entry<Object, Object>> it = hp.entrySet().iterator();
				Map.Entry<Object, Object> entry;
				while (it.hasNext()) {
					entry = it.next();
					System.out.print(entry.getKey() + " = " + entry.getValue());
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return max_num_vow;
	}

	public void readFile() {

		BufferedReader br = null;
		String cur_line;
		int max_vow = 0;
		int file_line = 1;
		int tmp = 0;
		try {

			br = new BufferedReader(new FileReader("d:\\lottery.txt"));

			while ((cur_line = br.readLine()) != null) {
				tmp = this.getMaxVow(cur_line);
				if (tmp > max_vow) {
					max_vow = tmp;
					file_line++;
				}
			}
			System.out.println("file line : " + file_line + " with max vow : "
					+ max_vow);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}

public class GoogleInterivews {

	/**
	 * @param args
	 */
	/**
	 * @param args
	 */
	/**
	 * @param args
	 */
	public static void main(String args[]) {
		myIntervew myInt = new myIntervew();
		String s = "asdaeiouf";
		String b = "aadddcbbaaszz";
		int[] a = { -4, 7, -9, 11, 8, 22, 98, 5, 5, 3, 3, 3 };
		int[] c = { 3, 4, 5, 6, 7, 1, 2 };
		int[] d = { 1, 1, 3, 3, 4, 5 };
		int[] e = { 1, 1, 3, 3 };
		int[] f = {1,2,3,-7, 1, 2, 10};

		try {
			assertEquals("good ", 6, myInt.getMaxVow(s));

			System.out.print(" hash table :  ");
			assertEquals("good ", 6, myInt.getMaxVowHashTable(s));
			System.out.println(" ");

			System.out.print(" hash map : ");
			assertEquals("good ", 6, myInt.getMaxVowHashMap(s));
			System.out.println(" ");

			System.out.print(" tree map : ");
			assertEquals("good ", 6, myInt.getMaxVowTreeMap(s));
			System.out.println(" ");

			System.out.print(" hash set : ");
			assertEquals("good ", 6, myInt.getMaxVowHashSet(s));
			System.out.println(" ");

			System.out.print(" tree set : ");
			assertEquals("good ", 6, myInt.getMaxVowTreeSet(s));
			System.out.println(" ");

		} catch (Exception z) {
			z.printStackTrace();
		}
		// myInt.getMaxVowHashTable(a);
		// myInt.readFile();
		
		myInt.testNode();
		myInt.division(10, 3);
		myInt.shiftStr();

		System.out.println("is symmetric : " + myInt.isSymetric(b));
//		myInt.productArray(c);
//		myInt.sortZeroInPlace();
//		myInt.compressString(b);
//		System.out.println(" is idx pivot rotation array : "
//				+ myInt.pivotedBinarySearch(c, c.length, 7));
//		System.out.println(" si not idx pivot rotation array : "
//				+ myInt.pivotedBinarySearch(d, d.length, 1));
//		myInt.quicksort(a, 0, a.length - 1);
//		System.out.println(" quick sort : " + Arrays.toString(a));
//		System.out.println("bracket combination : ");
//		myInt.printPar(3);
//		myInt.doStringToLong();
//		myInt.findMinIntersect(d, c);
//		myInt.minArrayListIntesect();
		
		System.out.println("max consecutive  = " + myInt.findMaxConsecutiveSum(f) );
		

	}

}
